package com.eoutlet.Eoutlet.pojo;


import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "msg",
        "data"
})
public class RememberCardName {



        @JsonProperty("status")
        public String status;
        @JsonProperty("msg")
        public String msg;
        @JsonProperty("data")
        public List<RememberCardDetail> data = null;

}