
package com.eoutlet.Eoutlet.pojo;

import java.io.Serializable;


public class OrderStreet implements Serializable
{
    private String _0;
    private String _1;
    private final static long serialVersionUID = -3307711843830742039L;

    public String get0() {
        return _0;
    }

    public void set0(String _0) {
        this._0 = _0;
    }

    public String get1() {
        return _1;
    }

    public void set1(String _1) {
        this._1 = _1;
    }

}
