package com.eoutlet.Eoutlet.pojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "code",
        "name",
        "cel_code",
        "placeholder"
})
public class countryCodeDetail {
    @JsonProperty("code")
    public String code;
    @JsonProperty("name")
    public String name;
    @JsonProperty("placeholder")
    public String placeholder;
    @JsonProperty("cel_code")
    public String cel_code;



}
