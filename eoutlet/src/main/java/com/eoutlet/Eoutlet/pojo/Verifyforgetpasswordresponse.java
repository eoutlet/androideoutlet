package com.eoutlet.Eoutlet.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Verifyforgetpasswordresponse {


    @JsonProperty("success")
    public Boolean success;
    @JsonProperty("otp")
    public String otp;
    @JsonProperty("customerId")
    public String customerId;
    @JsonProperty("url")
    public String url;

    @JsonProperty("msg")
    public String msg;


}
