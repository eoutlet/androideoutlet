package com.eoutlet.Eoutlet.pojo;

 public class CustoData2 {
     public String name;
     public String amount;
     public String size;
     public String color;
     public String quantity;

     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

     public String getAmount() {
         return amount;
     }

     public void setAmount(String amount) {
         this.amount = amount;
     }

     public String getSize() {
         return size;
     }

     public void setSize(String size) {
         this.size = size;
     }

     public String getColor() {
         return color;
     }

     public void setColor(String color) {
         this.color = color;
     }

     public String getQuantity() {
         return quantity;
     }

     public void setQuantity(String quantity) {
         this.quantity = quantity;
     }
 }
