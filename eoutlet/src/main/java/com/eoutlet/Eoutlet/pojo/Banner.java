
package com.eoutlet.Eoutlet.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "img",
    "id",
    "name"
})
public class Banner {

    @JsonProperty("img")
    public String img;
    @JsonProperty("id")
    public String id;
    @JsonProperty("name")
    public String name;

}
