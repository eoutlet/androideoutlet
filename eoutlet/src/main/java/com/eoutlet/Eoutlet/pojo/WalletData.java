package com.eoutlet.Eoutlet.pojo;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "customer_id",
        "wallet_amount",
        "transactions"
})
public class WalletData {

    @JsonProperty("customer_id")
    public String customerId;
    @JsonProperty("wallet_amount")
    public Integer walletAmount;
    @JsonProperty("transactions")
    public List<Transaction> transactions = null;




}
