
package com.eoutlet.Eoutlet.pojo;
import java.util.List;

import com.eoutlet.Eoutlet.pojo.CatagoryChild;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "image",
        "id",
        "name",
        "caption",
        "children"
})
public class Datum {

    @JsonProperty("image")
    public String image;
    @JsonProperty("id")
    public String id;
    @JsonProperty("name")
    public String name;
    @JsonProperty("caption")
    public String caption;
    @JsonProperty("children")
    public List<Child> children = null;

}