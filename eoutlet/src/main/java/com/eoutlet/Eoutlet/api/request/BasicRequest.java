package com.eoutlet.Eoutlet.api.request;




import com.eoutlet.Eoutlet.pojo.AddMoneyResponse;
import com.eoutlet.Eoutlet.pojo.AddressList;
import com.eoutlet.Eoutlet.pojo.AddtoCartResponse;
import com.eoutlet.Eoutlet.pojo.AppCurrentVersion;
import com.eoutlet.Eoutlet.pojo.ApplyCoupenResult;
import com.eoutlet.Eoutlet.pojo.BrandName;
import com.eoutlet.Eoutlet.pojo.CancelOrderListResponse;
import com.eoutlet.Eoutlet.pojo.CatagoryCollection;
import com.eoutlet.Eoutlet.pojo.ChangePassword;
import com.eoutlet.Eoutlet.pojo.ColorandSizeDetail;
import com.eoutlet.Eoutlet.pojo.ContactMessage;
import com.eoutlet.Eoutlet.pojo.DeleteitemMessage;
import com.eoutlet.Eoutlet.pojo.FeatureandRecommandedResponse;
import com.eoutlet.Eoutlet.pojo.FilterDetail;
import com.eoutlet.Eoutlet.pojo.GetBannners;
import com.eoutlet.Eoutlet.pojo.GetCountryCode;
import com.eoutlet.Eoutlet.pojo.GetHorizantalCategory;
import com.eoutlet.Eoutlet.pojo.GetShippingCharge;
import com.eoutlet.Eoutlet.pojo.GetToken;
import com.eoutlet.Eoutlet.pojo.Gethomecatagory;
import com.eoutlet.Eoutlet.pojo.GetnewArrivals;
import com.eoutlet.Eoutlet.pojo.GuestUser;
import com.eoutlet.Eoutlet.pojo.HelpSupport;
import com.eoutlet.Eoutlet.pojo.Homecatagory1;
import com.eoutlet.Eoutlet.pojo.LoginResponse;
import com.eoutlet.Eoutlet.pojo.OneHomeAPI;
import com.eoutlet.Eoutlet.pojo.OrderListResponse;
import com.eoutlet.Eoutlet.pojo.OrderResponse;
import com.eoutlet.Eoutlet.pojo.OrderResponseCod;
import com.eoutlet.Eoutlet.pojo.OrderResponseWallet;
import com.eoutlet.Eoutlet.pojo.OtpResponse;
import com.eoutlet.Eoutlet.pojo.RememberCardName;
import com.eoutlet.Eoutlet.pojo.ReorderResponse;
import com.eoutlet.Eoutlet.pojo.SaveAddress;
import com.eoutlet.Eoutlet.pojo.Trending;
import com.eoutlet.Eoutlet.pojo.VerificationResponse;
import com.eoutlet.Eoutlet.pojo.Verifyforgetpasswordresponse;
import com.eoutlet.Eoutlet.pojo.WalletHistory;
import com.eoutlet.Eoutlet.pojo.WalletPaymentStatus;
import com.eoutlet.Eoutlet.pojo.universalMessage;
import com.eoutlet.Eoutlet.pojo.MostDemanded;
import com.eoutlet.Eoutlet.pojo.ProductGallerryDetail;
import com.eoutlet.Eoutlet.pojo.SignUpResponse;
import com.eoutlet.Eoutlet.pojo.ViewCart1;
import com.eoutlet.Eoutlet.pojo.universalMessage2;
import com.eoutlet.Eoutlet.utility.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;



public interface BasicRequest {

    @GET(Constants.BASE_LINK + "guesttoken.php")
    Call<GuestUser> getGuestToken();

    @GET(Constants.BASE_LINK + "customertoken.php")
    Call<GuestUser> getGuestTokenforthankyou(@QueryMap Map<String, String> map1);

    @GET(Constants.BASE_LINK + "categoryapi.php")
    Call<CatagoryCollection> getcatagoryDetail();

    @GET(Constants.BASE_LINK + "categoryapi.php")
    Call<GetHorizantalCategory> getcatagoryDetailHome();

    @GET(Constants.BASE_LINK + "getbanners.php")
    Call<GetBannners> getBanners();

    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "productlistapi.php  ")
    Call<CatagoryList> getCataGoryList(@FieldMap Map<String, String> fields);

    @GET
    Call<ProductGallerryDetail> getCataGoryDetail(@Url String url);

    @GET(Constants.BASE_LINK + "prodgetsizecolor.php")
    Call<ColorandSizeDetail> getColorandSize(@QueryMap Map<String, String> query);

    @GET(Constants.BASE_LINK + "addcart.php")
    Call<AddtoCartResponse> addtocart(@QueryMap Map<String, String> query);




    @GET(Constants.BASE_LINK + "search.php")
    Call<CatagoryList> getSearchResult(@QueryMap Map<String, String> query);
    @GET(Constants.BASE_LINK + "getcustomerwallet.php")
    Call<WalletHistory> getwallethistory(@QueryMap Map<String, String> query);


    @GET(Constants.BASE_LINK + "getcurrentversion.php")
    Call<AppCurrentVersion> getLatestAppVersion();



    @GET(Constants.BASE_LINK + "viewcart.php")
    Call<ViewCart1> getCartDetail(@QueryMap Map<String, String> query);

    @GET(Constants.BASE_LINK + "getrelatedproduct.php?")
    Call<CatagoryList> getRelatedProduct(@QueryMap Map<String, String> query);


    @GET
    Call<Homecatagory1> getHomeCataGoryDetail(@Url String url);
    @GET
    Call<Gethomecatagory> getHomeCataGoryDetail2(@Url String url);


    @GET
    Call<OneHomeAPI> getHomeCataGoryDetail3(@Url String url);


    @GET
    Call<FeatureandRecommandedResponse> getfeatureandrecommanded(@Url String url);

    @GET
    Call<GetnewArrivals> getnewArrival(@Url String url);
    @GET
    Call<Trending> getTrending(@Url String url);



    @GET
    Call<BrandName> getHomeBrandDetail(@Url String url);

    @GET
    Call<MostDemanded> getpopularNewSesionData(@Url String url);


    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "signup.php")
    Call<SignUpResponse> getSignUp(@FieldMap Map<String, String> map1);


    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "getpayfortsdktoken.php")
    Call<GetToken> getTokenResponse(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "clearcart.php")
    Call<CancelOrderListResponse> clearAllItems(@FieldMap Map<String, String> map1);



    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "resetpassword.php")
    Call<universalMessage > resetnewPassword(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "prodel.php")
    Call<DeleteitemMessage> deleteItemfromCart(@FieldMap Map<String, String> map1);


    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "updatecart.php")
    Call<universalMessage2> changequantityfromCart(@FieldMap Map<String, String> map1);



    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "forgotpwd.php")
    Call<universalMessage> forGotPassword(@FieldMap Map<String, String> map1);


    @FormUrlEncoded
    @POST("paymentApi")
    Call<SignUpResponse> getToken(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "updateprofile.php")
    Call<ChangePassword> saveUpdateInfo(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "changepassword.php")
    Call<ChangePassword> saveNewPassword(@FieldMap Map<String, String> map1);


    @GET(Constants.BASE_LINK + "addresslist.php?")
    Call<AddressList> getAddressInfo(@QueryMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "addaddress.php")
    Call<SaveAddress> saveAddress(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "deleteaddress.php")
    Call<ChangePassword> deleteAddress(@FieldMap Map<String, String> map1);

    //@GET
    //Call<LoginResponse> doLogin(@Url String url);



    @GET(Constants.BASE_LINK + "customerapi.php")
    Call<LoginResponse> doLogin(@QueryMap Map<String, String> query);

    @GET(Constants.BASE_LINK + "countrylist.php")
    Call<GetCountryCode> getCountryDetail();



    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "deviceregister.php")
    Call<LoginResponse>deviceregister(@FieldMap Map<String, String> body);


    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "addmoney.php")
    Call<AddMoneyResponse>addmoney(@Field("amount") String amount, @Field("customer_id") String customer_id, @Field("remember_me") String remember_me, @Field("card_detail[number]") String number, @Field("card_detail[name]") String name, @Field("card_detail[expiry]") String expiry, @Field("card_detail[securitycode]") String securitycode);
    @FormUrlEncoded
    @POST(Constants.BASE_LINK + "addmoney.php")
    Call<AddMoneyResponse>addmoneyfromsavedcard(@Field("amount") String amount,@Field("token_name") String tokenname, @Field("customer_id") String customer_id, @Field("remember_me") String remember_me, @Field("card_detail[number]") String number, @Field("card_detail[name]") String name, @Field("card_detail[expiry]") String expiry, @Field("card_detail[securitycode]") String securitycode);

/*

    @POST(Constants.BASE_LINK + "addmoney.php")
    Call<LoginResponse>addmoney(@Body HashMap<Object, Object> body);
*/





    @POST(Constants.BASE_LINK + "createorder.php")
    Call<OrderResponse> createorder(@Body HashMap<Object,Object> body);
    @POST(Constants.BASE_LINK + "createorder.php")
    Call<OrderResponseCod> createorderCod(@Body HashMap<Object,Object> body);


    @POST(Constants.BASE_LINK + "createorder.php")
    Call<OrderResponseWallet> createorderWallet(@Body HashMap<Object,Object> body);

    @POST(Constants.BASE_LINK + "statusupdate.php")
    Call<universalMessage> sendoredretoserver(@QueryMap Map<String, String> query);


    @GET(Constants.BASE_LINK + "getcoupon.php")
    Call<ApplyCoupenResult> applycode(@QueryMap Map<String, String> query);


    @GET(Constants.BASE_LINK + "getshipping-estimation.php")
    Call<GetShippingCharge> getshippingchargebycountry(@QueryMap Map<String, String> query);

    @GET(Constants.BASE_LINK + "filterapi.php")
    Call<FilterDetail> getFilterCatagoryList(@QueryMap Map<String, String> query);


    @POST(Constants.BASE_LINK + "filter.php")
    Call<CatagoryList> getFilterProductList(@Body HashMap<Object,Object> body);



    @FormUrlEncoded
    @POST(Constants.HOST_LINK+"vsms/otp/send/")
    Call<OtpResponse> sendotp(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.HOST_LINK+"vsms/otp/verify/")
    Call<VerificationResponse> otpverification(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.HOST_LINK+"vsms/otp/sendaddress/")
    Call<OtpResponse> otp_for_address_mob_change(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.HOST_LINK+"vsms/otp/sendforgotpassword/")
    Call<OtpResponse> sendotpforresetPassword(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.HOST_LINK+"vsms/otp/sendlogin/")
    Call<OtpResponse> sendotpforLogin(@FieldMap Map<String, String> map1);
    @FormUrlEncoded
    @POST(Constants.HOST_LINK+"vsms/otp/verifyforgotpassword/")
    Call<Verifyforgetpasswordresponse> otpverificationforresetPassword(@FieldMap Map<String, String> map1);



    @FormUrlEncoded
    @POST(Constants.HOST_LINK+"vsms/otp/verifylogin/")
    Call<Verifyforgetpasswordresponse> otpverificationforLogin(@FieldMap Map<String, String> map1);



    @FormUrlEncoded
    @POST(Constants.HOST_LINK+"vsms/otp/verifyaddress/")
    Call<Verifyforgetpasswordresponse> otpverificationfoAddress(@FieldMap Map<String, String> map1);


    @GET(Constants.BASE_LINK +"help.php")
    Call<HelpSupport> getHelpSupport();


    @GET(Constants.BASE_LINK+"contact.php")
    Call<ContactMessage> contactMessage(@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST(Constants.BASE_LINK+"getcustomerorders.php")
    Call<OrderListResponse> orderList(@FieldMap Map<String, String> map1);



    @FormUrlEncoded
    @POST(Constants.BASE_LINK+"getcustomertrackingorder.php")
    Call<OrderListResponse> orderTrackingList(@FieldMap Map<String, String> map1);


    @FormUrlEncoded
    @POST(Constants.BASE_LINK+"ordercancel.php")
    Call<CancelOrderListResponse> canceorderList(@FieldMap Map<String, String> map1);
    @FormUrlEncoded
    @POST(Constants.BASE_LINK+"createreorder.php")
    Call<ReorderResponse> reorder(@FieldMap Map<String, String> map1);




    @FormUrlEncoded
    @POST(Constants.BASE_LINK+"paymentstatus.php")
    Call<WalletPaymentStatus> getpaymentstatus(@FieldMap Map<String, String> map1);

    @FormUrlEncoded
    @POST(Constants.BASE_LINK+"getremembermecards.php")
    Call<RememberCardName> getremembercards(@FieldMap Map<String, String> map1);
}