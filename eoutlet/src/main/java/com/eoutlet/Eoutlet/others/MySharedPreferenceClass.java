package com.eoutlet.Eoutlet.others;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreferenceClass {

    private static String MY_USERID = "UserID";

    private static String FIRST_NAME = "fname";
    private static String LAST_NAME = "lname";

    private static String MOBILE= "mobile";

    private static String USERNAME = "username";

    private static String EMAIL = "email";

    private static String BEDGE_COUNT = "bedgecount";
    private static String ADDRESSNAME = "addressname";
    private static String ADDRESSFIRSTNAME = "addressfirstname";
    private static String ADDRESSLASTNAME = "addresslastname";


    private static String STREET = "street";
    private static  String CITY = "city";
    private  static String COUNTRY = "country";
    private  static String ADDRESSPHONE = "addressphon";
    private static String COUNTRYID = "countryid";
    private static String SELECTEDID = "selectedid";
    private static String MASK_KEY = "maskkey";
    private static String CART_ID = "cartid";
    private static String TOKEN_NAME= "tokenname";
    private static String WALLET_AMOUNT= "amount";


    private static String CHECKBOX_FLAG= "checkboxflag";

    public static String getWalletamount(Context context) {
        return getPrefs(context).getString(WALLET_AMOUNT," ");
    }


    public static void setWalletAmount(Context context,String amount) {
        getPrefs(context).edit().putString(WALLET_AMOUNT,amount).commit();

    }
    public static String getSelectedTokenName(Context context) {
        return getPrefs(context).getString(TOKEN_NAME," ");
    }

    public static void setSelecteTokenName(Context context,String tokenname) {
        getPrefs(context).edit().putString(TOKEN_NAME,tokenname).commit();

    }

    public static void setCheckbofflag(Context context,boolean checkboxflag) {
        getPrefs(context).edit().putBoolean(CHECKBOX_FLAG,checkboxflag).commit();

    }
    public static boolean getCheckboxflag(Context context) {
        return getPrefs(context).getBoolean(CHECKBOX_FLAG,false);
    }






    public static String getSelectedAddressId(Context context) {
        return getPrefs(context).getString(SELECTEDID,"0");
    }

    public static void setSelecteAddressdId(Context context,String selectedid) {
        getPrefs(context).edit().putString(SELECTEDID, selectedid).commit();

    }

    public static String getMaskkey(Context context) {


        return getPrefs(context).getString(MASK_KEY, null);


    }

    public static  void setMaskkey(Context context,String maskkey){
        getPrefs(context).edit().putString(MASK_KEY,maskkey).commit();




    }
    public static int getCartId(Context context) {


        return getPrefs(context).getInt(CART_ID, 0);


    }

    public static  void setCartId(Context context,int cartId) {
        getPrefs(context).edit().putInt(CART_ID, cartId).commit();

    }




        public static String getCountryId(Context context) {
        return getPrefs(context).getString(COUNTRYID,null);
    }

    public static void setCountryId(Context context,String countryId) {
        getPrefs(context).edit().putString(COUNTRYID, countryId).commit();

    }

    public static String getAdressphone(Context context) {
        return getPrefs(context).getString(ADDRESSPHONE,null);
    }

    public static void setAddressphone(Context context,String addressphone) {
        getPrefs(context).edit().putString(ADDRESSPHONE, addressphone).commit();

    }

    public static String getfirstAdressname(Context context) {
        return getPrefs(context).getString(ADDRESSFIRSTNAME,null);
    }

    public static void setfirstAdressname(Context context,String addressname) {
        getPrefs(context).edit().putString(ADDRESSFIRSTNAME, addressname).commit();

    }

    public static String getlastAdressname(Context context) {
        return getPrefs(context).getString(ADDRESSLASTNAME,null);
    }

    public static void setlastAdressname(Context context,String addressname) {
        getPrefs(context).edit().putString(ADDRESSLASTNAME, addressname).commit();

    }


    public static String getAdressname(Context context) {
        return getPrefs(context).getString(ADDRESSNAME,null);
    }

    public static void setAddressname(Context context,String addressname) {
        getPrefs(context).edit().putString(ADDRESSNAME, addressname).commit();

    }

    public static String getStreetname(Context context) {
        return getPrefs(context).getString(STREET, null);
    }

    public static void setStreetName(Context context,String street) {
        getPrefs(context).edit().putString(STREET, street).commit();

    }

    public static String getCityName(Context context) {
        return getPrefs(context).getString(CITY, null);
    }

    public static void setCityname(Context context,String city) {
        getPrefs(context).edit().putString(CITY,city).commit();

    }


    public static String getCountryName(Context context) {
        return getPrefs(context).getString(COUNTRY, null);
    }

    public static void setCountryname(Context context, String countryname) {
        getPrefs(context).edit().putString(COUNTRY, countryname).commit();

    }








    public static int getBedgeCount(Context context) {
      return getPrefs(context).getInt(BEDGE_COUNT, 0);
    }

    public static void setBedgeCount(Context context,int bedgeCount) {
        getPrefs(context).edit().putInt(BEDGE_COUNT, bedgeCount).commit();

    }



    public static void setMobileNumber(Context context, String value) {


        getPrefs(context).edit().putString(MOBILE, value).commit();


    }

    public static String getMobileNumber(Context context) {


        return getPrefs(context).getString(MOBILE, " ");


    }








    public static String getMyUserName(Context context) {

        return getPrefs(context).getString(USERNAME, null);
    }

    public static void setMyUserName(Context context, String value) {
        // perform validation etc..
        getPrefs(context).edit().putString(USERNAME, value).commit();
    }









    public static String getEmail(Context context) {

        return getPrefs(context).getString(EMAIL, null);
    }

    public static void setEmail(Context context, String value) {
        // perform validation etc..
        getPrefs(context).edit().putString(EMAIL, value).commit();

    }



    @SuppressWarnings("static-access")
    public static SharedPreferences getPrefs(Context context) {




        return context.getSharedPreferences("UserDetails", context.MODE_PRIVATE);
    }

    ///////////////////////////////////////////////////////////
    //////////////For User ID/////////////////////////////////
    ///////////////////////////////////////////////////////////

    public static String getMyUserId(Context context) {

        return getPrefs(context).getString(MY_USERID, " ");
    }

    public static void setMyUserId(Context context, String value) {
        // perform validation etc..
        getPrefs(context).edit().putString(MY_USERID, value).commit();
    }





    public static void clear(Context context) {
        getPrefs(context).edit().clear().commit();
    }



    public static String getMyFirstNamePref(Context context) {

        return getPrefs(context).getString(FIRST_NAME, null);
    }

    public static void setMyFirstNamePref(Context context, String value) {
        // perform validation etc..
        getPrefs(context).edit().putString(FIRST_NAME, value).commit();
    }

    public static String getMyLastNamePref(Context context) {

        return getPrefs(context).getString(LAST_NAME, null);
    }

    public static void setMyLastNamePref(Context context, String value) {
        // perform validation etc..
        getPrefs(context).edit().putString(LAST_NAME, value).commit();
    }








}