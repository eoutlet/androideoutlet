package com.eoutlet.Eoutlet.adpters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.fragments.NewOrderListFragment;
import com.eoutlet.Eoutlet.pojo.OrderListItem;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewOrderListAdapter extends RecyclerView.Adapter<NewOrderListAdapter.ViewHolder> {

    private Context context;
    private List<OrderListItem> customDataList;
    private NewOrderListFragment testFragment;

    boolean isOpened=false;
    private int expandedPosition = -1;
    private OnItemClickListener onItemClickListener;
    private boolean mExpanded;
    private RecyclerView recyclerView = null;

    public NewOrderListAdapter(Context context, List<OrderListItem> customDataList, NewOrderListFragment testFragment) {
        this.customDataList = customDataList;
        this.context = context;
        this.testFragment = testFragment;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Boolean value);
    }

    @NonNull
    @Override
    public NewOrderListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.new_order_list_item, parent, false);
        return new NewOrderListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NewOrderListAdapter.ViewHolder holder, final int position) {
        holder.orderid.setText("طلب رقم:" +" "+ customDataList.get(position).getIncrementOrderId());
        holder.totalamount.setText(" SAR "+" "+ customDataList.get(position).getTotalAmount()+" ");
        holder.orderstatus.setText ( customDataList.get(position).getStatus());
        holder.date.setText(customDataList.get(position).getCreated_at());

        if (customDataList.get(position).getStatus().equalsIgnoreCase("canceled")){
            holder.orderstatus.setTextColor(context.getResources().getColor(R.color.canceled));
            holder.orderstatus.setText("ملغي");
        }
        else if (customDataList.get(position).getStatus().equalsIgnoreCase("pending")){
            holder.orderstatus.setTextColor(context.getResources().getColor(R.color.pending));
            holder.orderstatus.setText("جاري المعالجة");
        }
        else {
            holder.orderstatus.setTextColor(context.getResources().getColor(R.color.success));
            holder.orderstatus.setText("تم التوصيل");
        }

        if (customDataList.get(position).getStatus().equalsIgnoreCase("canceled")){
            holder.textcancel.setVisibility(View.GONE);
        }
        else {
            holder.textcancel.setVisibility(View.VISIBLE);
        }




        final boolean isExpanded = position==expandedPosition;
        holder.layoutChild.setVisibility(isExpanded?View.VISIBLE:View.GONE);
        holder.itemView.setActivated(isExpanded);

        holder.textcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCancelDialogue(customDataList.get(position).getOrderId(),position);

            }
        });



        holder.textreorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NewOrderListFragment)testFragment).doReorder(customDataList.get(position).getOrderId(),position);

            }
        });


        holder.layoutChild.setVisibility(isExpanded?View.VISIBLE:View.GONE);
        holder.itemView.setActivated(isExpanded);
        holder.mainHeaderView.setOnClickListener(new View.OnClickListener() {

            ImageView childitemimage;
            private RelativeLayout mainHeaderView;
            TextView childorderId,childtotalamount,childsize,childcolor,childqty,childorderName,previeworderId;
            @Override
            public void onClick(View v) {




                expandedPosition = isExpanded ? -1:position;

                TransitionManager.beginDelayedTransition(recyclerView);
                notifyDataSetChanged();
                holder.layoutChild.removeAllViews();
                for (int i = 0; i < customDataList.get(position).getItems().size(); i++) {
                    View view = testFragment.getLayoutInflater().inflate(R.layout.child_order_list, null);
                    childorderId = view.findViewById(R.id.childorderId);
                    childtotalamount = view.findViewById(R.id.childtotalamount);
                    childsize = view.findViewById(R.id.childsize);
                    childcolor = view.findViewById(R.id.childcolor);
                    childqty = view.findViewById(R.id.childqty);
                    childitemimage = view.findViewById(R.id.childitemimage);
                    childorderName = view.findViewById(R.id.childorderName);
                    previeworderId  =view.findViewById(R.id.previeworderid);
                    childorderName.setText(customDataList.get(position).getItems().get(i).getName());
                    childorderId.setText("رقم الأمر:" +" "+ customDataList.get(position).getItems().get(i).getSku());
                    childtotalamount.setText("قيمة الطلب:"+" "+ customDataList.get(position).getItems().get(i).getPrice()+" SAR");




                    if(customDataList.get(position).getItems().get(i).getSize()!="") {
                        //childsize.setText("|"+" "+"المقاس:" + " " + customDataList.get(position).getItems().get(i).getSize());
                        childsize.setText("المقاس:" + " " + customDataList.get(position).getItems().get(i).getSize()+" "+" |");
                    }
                    else{
                        childsize.setVisibility(View.GONE);

                    }
                    if(customDataList.get(position).getItems().get(i).getColor()!="") {
                        // childcolor.setText("|"+" "+"اللون:" + " " + customDataList.get(position).getItems().get(i).getColor());
                        childcolor.setText("اللون:" + " " + customDataList.get(position).getItems().get(i).getColor()+" "+ "|"+" ");
                    }
                    else{
                        childcolor.setVisibility(View.GONE);

                    }


                    childqty.setText("الكمية:" + " " + customDataList.get(position).getItems().get(i).getOrderedQty());


                    if(i==0) {
                        previeworderId.setText("رقم الأمر:" + " " + customDataList.get(position).getOrderId());
                    }



                    else
                    {
                        previeworderId.setVisibility(View.GONE);
                    }


                    Picasso.get().load(customDataList.get(position).getItems().get(i).getImageUrl()).into(childitemimage);



                    holder.layoutChild.addView(view);
                }



//                notifyItemChanged(position);
//                n


            }
        });



    }

    private void openCancelDialogue(final String orderId, final int position) {




        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage("هل أنت متأكد من الغاء الطلب");


        alertDialogBuilder.setPositiveButton("نعم",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        ((NewOrderListFragment)testFragment).callCancelOrderApi(orderId,position);

                    }
                });

        alertDialogBuilder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {
                //alertDialog.dismiss();

            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

    }


    @Override
    public int getItemCount() {
        return customDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layoutChild;
        TextView textPreview,orderid,totalamount,orderstatus,textcancel,textreorder,date;
        ImageView arrow_imageview;
        private RelativeLayout    mainHeaderView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layoutChild = itemView.findViewById(R.id.layoutChild);
        /*layoutChild = itemView.findViewById(R.id.layoutChild);
        textPreview = itemView.findViewById(R.id.textPreview);*/
            orderid = itemView.findViewById(R.id.orderid);
            totalamount = itemView.findViewById(R.id.totalamount);
            orderstatus = itemView.findViewById(R.id.orderstatus);
            textcancel = itemView.findViewById(R.id.textcancel);
            textreorder = itemView.findViewById(R.id.textreorder);
            arrow_imageview = itemView.findViewById(R.id.arrow_imageview);
            date = itemView.findViewById(R.id.date);
            mainHeaderView = itemView.findViewById(R.id.mainHeaderView);







        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.recyclerView = recyclerView;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}


