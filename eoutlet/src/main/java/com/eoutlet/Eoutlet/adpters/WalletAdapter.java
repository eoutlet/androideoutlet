package com.eoutlet.Eoutlet.adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.pojo.Transaction;


import java.util.List;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.MyViewHolder> {

    ExecuteFragment execute;

    private List<Transaction> transaction;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView orderid,amount,date;
        public ImageView iv;
        LinearLayout onSelection;

        public MyViewHolder(View view) {
            super(view);
          orderid  = view.findViewById(R.id.latorderid);
          amount= view.findViewById(R.id.latamount);
            date = view.findViewById(R.id.latdate);


        }
    }

    public WalletAdapter(Context context, List<Transaction> mydataset) {


        execute = (MainActivity) context;
        transaction = mydataset;


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_wallet_item, parent, false);

        return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder( MyViewHolder holder, final int position) {


        holder.orderid.setText("ORDER #"+" "+transaction.get(position).orderId);
        holder.amount.setText("SAR"+" "+transaction.get(position).amount.toString());
        //holder.date.setText(transaction.get(position).createAt.split(" ")[0]);

        holder.date.setText(transaction.get(position).createAt);
        /* holder.orderid.setText("Order"+ "201667677");
        holder.amount.setText("SAR"+" "+"270.0");
        holder.date.setText("20/07/2018");*/


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        //return 10;
        return transaction.size();
    }


}