package com.eoutlet.Eoutlet.adpters;

import android.content.Context;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.pojo.Datum;
import com.eoutlet.Eoutlet.pojo.HomeCatagory1Param;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeAdapter1  extends RecyclerView.Adapter<HomeAdapter1.MyViewHolder> {
List<String> mydataset;
    Context mContext;
    ExecuteFragment execute;
    ViewListener viewListener;
    TextView lastselectedtextview;

    int lastselectedposution = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre,discount;
        public ImageView iv;
        LinearLayout onSelection;

        public MyViewHolder(View view) {
            super(view);
           iv = view.findViewById(R.id.home1);
           title = view.findViewById(R.id.catagoryname1);
           discount = view.findViewById(R.id.discount);
           onSelection = view.findViewById(R.id.selectCatagory1);




        }}
    public void passData(Context context) {
        mContext = context;
        execute = (MainActivity)mContext;
    }
    public HomeAdapter1(Context context, List<String>  mydataset, ViewListener viewListener) {
        this.mydataset = mydataset;
        mContext = context;
        execute = (MainActivity)context;
        this.viewListener = viewListener;

    }



    @Override
    public HomeAdapter1.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_item1, parent, false);

        return new HomeAdapter1.MyViewHolder(itemView);



    }

    @Override
    public void onBindViewHolder(HomeAdapter1.MyViewHolder holder, final int position) {


      /*  Glide.with(holder.iv)
                .load(mydataset.get(position).image)
                .apply(RequestOptions.bitmapTransform(new RoundedCorners(55)))
                .into(holder.iv);*/

       // Picasso.get().load(mydataset.get(position).image).resize(230,230).transform(new RoundedCornersTransform(this)).into(holder.iv);

      holder.title.setText(mydataset.get(position));
      if(position==0  && lastselectedtextview==null){


          //holder.title.setTextColor(Color.parseColor("#191919"));
          lastselectedtextview = holder.title;




      }
       // holder.discount.setText(mydataset.get(position).caption);


        holder.onSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            viewListener.onClick(position, v);
            if(lastselectedtextview!=holder.title) {
                holder.title.setTextColor(Color.parseColor("#191919"));
                lastselectedtextview.setTextColor(Color.parseColor("#A1A1A1"));
                lastselectedtextview = holder.title;

            }



            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mydataset.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




}
