package com.eoutlet.Eoutlet.adpters;



import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.listener.ViewListener;


import java.util.List;

public class NewProfileAdapter extends RecyclerView.Adapter<NewProfileAdapter.MyViewHolder> {

    Context mContext;
    public List<String> profiletext;

    List<Integer> profileIcon;
    ExecuteFragment execute;
    int mydataset[];
    ViewListener viewlistener;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        public ImageView iv;

        LinearLayout edtprofileItemClick;
        public LinearLayout linearbackground;

        public MyViewHolder(View view) {
            super(view);
            iv = view.findViewById(R.id.profileImage);
            title = view.findViewById(R.id.profileName);
            linearbackground = view.findViewById(R.id.red_background);
            edtprofileItemClick = view.findViewById(R.id.profileItemClick);



        }
    }

    public void passData(Context context) {
        mContext = context;
        execute = (MainActivity) mContext;
    }

    public NewProfileAdapter(Context context,List<String> profiletext, int profileicon[], ViewListener viewListener) {

        this.profiletext = profiletext;
        this.mydataset = profileicon;
        mContext = context;
        this.viewlistener = viewListener;


    }


    @Override
    public NewProfileAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_profile_item, parent, false);

        return new NewProfileAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewProfileAdapter.MyViewHolder holder, final int position) {




     /*   if(position == 4){

            holder.edtprofileItemClick.setVisibility(View.GONE);



        }*/
        holder.title.setText(profiletext.get(position));
        //Picasso.get().load(profileIcon.get(position)).into(holder.iv);
        holder.iv.setImageDrawable(mContext.getResources().getDrawable(mydataset[position]));
        //holder.iv.setImageResource(mydataset[position]);


        holder.edtprofileItemClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewlistener.onClick(position, v);
            }
        });




    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return profiletext.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

}