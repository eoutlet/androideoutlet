package com.eoutlet.Eoutlet;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;

import java.util.HashMap;
import java.util.Map;

public class AFApplication extends Application {
    private static final String AF_DEV_KEY = "Qy2rqKhCQPVNLdyt7Rb5ra";
    public static String shortlink1 = " ";

    @Override
    public void onCreate() {
        super.onCreate();
        AppsFlyerConversionListener conversionListener = new AppsFlyerConversionListener() {


            @Override
            public void onConversionDataSuccess(Map<String, Object> conversionData) {

                for (String attrName : conversionData.keySet()) {
                    Log.d("LOG_TAG", "attribute: " + attrName + " = " + conversionData.get(attrName));


                }

            }

            @Override
            public void onConversionDataFail(String errorMessage) {
                Log.d("LOG_TAG", "error getting conversion data: " + errorMessage);
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> conversionData) {

                for (String attrName : conversionData.keySet()) {
                    Log.d("LOG_TAG", "attribute: " + attrName + " = " + conversionData.get(attrName));


                    if(attrName.equals("shortlink")) {
                        shortlink1 = conversionData.get(attrName) + " ";
                        Toast.makeText(getBaseContext(), "" + shortlink1, Toast.LENGTH_SHORT).show();
                    }

                }

            }


            @Override
            public void onAttributionFailure(String errorMessage) {
                Log.d("LOG_TAG", "error onAttributionFailure : " + errorMessage);
            }
        };

        AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionListener, getApplicationContext());
        AppsFlyerLib.getInstance().startTracking(this);
        AppsFlyerLib.getInstance().setDebugLog(true);


/*        //ADD_TO_CART

        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.PRICE, 350);
        eventValue.put(AFInAppEventParameterName.CONTENT_ID, "001");
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, "shirt");
        eventValue.put(AFInAppEventParameterName.CURRENCY, "USD");
        eventValue.put(AFInAppEventParameterName.QUANTITY, 3);
        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.ADD_TO_CART, eventValue);


        //Purchase

        Map<String, Object> eventValuepurchase = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.PRICE, 350);
        eventValue.put(AFInAppEventParameterName.CONTENT_ID, "221");
// for multiple product categories, set the param value as: // new String {"221", "124"}
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, "shirt");
// for multiple product categories,, set the param value as: new String {"shoes", "pants"}
        eventValue.put(AFInAppEventParameterName.CURRENCY, "USD");
        eventValue.put(AFInAppEventParameterName.QUANTITY, 2);
// for multiple product categories, set the param value as: new int {2, 5}
        eventValue.put(AFInAppEventParameterName.RECEIPT_ID, "X123ABC");
        eventValue.put("af_order_id", "X123ABC");
        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.PURCHASE, eventValuepurchase);*/

    }
}
