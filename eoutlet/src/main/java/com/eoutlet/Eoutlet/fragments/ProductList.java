package com.eoutlet.Eoutlet.fragments;

import android.app.Dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;


import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.adpters.FilterListadapeter;
import com.eoutlet.Eoutlet.adpters.ProductListAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.api.request.CatagoryList;
import com.eoutlet.Eoutlet.api.request.ListItems;

import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.listener.PaginationListener;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.pojo.Child;
import com.eoutlet.Eoutlet.pojo.Filtermainlist;
import com.eoutlet.Eoutlet.pojo.HomeCatagory1Param;
import com.eoutlet.Eoutlet.viewmodels.ProductListViewModel;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ProductList extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    Context mContext;
    private ExecuteFragment execute;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView productListRecycler, filterlstrecycler;
    private ProductListAdapter mAdapter;


    GridLayoutManager lm;

    private String name, fromwhere;
    private int currentpage = 1;
    boolean view_items_flag = true;
    private Dialog dialog;
    private String item_catagory_name;
    private FirebaseAnalytics mFirebaseAnalytics;
    private int filterpage = 1;

    private HomeCatagory1Param filterlist = new HomeCatagory1Param();
    private List<Child> childlist = new ArrayList<>();
    private List<com.eoutlet.Eoutlet.pojo.CatagoryList> childcatagorylist = new ArrayList<>();
    private List<String> childtitle = new ArrayList<>();
    private List<String> childid = new ArrayList<>();
    private SearchView searchView;
    List<List<String>> mainCatList = new ArrayList<>();
    List<List<String>> mainCatListclone = new ArrayList<>();
    private PaginationListener scrollListener;
    private List<Filtermainlist> fullList = new ArrayList<>();
    private FilterListadapeter madapter;
    private boolean scrollflag = false;
    View toolbarbeg;
    private int counter = 0;

    boolean showappprogress = true;

    public int sortingflag = 99;
    private ImageView tick_image, tick_image2, tick_image3, tick_image4;


    public ParentDialogsListener parentDialogListener;
    private List<ListItems> catagoryList = new ArrayList<>();
    private List<ListItems> filtercatagoryList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private LinearLayout filterbutton, sortbutton;
    private ProductListViewModel model;
    private List<ListItems> productListmain = new ArrayList<>();
    private List<ListItems> recentlyarrivedList = new ArrayList<>();
    private List<ListItems> higtolowlist = new ArrayList<>();
    private List<ListItems> lowtohighlist = new ArrayList<>();

    private List<ListItems> filterproductList = new ArrayList<>();
    private View v;
    private TextView recentlyarrived, hightolow, lowtohigh, defaultlist, toolbarname;
    static int searchflage = 0;
    private int value;
    private String adapterposition = "";
    String subcatagoryid = " ";
    int subcatagorypage = 1;
    TextView sorttext, filtertitle, filtercounter;
    private static int isGetAllProductlist = 1;
    private LinearLayout filteapplybtn, sortapplybbtn;

    int selectedtopcategory = 99;

    private HashMap<String, List<String>> selectedvalue = new HashMap();
    // private List<Integer> countlist = new ArrayList<>();
    private HashMap<String, Integer> countlist = new HashMap<>();
    private String selectmanufec, selectesize, selectitemtype;

    private ImageView searchImage, backarrow, sortimg, filterimg;
    private Toolbar toolbar1;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        try {

            parentDialogListener = (ParentDialogsListener) context;

        } catch (ClassCastException e) {

            throw new ClassCastException(context.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }


    private OnFragmentInteractionListener mListener;

    public ProductList() {

    }

    public void passData(Context context) {
        mContext = context;
        execute = (MainActivity) mContext;
    }


    // TODO: Rename and change types and number of parameters
    public static ProductList newInstance(String param1, String param2) {
        ProductList fragment = new ProductList();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (v == null) {
            v = inflater.inflate(R.layout.fragment_product_list, container, false);
            value = getArguments().getInt("productId");
            name = getArguments().getString("name");
            adapterposition = getArguments().getString("adapterposition");

            if (adapterposition != null) {
                selectedtopcategory = Integer.valueOf(adapterposition);
            }

            fromwhere = getArguments().getString("fromwhere");
            initViews(v);


            lm = new GridLayoutManager(v.getContext(), 2) {
                @Override
                protected boolean isLayoutRTL() {
                    return true;
                }
            };



        /*This is pagination part used to load limited items at a time and load more items on page

        scroll  */
            scrollListener = new PaginationListener(
                    lm) {
                @Override
                public void onLoadMore(int current_page) {


                    //model.getProductListData(value, getContext(),currentpage);

                    if (isGetAllProductlist == 1) {
                        currentpage++;
                        getProductList(String.valueOf(value), currentpage);

                    } else if (isGetAllProductlist == 2) {


                        filterpage++;
                        if ((mainCatList.size() > 0 && fullList.size() > 0)) {
                            getfilterdata(filterpage);
                        }


                    } else if (isGetAllProductlist == 3) {
                        subcatagorypage++;
                        getProductListforsubCatagory(subcatagoryid, subcatagorypage);


                    }
                }

            };

            productListRecycler.addOnScrollListener(scrollListener);
            scrollListener.resetState();


            //childtitle.add(" فلاتر البحث");
            //childtitle.add(" ترتيب حسب");


            if (fromwhere.equals("fromhome")) {
                childlist = (List) getArguments().getSerializable("childeren");

                if (childlist != null) {


                    for (int i = 0; i < childlist.size(); i++) {

                        childid.add(childlist.get(i).id);
                        childtitle.add(childlist.get(i).name);


                    }
                }

                initfilterListRecycler(childtitle, counter, selectedtopcategory);


            } else if (fromwhere.equals("fromcatagory")) {
                childcatagorylist = (List) getArguments().getSerializable("childeren");

                if (childcatagorylist != null) {


                    for (int i = 0; i < childcatagorylist.size(); i++) {


                        childtitle.add(childcatagorylist.get(i).name);
                        childid.add(childcatagorylist.get(i).id);
                        /*childid.add(childlist.get(i).id);*/


                    }
                }

                initfilterListRecycler(childtitle, counter, selectedtopcategory);


            } else {

                initfilterListRecycler(childtitle, counter, selectedtopcategory);


            }


            // tooltext.setText(name);
            currentpage = 1;
            scrollListener.resetState();
            PaginationListener.current_page = 1;
            getProductList(String.valueOf(value), 1);


            sortbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


//                    initSortDialogue();


                }
            });

            filterbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    DialogFragment prFrag = new FilterFragmentNew();
                    Bundle bund = new Bundle();

                    bund.putInt("catId", value);
                    prFrag.setArguments(bund);


                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);


                    prFrag.setTargetFragment(ProductList.this, 101);
                    prFrag.show(ft, "dialog  ");


                }
            });
            backarrow = toolbar1.findViewById((R.id.backarrow));

            searchImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Fragment prFrag = new SearchResultFragment();
                    Bundle databund = new Bundle();


                    getFragmentManager()
                            .beginTransaction().addToBackStack(null)
                            .replace(R.id.containerdetail, prFrag)
                            .commit();


                }
            });

            backarrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().popBackStack();
                }
            });


        }
        iniatializesortDialogue();

        return v;
    }

    private void iniatializesortDialogue() {
        Handler mHandler;
        dialog = new Dialog(getContext(), R.style.CustomDialogAnimation);


        dialog.setContentView(R.layout.sort_design);
        mHandler = new Handler();
        /*DisplayMetrics displayMetrics = new DisplayMetrics();

      int  height = displayMetrics.heightPixels;
      int  width = displayMetrics.widthPixels;
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, height / 2);*/
        if (dialog != null) {
            Window window = dialog.getWindow();

            //  dialog.getWindow().setBackgroundDrawableResource(android.R.color.black);
            if (window != null) {

                // dialog.getWindow().setDimAmount(0.5f);


                WindowManager.LayoutParams params = window.getAttributes();
                params.width = WindowManager.LayoutParams.MATCH_PARENT;
                params.height = WindowManager.LayoutParams.WRAP_CONTENT;

                params.horizontalMargin = 10f;

                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);

                params.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
                params.y = 380;

                params.dimAmount = 0;
                params.dimAmount = 0.2f;
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_drawable);
                // params.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
                window.setAttributes(params);
            }

            recentlyarrived = dialog.findViewById(R.id.recentlyarrived);
            hightolow = dialog.findViewById(R.id.hightolow);
            lowtohigh = dialog.findViewById(R.id.lowtohigh);
            defaultlist = dialog.findViewById(R.id.defaultlist);
            tick_image = dialog.findViewById(R.id.tick_image);
            tick_image2 = dialog.findViewById(R.id.tick_image2);
            tick_image3 = dialog.findViewById(R.id.tick_image3);
            tick_image4 = dialog.findViewById(R.id.tick_image4);


            defaultlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //Update the value background thread to UI thread
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            tick_image.setVisibility(View.VISIBLE);
                            tick_image2.setVisibility(View.GONE);
                            tick_image3.setVisibility(View.GONE);
                            tick_image4.setVisibility(View.GONE);
                        }
                    });


                    sortingflag = 0;
                    currentpage = 1;
                    filterpage = 1;
                    subcatagorypage = 1;

                    if (isGetAllProductlist == 1) {

                        getProductList(String.valueOf(value), currentpage);

                    } else if (isGetAllProductlist == 2) {


                        if ((mainCatList.size() > 0 && fullList.size() > 0)) {
                            getfilterdata(filterpage);
                        }


                    } else if (isGetAllProductlist == 3) {

                        getProductListforsubCatagory(subcatagoryid, subcatagorypage);


                    }


                    dialog.cancel();


                }
            });


            recentlyarrived.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tick_image.setVisibility(View.GONE);
                    tick_image2.setVisibility(View.VISIBLE);
                    tick_image3.setVisibility(View.GONE);
                    tick_image4.setVisibility(View.GONE);

                    sortingflag = 1;
                    currentpage = 1;
                    filterpage = 1;
                    subcatagorypage = 1;

                    if (isGetAllProductlist == 1) {
                        getProductList(String.valueOf(value), currentpage);

                    } else if (isGetAllProductlist == 2) {

                        if ((mainCatList.size() > 0 && fullList.size() > 0)) {
                            getfilterdata(filterpage);
                        }


                    } else if (isGetAllProductlist == 3) {

                        getProductListforsubCatagory(subcatagoryid, subcatagorypage);


                    }


                    dialog.cancel();


                }
            });

            lowtohigh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tick_image.setVisibility(View.GONE);
                    tick_image2.setVisibility(View.GONE);
                    tick_image3.setVisibility(View.VISIBLE);
                    tick_image4.setVisibility(View.GONE);
                    sortingflag = 2;
                    currentpage = 1;

                    filterpage = 1;
                    subcatagorypage = 1;

                    if (isGetAllProductlist == 1) {

                        getProductList(String.valueOf(value), currentpage);

                    } else if (isGetAllProductlist == 2) {


                        if ((mainCatList.size() > 0 && fullList.size() > 0)) {
                            getfilterdata(filterpage);
                        }


                    } else if (isGetAllProductlist == 3) {

                        getProductListforsubCatagory(subcatagoryid, subcatagorypage);


                    }


                    dialog.cancel();


                }
            });

            hightolow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tick_image.setVisibility(View.GONE);
                    tick_image2.setVisibility(View.GONE);
                    tick_image3.setVisibility(View.GONE);
                    tick_image4.setVisibility(View.VISIBLE);

                    sortingflag = 3;
                    currentpage = 1;
                    filterpage = 1;
                    subcatagorypage = 1;

                    if (isGetAllProductlist == 1) {

                        getProductList(String.valueOf(value), currentpage);

                    } else if (isGetAllProductlist == 2) {


                        if ((mainCatList.size() > 0 && fullList.size() > 0)) {
                            getfilterdata(filterpage);
                        }


                    } else if (isGetAllProductlist == 3) {

                        getProductListforsubCatagory(subcatagoryid, subcatagorypage);


                    }


                    dialog.cancel();


                }
            });


        }

    }


    public void initViews(View v) {
        productListRecycler = v.findViewById(R.id.product_list_Recycler);
        filterlstrecycler = v.findViewById(R.id.filterlistrecycler);
        filterbutton = v.findViewById(R.id.filrterbtn);
        sortbutton = v.findViewById(R.id.sort);
        sorttext = v.findViewById(R.id.sorttitle);
        filtertitle = v.findViewById(R.id.filtertitle);
        filteapplybtn = v.findViewById(R.id.filterapplybtn);
        sortapplybbtn = v.findViewById(R.id.sortapplybtn);
        sortimg = v.findViewById(R.id.sortimg);
        filterimg = v.findViewById(R.id.filterimg);
        filtercounter = v.findViewById(R.id.filtercounter);

        toolbarname = v.findViewById(R.id.toolname);
        toolbar1 = v.findViewById(R.id.toolbar);
        searchImage = toolbar1.findViewById(R.id.serachbar);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        execute = (MainActivity) getActivity();

        if (name != null) {

            toolbarname.setText(name);

        }


        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);


        filteapplybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedtopcategory = 99;

                initfilterListRecycler(childtitle, counter, selectedtopcategory);

                filteapplybtn.setBackgroundResource(R.drawable.rectangular_border_black2);
                filtertitle.setTextColor(Color.parseColor("#FFFFFF"));
                filterimg.setImageResource(R.drawable.new_filter_white);
                filtercounter.setTextColor(Color.parseColor("#ffffff"));

                sortimg.setImageResource(R.drawable.ic_sort_icon_black);
                sortapplybbtn.setBackgroundResource(R.drawable.rectangular_border_corner2);
                sorttext.setTextColor(Color.parseColor("#000000"));


                DialogFragment prFrag = new FilterFragmentNew();
                Bundle bundle = new Bundle();
                Log.e("maincatList-->>", String.valueOf(mainCatList.size()));

                Log.e("countlist-->>>", String.valueOf(countlist.size()));

                bundle.putInt("catId", value);
                bundle.putString("previoussize", selectesize);
                bundle.putString("previousitemtype", selectitemtype);
                bundle.putString("previousmanufecturer", selectmanufec);
                bundle.putSerializable("prevselectedvalue", (Serializable) new HashMap<>(selectedvalue));


                bundle.putSerializable("selctecatagory", ((Serializable) new ArrayList<>(mainCatListclone)));


                bundle.putSerializable("countlist", (Serializable) new HashMap<String, Integer>(countlist));


                prFrag.setArguments(bundle);

                prFrag.setTargetFragment(ProductList.this, 101);


                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }


                prFrag.setTargetFragment(ProductList.this, 101);
                prFrag.show(ft, "dialog");


            }
        });

        sortapplybbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                selectedtopcategory = 99;
                initfilterListRecycler(childtitle, counter, selectedtopcategory);
                sortimg.setImageResource(R.drawable.ic_sort_icon_white);
                sortapplybbtn.setBackgroundResource(R.drawable.rectangular_border_black2);
                sorttext.setTextColor(Color.parseColor("#FFFFFF"));
                filteapplybtn.setBackgroundResource(R.drawable.rectangular_border_corner2);
                filtertitle.setTextColor(Color.parseColor("#000000"));
                filterimg.setImageResource(R.drawable.new_filter_black);
                filtercounter.setTextColor(Color.parseColor("#000000"));
            }
        });


    }


    /*This method is used to get the price from high to low for sorting*/


    public void gethighttolow() {
        List<ListItems> higtolowlist = new ArrayList<>();
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i = 0; i < productListmain.size(); i++) {

            if (productListmain.get(i).price.contains(",")) {
                arr.add((int) Float.parseFloat(productListmain.get(i).price.replace(",", "")));


            } else {

                arr.add((int) Float.parseFloat(productListmain.get(i).price));


            }
        }
        Log.e("productlistmain size", "size is-->" + productListmain.size());


        Collections.sort(arr);
        Set<Integer> primesWithoutDuplicates = new LinkedHashSet<Integer>(arr);
        arr.clear();
        arr.addAll(primesWithoutDuplicates);
        Log.e("arr", "Size is-->>" + arr.size());

        for (int i = 0; i < arr.size(); i++) {

            Log.e("Sorted array of price", "Array" + arr.get(i));


        }
        for (int i = arr.size() - 1; i >= 0; i--) {

            for (int j = 0; j < productListmain.size(); j++) {
                if (productListmain.get(j).price.contains(",")) {

                    int finalprice = ((int) Float.parseFloat(productListmain.get(j).price.replace(",", "")));

                    if (arr.get(i) == finalprice) {

                        higtolowlist.add(productListmain.get(j));


                    }


                } else if (arr.get(i) == ((int) Float.parseFloat(productListmain.get(j).price))) {


                    higtolowlist.add(productListmain.get(j));
                }


            }


        }


        for (int k = 0; k < higtolowlist.size(); k++) {
            Log.e("Hight to Low price is", higtolowlist.get(k).price);
        }


        catagoryList = higtolowlist;

        initRecycler(v, higtolowlist);
        dialog.cancel();

    }


    /*This method is used to get the price from low to high  for sorting*/
    public void getlowtohigh() {
        List<ListItems> lowtohighlist = new ArrayList<>();
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i = 0; i < productListmain.size(); i++) {
            if (productListmain.get(i).price.contains(",")) {
                arr.add((int) Float.parseFloat(productListmain.get(i).price.replace(",", "")));


            } else {

                arr.add((int) Float.parseFloat(productListmain.get(i).price));


            }
        }
        Log.e("productlistmain size", "sf" + productListmain.size());

        Collections.sort(arr);
        Set<Integer> primesWithoutDuplicates = new LinkedHashSet<Integer>(arr);
        arr.clear();
        arr.addAll(primesWithoutDuplicates);
        Log.e("arr", "sf" + arr.size());

        for (int i = 0; i < arr.size(); i++) {

            Log.e("Sorted final date is", arr.get(i).toString());


        }
        for (int i = 0; i < arr.size(); i++) {


            for (int j = 0; j < productListmain.size(); j++) {
                if (productListmain.get(j).price.contains(",")) {

                    int finalprice = ((int) Float.parseFloat(productListmain.get(j).price.replace(",", "")));

                    if (arr.get(i) == finalprice) {

                        lowtohighlist.add(productListmain.get(j));


                    }


                } else if (arr.get(i) == ((int) Float.parseFloat(productListmain.get(j).price))) {


                    lowtohighlist.add(productListmain.get(j));
                }


            }


        }


        for (int k = 0; k < lowtohighlist.size(); k++) {
            Log.e("low to hight price is", lowtohighlist.get(k).price);
        }


        catagoryList = lowtohighlist;


        initRecycler(v, lowtohighlist);


        dialog.cancel();


    }


    public void getsortbyrecentlyArrived() {
        ArrayList<String> arr = new ArrayList<>();

        for (int i = 0; i < productListmain.size(); i++) {
            arr.add(productListmain.get(i).created);
        }
        try {
            if (arr.size() > 0) {
                Collections.sort(arr);
                for (int i = arr.size() - 1; i >= 0; i--) {
                    for (int j = 0; j < productListmain.size(); j++) {

                        if (arr.get(i).equals(productListmain.get(j).created)) {
                            recentlyarrivedList.add(productListmain.get(j));
                        }


                    }


                }


                for (int k = 0; k < recentlyarrivedList.size(); k++) {
                    Log.e("Sorted final date is", recentlyarrivedList.get(k).created);
                }
                if (recentlyarrivedList.size() > 0) {
                    catagoryList = recentlyarrivedList;


                    initRecycler(v, recentlyarrivedList);
                } else {
                    initRecycler(v, recentlyarrivedList);
                    Toast.makeText(getContext(), "لا يوجد سجلات", Toast.LENGTH_SHORT).show();
                }
                dialog.cancel();

            }
            dialog.cancel();
        } catch (Exception e) {

            dialog.cancel();


        }
    }

    public void initRecycler(View v, final List<ListItems> catagoryList) {
        productListRecycler.setHasFixedSize(true);




/*  This code used to start  grid layout from right side of the screen as comfortable for

arabic Views*/


        productListRecycler.setLayoutManager(lm/*new GridLayoutManager(v.getContext(), 2)*/);

        // specify an adapter (see also next example)


        mAdapter = new ProductListAdapter(mContext, catagoryList, new ViewListener() {
            @Override
            public void onClick(int position, View view) {
                int id = view.getId();


                switch (id) {

                    case R.id.product_list_image://button for message
                        Fragment prFrag = new ProductDetail();
                        Bundle databund = new Bundle();
                        databund.putString("sku", catagoryList.get(position).sku);
                        databund.putString("type", catagoryList.get(position).type);
                        databund.putString("pid", catagoryList.get(position).id);
                        databund.putString("size", catagoryList.get(position).size);
                        databund.putString("color", catagoryList.get(position).color);
                        databund.putString("color_name", catagoryList.get(position).color_name);
                        databund.putSerializable("catagoryobject", catagoryList.get(position));
                        prFrag.setArguments(databund);
                        getFragmentManager()
                                .beginTransaction().addToBackStack(null)
                                .replace(R.id.containerdetail, prFrag)
                                .commit();


                        break;
                }
            }
        });

        productListRecycler.setAdapter(mAdapter);


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    //Api Calling Function to get Detail of A perticuler Item According to its Catagory Id

    public void getProductList(String productId, final int current_page) {
        //catagoryList = new ArrayList<>();

        if (showappprogress) {
            parentDialogListener.showProgressDialog();
        }

        showappprogress = true;

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map = new HashMap<>();
        map.put("cat_id", productId);
        map.put("page", String.valueOf(current_page));

        map.put("sort_by", String.valueOf(sortingflag));


        Call<CatagoryList> call = apiService.getCataGoryList(map);
        call.enqueue(new Callback<CatagoryList>() {
            @Override
            public void onResponse(Call<CatagoryList> call, Response<CatagoryList> response) {
                productListmain.addAll(response.body().data);

                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    mSwipeRefreshLayout.setRefreshing(false);

                    if (response.body().data.size() > 0) {
                        item_catagory_name = response.body().data.get(0).category_name;

                        if (view_items_flag) {
                            view_items_flag = false;
                            firebase_view_item_list();
                        }


                        if (currentpage == 1) {
                            catagoryList.clear();


                        }


                        for (int i = 0; i < response.body().data.size(); i++) {

                            catagoryList.add(response.body().data.get(i));
                        }


                        if (catagoryList.size() > 0) {
                            if (currentpage == 1) {
                                scrollListener.resetState();

                                initRecycler(v, catagoryList);
                            } else {
                                if (madapter != null) {
                                    mAdapter.notifyDataSetChanged();
                                }


                            }
                        } else {
                            //initRecycler(v, catagoryList);
                            Toast.makeText(getContext(), "لا يوجد سجلات", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        mSwipeRefreshLayout.setRefreshing(false);


                    }
                } else {
                    mSwipeRefreshLayout.setRefreshing(false);

                    parentDialogListener.hideProgressDialog();

                }


            }


            @Override
            public void onFailure(Call<CatagoryList> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                mSwipeRefreshLayout.setRefreshing(false);
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onRefresh() {


        currentpage = 1;

        //sortingflag = 0;

        //isGetAllProductlist = 1;

        PaginationListener.current_page = 1;

        scrollListener.resetState();

        catagoryList = new ArrayList<>();

        //productListmain.clear();


        //countlist.clear();

        // mainCatListclone.clear();


        //selectedvalue.clear();

        //selectmanufec = " ";
        //selectitemtype = " ";
        //selectesize = " ";


        //counter=0;
        showappprogress = false;

        // initfilterListRecycler(childtitle,counter);


        if (isGetAllProductlist == 1) {
            currentpage++;
            getProductList(String.valueOf(value), currentpage);

        } else if (isGetAllProductlist == 2) {


            filterpage++;
            if ((mainCatList.size() > 0 && fullList.size() > 0)) {
                getfilterdata(filterpage);
            }


        } else if (isGetAllProductlist == 3) {
            subcatagorypage++;
            getProductListforsubCatagory(subcatagoryid, subcatagorypage);


        }


        getProductList(String.valueOf(value), currentpage);


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        //Toast.makeText(getContext(), "From on Result", Toast.LENGTH_SHORT).show();


        if (requestCode == 101) {
            if (resultCode == 100) {


                Bundle args = data.getBundleExtra("BUNDLE");

                mainCatList.clear();
                mainCatListclone.clear();


                //countlist.clear(); //New Add


                selectesize = args.getString("size");
                selectitemtype = args.getString("itemtype");
                selectmanufec = args.getString("manufecturer");


                mainCatList = new ArrayList<>((List<List<String>>) args.getSerializable("selctecatagory"));

                for (int i = 0; i < mainCatList.size(); i++) {
                    List<String> l1 = new ArrayList<>(mainCatList.get(i));
                    mainCatListclone.add(l1);
                }


                fullList = new ArrayList<>((List<Filtermainlist>) args.getSerializable("maincatagory"));
                //countlist = new ArrayList<>((List<Integer>) args.getSerializable("countlist"));
                countlist = new HashMap<String, Integer>((HashMap<String, Integer>) args.getSerializable("countlist"));
                selectedvalue = new HashMap<>((HashMap<String, List<String>>) args.getSerializable("selectedvalue"));
                counter = 0;


                Collection<Integer> values = countlist.values();
                ArrayList<Integer> val = new ArrayList<>(values);


                for (int i = 0; i < val.size(); i++) {

                    counter = counter + val.get(i);
                    initfilterListRecycler(childtitle, counter, selectedtopcategory);
                    if (counter != 0) {

                        filtercounter.setVisibility(View.VISIBLE);
                        filtercounter.setText(String.valueOf("(" + counter + ")"));
                        filterimg.setImageResource(R.drawable.new_filter_white);


                    } else {
                        filtercounter.setVisibility(View.GONE);


                    }
                }

                if (mainCatList.size() > 0 && fullList.size() > 0) {

                    isGetAllProductlist = 2;

                    filterpage = 1;

                    getfilterdata(filterpage);
                }
            }


        }


    }


    /*Applying filter in below method after apply button click in filter class*/

    public void getfilterdata(final int filterpage) {

        parentDialogListener.showProgressDialog();


        HashMap<Object, Object> map = new HashMap<>();

        map.put("cat_id", value);
        map.put("page", String.valueOf(filterpage));
        map.put("sort_by", String.valueOf(sortingflag));


        ArrayList<Object> data = new ArrayList<>();

        if (fullList.size() > 0) {
            for (int i = 0; i < fullList.size(); i++) {
                HashMap<Object, Object> catagorydetailobject = new HashMap<>();

                catagorydetailobject.put("name", fullList.get(i).name);
                catagorydetailobject.put("code", fullList.get(i).code);

                ArrayList<Object> detaildata = new ArrayList<>();


                for (int j = 0; j < fullList.get(i).data.size(); j++) {
                    if (mainCatList.get(i).size() > 0) {

                        if (mainCatList.get(i).get(j).equals("true")) {


                            HashMap<Object, Object> internalobject = new HashMap();

                            internalobject.put("display", fullList.get(i).data.get(j).display);
                            internalobject.put("value", fullList.get(i).data.get(j).value);
                            internalobject.put("count", fullList.get(i).data.get(j).count);

                            detaildata.add(internalobject);

                        }
                    }


                }
                catagorydetailobject.put("data", detaildata);
                data.add(catagorydetailobject);


            }
        }
        map.put("data", data);


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Call<CatagoryList> call = apiService.getFilterProductList(map);
        call.enqueue(new Callback<CatagoryList>() {
            @Override
            public void onResponse(Call<CatagoryList> call, Response<CatagoryList> response) {
                if (filterpage == 1) {
                    catagoryList = new ArrayList<>();
                    scrollListener.resetState();
                }
                productListmain.clear();
                productListmain.addAll(response.body().data); //added  to get latest data in sorting

                if (response.body() != null && response.body().msg.equals("success")) {
                    parentDialogListener.hideProgressDialog();
                    mSwipeRefreshLayout.setRefreshing(false);

                    if (response.body().data.size() > 0) {

                        for (int i = 0; i < response.body().data.size(); i++) {
                            catagoryList.add(response.body().data.get(i));
                        }

                        if (catagoryList.size() > 0) {
                            if (filterpage == 1) {

                                initRecycler(v, catagoryList);
                            } else {

                                mAdapter.notifyDataSetChanged();

                            }
                        } else {
                            initRecycler(v, catagoryList);

                        }

                    } else {


                    }
                } else {
                    parentDialogListener.hideProgressDialog();
                    if (filterpage == 1) {
                        catagoryList.clear();
                        initRecycler(v, catagoryList);
                        //mAdapter.notifyDataSetChanged();
                    }

                    mSwipeRefreshLayout.setRefreshing(false);


                }


            }


            @Override
            public void onFailure(Call<CatagoryList> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                mSwipeRefreshLayout.setRefreshing(false);
                Log.e(TAG, t.toString());
                if (filterpage == 1) {
                    catagoryList.clear();
                    initRecycler(v, catagoryList);
                    //mAdapter.notifyDataSetChanged();
                }


            }
        });
    }


    public void initfilterListRecycler(final List<String> cat6, int counter, int selectedtopcategory2) {


        filterlstrecycler.setHasFixedSize(true);

        // use a linear layout manager

        LinearLayoutManager lm = new LinearLayoutManager(v.getContext(), LinearLayoutManager.HORIZONTAL, true);


        filterlstrecycler.setLayoutManager(lm);

        // specify an adapter (see also next example)
        madapter = new FilterListadapeter(getContext(), cat6, counter, selectedtopcategory2


                , new ViewListener() {
            @Override
            public void onClick(int position, View view) {
                int id = view.getId();
                selectedtopcategory = position;

                switch (id) {

                    case R.id.filterlistclick://button for message


                        sortimg.setImageResource(R.drawable.ic_sort_icon_black);
                        sortapplybbtn.setBackgroundResource(R.drawable.rectangular_border_corner2);
                        sorttext.setTextColor(Color.parseColor("#000000"));


                        filteapplybtn.setBackgroundResource(R.drawable.rectangular_border_corner2);
                        filtertitle.setTextColor(Color.parseColor("#000000"));
                        filterimg.setImageResource(R.drawable.new_filter_black);
                        filtercounter.setTextColor(Color.parseColor("#000000"));

                        //initfilterListRecycler(childtitle,counter,selectedtopcategory);
                       /* if (position == 0) {


                            DialogFragment prFrag = new FilterFragmentNew();
                            Bundle bundle = new Bundle();
                            Log.e("maincatList-->>", String.valueOf(mainCatList.size()));

                            Log.e("countlist-->>>", String.valueOf(countlist.size()));

                            bundle.putInt("catId", value);
                            bundle.putString("previoussize",selectesize);
                            bundle.putString("previousitemtype",selectitemtype);
                            bundle.putString("previousmanufecturer",selectmanufec);
                            bundle.putSerializable("prevselectedvalue",(Serializable) new HashMap<>(selectedvalue));




                            bundle.putSerializable("selctecatagory", ((Serializable) new ArrayList<>(mainCatListclone)));



                            bundle.putSerializable("countlist", (Serializable) new HashMap<String,Integer>(countlist));



                            prFrag.setArguments(bundle);

                            prFrag.setTargetFragment(ProductList.this, 101);


                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                            if (prev != null) {
                                ft.remove(prev);
                            }


                            prFrag.setTargetFragment(ProductList.this, 101);
                            prFrag.show(ft, "dialog");


                            //prFrag.show(getFragmentManager(), "dialog");


                        } else if (position == 1) {
                            dialog.show();


//                            initSortDialogue();

                        }*/

                        // else {

                        tick_image.setVisibility(View.GONE);
                        tick_image2.setVisibility(View.GONE);
                        tick_image3.setVisibility(View.GONE);
                        tick_image4.setVisibility(View.GONE);

                        countlist.clear();
                        mainCatList.clear();
                        subcatagorypage = 1;
                        isGetAllProductlist = 3;

                        int finalposition = position /*- 2*/;
                        catagoryList = new ArrayList<>();
                        if (fromwhere.equals("fromhome")) {
                            subcatagoryid = childlist.get(finalposition).id;


                        } else if (fromwhere.equals("fromcatagory")) {
                            subcatagoryid = childcatagorylist.get(finalposition).id;


                        }

                        value = Integer.parseInt(subcatagoryid);


                        //tooltext.setText(childtitle.get(position));

                        sortingflag = 0;
                        subcatagorypage = 1;
                        getProductListforsubCatagory(subcatagoryid, subcatagorypage);


                        //  }


                        break;
                }
            }
        }


        );
        filterlstrecycler.setAdapter(madapter);
        filterlstrecycler.setNestedScrollingEnabled(false);


        RecyclerView.LayoutManager layoutManager = filterlstrecycler.getLayoutManager();



     /*filterlstrecycler.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {

            }
            public void onSwipeRight() {
               // childtext.setVisibility(View.GONE);
            }
            public void onSwipeLeft() {
                childtext.setVisibility(View.VISIBLE);
            }
            public void onSwipeBottom() {

            }

        });*/

        if (adapterposition != null)
            filterlstrecycler.getLayoutManager().scrollToPosition(Integer.parseInt(adapterposition));


        filterlstrecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);


                RecyclerView.LayoutManager layoutManager = filterlstrecycler.getLayoutManager();

                int firstCompleteVisibleItemPosition = -1;
                int lastCompleteVisibleItemPosition = -1;
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();

                if (layoutManager instanceof GridLayoutManager) {
                    GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
                    firstCompleteVisibleItemPosition = gridLayoutManager.findFirstCompletelyVisibleItemPosition();
                    lastCompleteVisibleItemPosition = gridLayoutManager.findLastCompletelyVisibleItemPosition();
                } else if (layoutManager instanceof LinearLayoutManager) {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
                    firstCompleteVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                    lastCompleteVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                }

                if (dy > 0) {
                    // Scrolling up
                } else {
                    // Scrolling down
                }

                if (firstCompleteVisibleItemPosition == 2) {
                    // dy < 0 means scroll to bottom, dx < 0 means scroll to right at beginning.
                    if (dy < 0 || dx < 0) {
                        // Means scroll to bottom.
                        if (dy < 0) {

                        }

                        // Means scroll to right.
                        if (dx < 0) {
                            //childtext.setVisibility(View.GONE);
                            System.out.println("Scrolled to Right");
                        }
                    }
                }
                // Means scroll at ending ( bottom to top or right to left )
                else if (lastCompleteVisibleItemPosition == (totalItemCount - 1)) {
                    // dy > 0 means scroll to up, dx > 0 means scroll to left at ending.
                    if (dy > 0 || dx > 0) {
                        // Scroll to top
                        if (dy > 0) {

                        }

                        // Scroll to left
                        if (dx > 0) {
                            //childtext.setVisibility(View.VISIBLE);
                            System.out.println("Scrolled to Left");
                        }
                    }
                }


            }


        });


    }


    public void getProductListforsubCatagory(String productId, final int current_page) {
        //catagoryList = new ArrayList<>();
        parentDialogListener.showProgressDialog();


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map = new HashMap<>();
        map.put("cat_id", productId);
        map.put("page", String.valueOf(current_page));
        map.put("sort_by", String.valueOf(sortingflag));


        Call<CatagoryList> call = apiService.getCataGoryList(map);
        call.enqueue(new Callback<CatagoryList>() {
            @Override
            public void onResponse(Call<CatagoryList> call, Response<CatagoryList> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    mSwipeRefreshLayout.setRefreshing(false);
                    productListmain.clear();
                    productListmain.addAll(response.body().data);
                    if (response.body().data.size() > 0) {
                        if (subcatagorypage == 1) {
                            catagoryList = new ArrayList<>();
                            scrollListener.resetState();
                        }


                        if (subcatagorypage == 1) {


                            catagoryList.clear();

                        }


                        for (int i = 0; i < response.body().data.size(); i++) {
                            catagoryList.add(response.body().data.get(i));
                        }


                        if (catagoryList.size() > 0) {
                            if (subcatagorypage == 1) {


                                initRecycler(v, catagoryList);
                            } else {

                                mAdapter.notifyDataSetChanged();


                            }
                        } else {
                            // initRecycler(v, catagoryList);
                            // Toast.makeText(getContext(), "لا يوجد سجلات", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        mSwipeRefreshLayout.setRefreshing(false);


                    }
                } else {
                    mSwipeRefreshLayout.setRefreshing(false);
                    parentDialogListener.hideProgressDialog();

                }


            }


            @Override
            public void onFailure(Call<CatagoryList> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                mSwipeRefreshLayout.setRefreshing(false);
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // Toast.makeText(getContext(),"resume",Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onStart() {
        super.onStart();
        // Toast.makeText(getContext(),"on Start",Toast.LENGTH_SHORT).show();
    }


    public void firebase_view_item_list() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, item_catagory_name);


        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST, bundle);


    }
}
