package com.eoutlet.Eoutlet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.bumptech.glide.Glide;
import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.adpters.RelatedProductAdapter;
import com.eoutlet.Eoutlet.adpters.SizeAdapter;
import com.eoutlet.Eoutlet.adpters.SliderAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.api.request.CatagoryList;
import com.eoutlet.Eoutlet.api.request.ListItems;
import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.intrface.UpdateBedgeCount;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.AddtoCartResponse;
import com.eoutlet.Eoutlet.pojo.ColorandSizeDetail;
import com.eoutlet.Eoutlet.pojo.ProductGallerryDetail;
import com.eoutlet.Eoutlet.pojo.ViewCart1;
import com.eoutlet.Eoutlet.utility.Constants;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class ProductDetail extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private Context mContext;
    private ExecuteFragment execute;
    private ViewPager viewPager;
    private ListItems catagoryList;
    private UpdateBedgeCount updatefix;


    private String newprice;
    private ImageView ivcolor;
    View toolbarbeg;
    private ArrayList<String> sizedata = new ArrayList<>();
    private ArrayList<String> indexdata = new ArrayList<>();
    private ArrayList<String> colordata = new ArrayList<>();
    private ArrayList<String> colorurl = new ArrayList<>();
    private ArrayList<Integer> salable = new ArrayList<>();
    private ArrayList<String> priceaccordingtosize = new ArrayList<>();
    private ArrayList<String> oldpriceaccordingtosize = new ArrayList<>();

    private ArrayList<String> indexdata2 = new ArrayList<>();
    private String choosensizeIndex = "";
    static String choosencolorIndex = " ";
    private RelatedProductAdapter mAdapter7;
    private TextView toolbar_bedgetextdetail, tooltext;
    int choosenquantity = 1;
    int totalcount;
    private SizeAdapter mAdapter;
    private LinearLayout colourandsize, relatedproducttext;
    private RecyclerView sizerecycler, relatedproduct;
    private RelativeLayout sizeofItem;
    private GridLayoutManager lm;
    private LinearLayoutManager llm;

    private String item_catagory_name;
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView tvaddtocart;
    TextView oldPrice, newPrice, description, productname, tvplus,  tvfinalqty, selectedsizename, selectcoloname,sizeselectiontext,
            sizeofsimplaeitem, tv_sku;

    private FrameLayout tvminace;
    public static String value, pid, size, color, colorname;

    private ImageView searchImage,backarrow;
    private Toolbar toolbar1;
    private String type;
    public ParentDialogsListener parentDialogListener;
    private List<String> myProductList = new ArrayList<>();
    private TextView viewmore;
    int[] myImageList = new int[]{R.drawable.pro1, R.drawable.pro2, R.drawable.pro3, R.drawable.pro4, R.drawable.pro5, R.drawable.pro6,
            R.drawable.pro7, R.drawable.pro8, R.drawable.pro9, R.drawable.pro10};
    // TODO: Rename and change types of parameters
    View v;

    private OnFragmentInteractionListener mListener;

    public ProductDetail() {
        // Required empty public constructor
    }

    public void passData(Context context) {
        mContext = context;
        execute = (MainActivity) getActivity();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            parentDialogListener = (ParentDialogsListener) activity;

        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }

    public static ProductDetail newInstance(String param1, String param2) {
        ProductDetail fragment = new ProductDetail();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_product_detail, container, false);
            execute = (MainActivity) getActivity();
            value = getArguments().getString("sku");
            type = getArguments().getString("type");
            pid = getArguments().getString("pid");
            size = getArguments().getString("size");
            color = getArguments().getString("color");
            colorname = getArguments().getString("color_name");
            updatefix = (MainActivity) getActivity();


            catagoryList = (ListItems) getArguments().getSerializable("catagoryobject");

            initViews();

            getProductDetail(value);



            toolbar1 = v.findViewById(R.id.toolbar);
            searchImage = toolbar1.findViewById(R.id.serachbar);
            backarrow = toolbar1.findViewById((R.id.backarrow));

            searchImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Fragment prFrag = new SearchResultFragment();
                    Bundle databund = new Bundle();


                    getFragmentManager()
                            .beginTransaction().addToBackStack(null)
                            .replace(R.id.profileContainer, prFrag)
                            .commit();



                }
            });

            backarrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().popBackStack();
                }
            });

            //firebase_view_item(value);



        }


        return v;
    }

    public void initSizeRecycler() {
        // choosensizeIndex ="";
        if (salable.get(0) == 1) {
            selectedsizename.setText(sizedata.get(0));
        }


        sizerecycler.setHasFixedSize(true);
        sizerecycler.setNestedScrollingEnabled(false);


        // use a linear layout manager



        //  FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());

        // layoutManager.setFlexDirection(FlexDirection.ROW);

        //  layoutManager.setFlexDirection(FlexDirection.ROW_REVERSE);
        //  sizerecycler.setLayoutManager(layoutManager);



        llm = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true);




        sizerecycler.setLayoutManager(llm);



        // specify an adapter (see also next example)
        mAdapter = new SizeAdapter(v.getContext(), sizedata, indexdata, salable, new ViewListener() {
            @Override
            public void onClick(int position, View view) {
                int id = view.getId();


                switch (id) {

                    case R.id.sizeofItems://button for message


                        choosensizeIndex = indexdata.get(position);
                        choosencolorIndex = indexdata2.get(0);
                        selectedsizename.setText(sizedata.get(position));

                        newPrice.setText("SAR"+" "+priceaccordingtosize.get(position));


                        if (oldpriceaccordingtosize.get(position).equals("0") || oldpriceaccordingtosize==priceaccordingtosize) {

                            oldPrice.setVisibility(View.GONE);


                        } else {
                            oldPrice.setVisibility(View.VISIBLE);
                            String mainprice = " ";
                            if (oldpriceaccordingtosize.contains(",")) {
                                mainprice = oldpriceaccordingtosize.get(position).replaceAll(",", "");

                                oldPrice.setText("SAR" + " " + (int) Float.parseFloat(mainprice));


                            } else {

                                oldPrice.setText("SAR" + " " + (int) Float.parseFloat(oldpriceaccordingtosize.get(position)));


                            }

                        }


                        break;
                }
            }
        });
        sizerecycler.setAdapter(mAdapter);

        getRelatedproduct();


    }

    //Initializing Views
    public void initViews() {
        oldPrice = v.findViewById(R.id.productoldPrice);
        newPrice = v.findViewById(R.id.productnewprice);
        productname = v.findViewById(R.id.productName);
        description = v.findViewById(R.id.productDescription);
        tvaddtocart = v.findViewById(R.id.addtoCart);
        sizerecycler = v.findViewById(R.id.size_list_Recycler);
        colourandsize = v.findViewById(R.id.colorandsize);
        tvplus = v.findViewById(R.id.plus_button);
        tvminace = v.findViewById(R.id.minace_button);
        tvfinalqty = v.findViewById(R.id.selectedQuantitity);
        relatedproduct = v.findViewById(R.id.related_product);
        selectedsizename = v.findViewById(R.id.sizename);
        selectcoloname = v.findViewById(R.id.colorname);
        sizeselectiontext = v.findViewById(R.id.selectsizetext);
        ivcolor = v.findViewById(R.id.colorImage);
        relatedproducttext = v.findViewById(R.id.relatedproducttext);
        tv_sku = v.findViewById(R.id.skunumber);

        sizeofItem = v.findViewById(R.id.relativesimplesizeview);
        sizeofsimplaeitem = v.findViewById(R.id.sizeofsimpleItems);

        viewmore = v.findViewById(R.id.viewmore);



        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        tvplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                choosenquantity++;
                tvfinalqty.setText(String.valueOf(choosenquantity));


            }
        });
        tvminace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (choosenquantity > 1) {

                    choosenquantity--;
                    tvfinalqty.setText(String.valueOf(choosenquantity));
                }


            }
        });
        try {
            tv_sku.setText(catagoryList.sku);

            // oldPrice.setText("SAR" + " " + (int) Float.parseFloat(catagoryList.oldPrice));

            if (catagoryList.price.contains(",")) {

                newprice = catagoryList.price.replaceAll(",", "");
                newPrice.setText("SAR" + " " + (int) Float.parseFloat(newprice));

            } else {


                newPrice.setText("SAR" + " " + (int) Float.parseFloat(catagoryList.price));


            }


            if (catagoryList.oldPrice.equals("0")) {

                oldPrice.setVisibility(View.GONE);


            } else {
                oldPrice.setVisibility(View.VISIBLE);
                String mainprice = " ";
                if (catagoryList.oldPrice.contains(",")) {
                    mainprice = catagoryList.oldPrice.replaceAll(",", "");

                    oldPrice.setText("SAR" + " " + (int) Float.parseFloat(mainprice));


                } else {

                    oldPrice.setText("SAR" + " " + (int) Float.parseFloat(catagoryList.oldPrice));


                }

            }
            productname.setText(catagoryList.name);

            description.setText(catagoryList.short_descrition);

            description.post(new Runnable() {
                @Override
                public void run() {
                    int lineCount = description.getLineCount();
                    if (lineCount> 2){

                        viewmore.setVisibility(View.VISIBLE);
                        viewmore.setText("قراءة المزيد…");
                        description.setMaxLines(2);

                    }
                    else {
                        description.setText(catagoryList.short_descrition);
                    }
                    // Use lineCount here
                }
            });
            viewmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (description.getLineCount()==2) {
                        description.setText(catagoryList.short_descrition.trim());
                        viewmore.setVisibility(View.GONE);
                    } else {
                        description.setText(catagoryList.short_descrition.trim());
                        description.setMaxLines(5);
                        viewmore.setText("قراءة المزيد…");
                        viewmore.setVisibility(View.GONE);
                    }
                }
            });


            /*if (catagoryList.short_descrition.length() > 80) {

                viewmore.setVisibility(View.VISIBLE);
                final String displayText = catagoryList.short_descrition.substring(0, 70).trim() + "...";
                description.setText(displayText.trim());
                viewmore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (description.getText().length() < 90) {
                            description.setText(catagoryList.short_descrition.trim());
                            viewmore.setVisibility(View.GONE);
                        } else {
                            description.setText(displayText);
                            viewmore.setText("قراءة المزيد…");
                            viewmore.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }


            else
                description.setText(catagoryList.short_descrition.trim());*/



        }



        catch (Exception e) {

            Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_SHORT).show();


        }





        tvaddtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!type.equals("simple")) {
                    if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {
                        if (choosensizeIndex != null && !choosensizeIndex.equals("")) {
                            addtocart();
                        } else {
                            Toast.makeText(getContext(), "برجاء اختيار المقاس المطلوب أولا", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (choosensizeIndex != null && !choosensizeIndex.equals("")) {
                            addtoCartguestUser();
                        } else {
                            Toast.makeText(getContext(), "برجاء اختيار المقاس المطلوب أولا", Toast.LENGTH_SHORT).show();
                        }
                    }


                } else {

                    if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {

                        addtocart();

                    } else {

                        addtoCartguestUser();

                    }


                }
            }


        });

        if (type.equals("simple")) {
            sizeofsimplaeitem.setText(size);

            //colourandsize.setVisibility(View.GONE);


        } else {
            sizeofItem.setVisibility(View.GONE);
            colourandsize.setVisibility(View.VISIBLE);

        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void intiViewPager(final List<String> producudetail) {
        viewPager = v.findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(myImageList.length);
        viewPager.setAdapter(new SliderAdapter(v.getContext(), producudetail));

        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.indicator);
        tabLayout.setupWithViewPager(viewPager, true);


    }

    //Getting multiple Images of Particular Product here.
    public void getProductDetail(String sku) {

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        //Call<ProductGallerryDetail> call = apiService.getCataGoryDetail(Constants.PRODUCT_BASE_LINK + value);

        Call<ProductGallerryDetail> call = apiService.getCataGoryDetail(Constants.mediaSourceURL + "rest/V1/products/" + value);


        //The version always remain V1 in this


        call.enqueue(new Callback<ProductGallerryDetail>() {
            @Override
            public void onResponse(Call<ProductGallerryDetail> call, Response<ProductGallerryDetail> response) {


                if (response.body() != null) {

                    if (!type.equals("simple")) {
                        getcolorandSize(value);


                    } else {
                        selectcoloname.setText(colorname);

                        if (color != null) {
                            Glide.with(ivcolor)
                                    .load(color).override(70, 70)
                                    .into(ivcolor);
                        }

                        // Picasso.get().load(color)/*.fit()*/.resize(70, 70).centerCrop().into(ivcolor);
                        selectedsizename.setText(size);

                        getRelatedproduct();
                    }

                    if (response.body().status == 1) {


                        for (int i = 0; i < response.body().mediaGalleryEntries.size(); i++) {


                            myProductList.add(Constants.mediaSourceURL + "/pub/media/catalog/product/" + response.body().mediaGalleryEntries.get(i).file);


                        }
                        intiViewPager(myProductList);


                    } else {
                        parentDialogListener.hideProgressDialog();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }
                firebase_view_item(value);

            }


            @Override
            public void onFailure(Call<ProductGallerryDetail> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getcolorandSize(String value) {

        //parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();

        query.put("sku", value);

        Call<ColorandSizeDetail> call = apiService.getColorandSize(query);
        call.enqueue(new Callback<ColorandSizeDetail>() {
            @Override
            public void onResponse(Call<ColorandSizeDetail> call, Response<ColorandSizeDetail> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    try {
                        if (response.body().data.size() > 0) {
                            sizeselectiontext.setVisibility(View.VISIBLE);
                            if (response.body().data.get(1) != null) {
                                for (int i = 0; i < response.body().data.get(1).data.size(); i++) {
                                    sizedata.add(response.body().data.get(1).data.get(i).name);
                                    indexdata.add(response.body().data.get(1).data.get(i).value_Index);
                                    salable.add(response.body().data.get(1).data.get(i).salable);
                                    priceaccordingtosize.add(String.valueOf(response.body().data.get(1).data.get(i).price));
                                    oldpriceaccordingtosize.add(String.valueOf(response.body().data.get(1).data.get(i).old_price));


                                }
                            }
                            for (int i = 0; i < response.body().data.get(0).data.size(); i++) {
                                colordata.add(response.body().data.get(0).data.get(i).name);

                                indexdata2.add(response.body().data.get(0).data.get(i).value_Index);
                                colorurl.add(response.body().data.get(0).data.get(i).image);
                            }
                            if (salable.get(0) == 1) {

                                choosensizeIndex = response.body().data.get(1).data.get(0).value_Index;
                            }
                            choosencolorIndex = indexdata2.get(0);
                            selectcoloname.setText(colordata.get(0));
                            Picasso.get().load(colorurl.get(0))/*.fit()*/.resize(70, 70).centerCrop().into(ivcolor);

                            initSizeRecycler();


                        }
                    } catch (Exception e) {

                        parentDialogListener.hideProgressDialog();
                        Log.e("Exception--->", "There are some Exception");


                    }


                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<ColorandSizeDetail> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void addtoCartguestUser() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        if (type.equals("simple")) {


            query.put("customer_id", " ");
            query.put("sku", String.valueOf(value));
            query.put("type", type);

            query.put("qty", String.valueOf(choosenquantity));
            query.put("size", " ");
            query.put("color", " ");
            query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));
            query.put("mask_key", MySharedPreferenceClass.getMaskkey(getContext()));
            query.put("product_id", catagoryList.id);

        } else {
            query.put("customer_id", " ");
            query.put("sku", String.valueOf(value));
            query.put("size", choosensizeIndex);
            if (choosencolorIndex != null) {
                query.put("color", choosencolorIndex);
            } else {
                query.put("color", " ");

            }
            query.put("type", type);
            query.put("qty", String.valueOf(choosenquantity));
            query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));
            query.put("mask_key", MySharedPreferenceClass.getMaskkey(getContext()));
            query.put("product_id", catagoryList.id);
        }


        Call<AddtoCartResponse> call = apiService.addtocart(query);
        call.enqueue(new Callback<AddtoCartResponse>() {
            @Override
            public void onResponse(Call<AddtoCartResponse> call, Response<AddtoCartResponse> response) {


                if (response.body() != null) {
                    //parentDialogListener.hideProgressDialog();

                    if (response.body().msg.equals("success")) {


                        appsflyer_event_add_to_cart(newprice, String.valueOf(value), String.valueOf(choosenquantity));
                        firebase_event_add_to_cart(newprice, String.valueOf(value), String.valueOf(choosenquantity));


                        getCartDataforguestuser();

                    } else {
                        Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_SHORT).show();
                        //parentDialogListener.hideProgressDialog();
                        getCartDataforguestuser();


                    }
                }


            }


            @Override
            public void onFailure(Call<AddtoCartResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void addtocart() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        if (type.equals("simple")) {

            query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
            query.put("sku", String.valueOf(value));
            query.put("type", type);
            query.put("qty", String.valueOf(choosenquantity));
            query.put("size", " ");
            query.put("color", " ");
            query.put("product_id", catagoryList.id);
            query.put("cart_id", "");
            query.put("mask_key", "");


        } else {
            query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
            query.put("sku", String.valueOf(value));
            query.put("size", choosensizeIndex);
            query.put("color", choosencolorIndex);
            query.put("type", type);
            query.put("qty", String.valueOf(choosenquantity));
            query.put("product_id", catagoryList.id);
            query.put("cart_id", "");
            query.put("mask_key", "");

        }


        Call<AddtoCartResponse> call = apiService.addtocart(query);
        call.enqueue(new Callback<AddtoCartResponse>() {
            @Override
            public void onResponse(Call<AddtoCartResponse> call, Response<AddtoCartResponse> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();

                    if (response.body().msg.equals("success")) {


                        appsflyer_event_add_to_cart(newprice, String.valueOf(value), String.valueOf(choosenquantity));
                        firebase_event_add_to_cart(newprice, String.valueOf(value), String.valueOf(choosenquantity));

                        getCartData();


                    } else {
                        Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_SHORT).show();
                        parentDialogListener.hideProgressDialog();


                    }
                }


            }


            @Override
            public void onFailure(Call<AddtoCartResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void getCartData() {
        totalcount = 0;

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {
                parentDialogListener.hideProgressDialog();

                if (response.body().msg.equals("success")) {
                    dovibrate();

                    if (response.body().data.size() > 0) {
                        for (int i = 0; i < response.body().data.size(); i++) {

                            if (response.body().data.get(i).qty instanceof String) {

                                totalcount = totalcount + (int) Float.parseFloat((String) response.body().data.get(i).qty);

                            } else {


                                totalcount = totalcount + (int) (response.body().data.get(i).qty);


                            }


                            //   totalcount = totalcount+(int)response.body().data.get(i).qty;


                        }
                        parentDialogListener.hideProgressDialog();
                        MySharedPreferenceClass.setBedgeCount(getContext(), totalcount);

                        updatefix.updateBedgeCount();
                        MainActivity.notificationBadge.setVisibility(View.VISIBLE);


//                        toolbar_bedgetextdetail.setText(String.valueOf(totalcount));
                        // toolbar_bedgetextdetail.setVisibility(View.VISIBLE);


                       /* if (ProductList.toolbar_bedgetextlist != null) {
                            ProductList.toolbar_bedgetextlist.setText(String.valueOf(totalcount));
                            ProductList.toolbar_bedgetextlist.setVisibility(View.VISIBLE);
                        }

                        if (AZfragment.toolbar_bedgetextbrand != null) {
                            AZfragment.toolbar_bedgetextbrand.setVisibility(View.VISIBLE);
                            AZfragment.toolbar_bedgetextbrand.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));
                        }
                        if (NewCategoryFragment.toolbar_bedgetextcat != null) {
                            NewCategoryFragment.toolbar_bedgetextcat.setVisibility(View.VISIBLE);
                            NewCategoryFragment.toolbar_bedgetextcat.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));
                        }

                        if (CartFragment.toolbar_bedgetextcart != null) {
                            CartFragment.toolbar_bedgetextcart.setVisibility(View.VISIBLE);
                            CartFragment.toolbar_bedgetextcart.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));
                        }*/

                        //NewHomeFragment.toolbar_bedgetexthome.setText(String.valueOf(totalcount));
                        //NewHomeFragment.toolbar_bedgetexthome.setVisibility(View.VISIBLE);
                        Log.e("Total Value in Cart--->", String.valueOf(totalcount));


                    }


                } else {

                    Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();

                }


            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getCartDataforguestuser() {
        totalcount = 0;

        //parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", " ");
        query.put("mask_key", MySharedPreferenceClass.getMaskkey(getContext()));
        query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));


        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {

                parentDialogListener.hideProgressDialog();
                if (response.body().msg.equals("success")) {

                    dovibrate();
                    if (response.body().data.size() > 0) {
                        for (int i = 0; i < response.body().data.size(); i++) {

                            if (response.body().data.get(i).qty instanceof String) {

                                totalcount = totalcount + (int) Float.parseFloat((String) response.body().data.get(i).qty);

                            } else {


                                totalcount = totalcount + (int) (response.body().data.get(i).qty);


                            }


                            //   totalcount = totalcount+(int)response.body().data.get(i).qty;


                        }

                        MySharedPreferenceClass.setBedgeCount(getContext(), totalcount);

                        updatefix.updateBedgeCount();
                        MainActivity.notificationBadge.setVisibility(View.VISIBLE);
                        parentDialogListener.hideProgressDialog();
                        Log.e("Total Value in Cart--->", String.valueOf(totalcount));

//                        toolbar_bedgetextdetail.setText(String.valueOf(totalcount));
                        //  toolbar_bedgetextdetail.setVisibility(View.VISIBLE);

                        /*if (ProductList.toolbar_bedgetextlist != null) {
                            ProductList.toolbar_bedgetextlist.setText(String.valueOf(totalcount));
                            ProductList.toolbar_bedgetextlist.setVisibility(View.VISIBLE);
                        }
                        if (AZfragment.toolbar_bedgetextbrand != null) {
                            AZfragment.toolbar_bedgetextbrand.setVisibility(View.VISIBLE);
                            AZfragment.toolbar_bedgetextbrand.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));
                        }
                        if (NewCategoryFragment.toolbar_bedgetextcat != null) {
                            NewCategoryFragment.toolbar_bedgetextcat.setVisibility(View.VISIBLE);
                            NewCategoryFragment.toolbar_bedgetextcat.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));
                        }

                        if (CartFragment.toolbar_bedgetextcart != null) {
                            CartFragment.toolbar_bedgetextcart.setVisibility(View.VISIBLE);
                            CartFragment.toolbar_bedgetextcart.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));
                        }

                        NewHomeFragment.toolbar_bedgetexthome.setText(String.valueOf(totalcount));
                        NewHomeFragment.toolbar_bedgetexthome.setVisibility(View.VISIBLE);*/

                    }


                } else {
                    // if(totalcount>0){
                    // MySharedPreferenceClass.setBedgeCount(getContext(), totalcount);
                    parentDialogListener.hideProgressDialog();
                    // updatefix.updateBedgeCount();
                    //  MainActivity.notificationBadge.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();

                    //}
                }


            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void openSignindialog() {
        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage("يجب تسجيل الدخول أولا");


        alertDialogBuilder.setPositiveButton("نعم",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        Fragment prFrag = new LoginNewFragment();
                        Bundle databund = new Bundle();
                        databund.putString("flag", "fromdetail");

                        prFrag.setArguments(databund);


                        getFragmentManager()
                                .beginTransaction().addToBackStack(null)
                                .replace(R.id.coordinator, prFrag)
                                .commit();
                    }
                });

        alertDialogBuilder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {
                //alertDialog.dismiss();

            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void getRelatedproduct() {

        // parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("pid", pid);

        Call<CatagoryList> call = apiService.getRelatedProduct(query);
        call.enqueue(new Callback<CatagoryList>() {
            @Override
            public void onResponse(Call<CatagoryList> call, Response<CatagoryList> response) {

                if (response.body() != null && response.body().msg != null) {
                    if (response.body().msg.equals("success")) {
                        parentDialogListener.hideProgressDialog();


                        if (response.body().data.size() > 0) {
                            initrelatedProductrecycler(response.body().data);

                        }


                    } else {
                        relatedproducttext.setVisibility(View.GONE);
                        parentDialogListener.hideProgressDialog();


                    }


                }
            }

            @Override
            public void onFailure(Call<CatagoryList> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                relatedproducttext.setVisibility(View.GONE);

                //Toast.makeText(getContext(), "No Items Found", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void initrelatedProductrecycler(final List<ListItems> cat7) {

        relatedproduct.setHasFixedSize(true);
        relatedproduct.setNestedScrollingEnabled(false);

        relatedproduct.setLayoutManager(new LinearLayoutManager(v.getContext(), LinearLayoutManager.HORIZONTAL, false));

        mAdapter7 = new RelatedProductAdapter(mContext, cat7,


                new ViewListener() {
                    @Override
                    public void onClick(int position, View view) {
                        int id = view.getId();


                        switch (id) {

                            case R.id.man_image://button for message
                                Fragment prFrag = new ProductDetail();
                                Bundle databund = new Bundle();
                                databund.putString("sku", cat7.get(position).sku);
                                databund.putString("type", cat7.get(position).type);
                                databund.putString("pid", cat7.get(position).id);

                                databund.putString("size", cat7.get(position).size);
                                databund.putString("color", cat7.get(position).color);
                                databund.putString("color_name", cat7.get(position).color_name);
                                databund.putSerializable("catagoryobject", cat7.get(position));
                                prFrag.setArguments(databund);


                                getFragmentManager()
                                        .beginTransaction().addToBackStack(null)
                                        .replace(ProductDetail.this.getId(), prFrag)
                                        .commit();

                                break;
                        }
                    }
                }


        );
        relatedproduct.setAdapter(mAdapter7);


    }

    public void appsflyer_event_add_to_cart(String price, String sku, String quantity) {
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.PRICE, newPrice.getText().toString());
        eventValue.put(AFInAppEventParameterName.CONTENT_ID, sku);
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, " ");
        eventValue.put(AFInAppEventParameterName.CURRENCY, "SAR");
        eventValue.put(AFInAppEventParameterName.QUANTITY, quantity);
        AppsFlyerLib.getInstance().trackEvent(getActivity(), AFInAppEventType.ADD_TO_CART, eventValue);
    }


    public void firebase_event_add_to_cart(String price, String sku, String quantity) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sku);
        bundle.putString(FirebaseAnalytics.Param.QUANTITY, quantity);
        bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, catagoryList.category_name);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, productname.getText().toString());
        bundle.putInt(FirebaseAnalytics.Param.VALUE, Integer.parseInt(newPrice.getText().toString().split(" ")[1]));
        bundle.putString(FirebaseAnalytics.Param.PRICE, newPrice.getText().toString().split(" ")[1]);
        bundle.putString(FirebaseAnalytics.Param.ITEM_LOCATION_ID, Locale.getDefault().getCountry());
        bundle.putString(FirebaseAnalytics.Param.CURRENCY, "SAR");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, bundle);

    }

    public void firebase_view_item(String sku) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sku);
        bundle.putString(FirebaseAnalytics.Param.ITEM_LOCATION_ID, Locale.getDefault().getCountry());
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle);


    }


    public void dovibrate() {

        Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        assert v != null;
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(80,
                    VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(100);
        }
    }
}





