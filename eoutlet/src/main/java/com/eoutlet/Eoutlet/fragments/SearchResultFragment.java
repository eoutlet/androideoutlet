package com.eoutlet.Eoutlet.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.adpters.ProductListAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.api.request.CatagoryList;
import com.eoutlet.Eoutlet.api.request.ListItems;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.listener.PaginationListener;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.google.common.reflect.TypeToken;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class SearchResultFragment extends Fragment {
    private RecyclerView productListRecycler;
    private ProductListAdapter mAdapter;

    List<ListItems> catagoryList = new ArrayList<>();
    private SearchView serachview;
    ArrayList<String>  temp = new ArrayList<>();
    AutoCompleteTextView autoCountry;
    GridLayoutManager lm;
    private FirebaseAnalytics mFirebaseAnalytics;
    private PaginationListener scrollListener;
    private OnFragmentInteractionListener mListener;
    private Toolbar toolbar;
    ListView listView;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;
    View view;
    String selectquery = " ";
    int currentpage = 1;
    public ParentDialogsListener parentDialogListener;

    public SearchResultFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static SearchResultFragment newInstance(String param1, String param2) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search_result, container, false);


        productListRecycler = view.findViewById(R.id.product_list_search_Recycler);
        serachview = view.findViewById(R.id.searchViewMain);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        toolbar = getActivity().findViewById(R.id.common_toolbar);
        toolbar.setVisibility(View.GONE);
        autoCountry = (AutoCompleteTextView) view.findViewById(R.id.autosearch);
        //toolbar.setVisibility(View.GONE);

        list = new ArrayList<>();
        //list.add(" ");





        if(getArrayList("SearchHistory") == null) {
            saveArrayList(list, "SearchHistory");
        }

        list = getArrayList("SearchHistory");



        lm = new GridLayoutManager(view.getContext(), 2) {
            @Override
            protected boolean isLayoutRTL() {
                return true;
            }
        };

        scrollListener = new PaginationListener(
                lm) {
            @Override
            public void onLoadMore(int current_page) {


                //model.getProductListData(value, getContext(),currentpage);


                currentpage++;


                getSearchResult(String.valueOf(selectquery), currentpage);


            }

        };

        productListRecycler.addOnScrollListener(scrollListener);



        listView = (ListView) view.findViewById(R.id.lv1);

        temp.clear();



        for(int i=list.size()-1;i>=0;i--) {

            temp.add(list.get(i));

        }

       //adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, list);
        adapter = new ArrayAdapter<String>(getContext(), R.layout.search_textview,R.id.searchtext, temp);
        //adapter = new SearchResultAdapter(getContext(),list);
        listView.setAdapter(adapter);

       serachview.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
           @Override
           public void onFocusChange(View v, boolean hasFocus) {
               if(hasFocus)
                   listView.setVisibility(View.VISIBLE);
           }
       });






        serachview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {





                //scrollListener.resetState();








                if(!getArrayList("SearchHistory").contains(query) &&  getArrayList("SearchHistory").size()< 10) {
                  //adapter.clear();


                  list.add(query);

                  temp.clear();



                  for(int i=list.size()-1;i>=0;i--) {

                      temp.add(list.get(i));

                  }




                    adapter = new ArrayAdapter<String>(getContext(), R.layout.search_textview,R.id.searchtext, temp);



                    listView.setAdapter(adapter);


                    listView.setVisibility(View.GONE );


                       saveArrayList(list, "SearchHistory");

                }

              else if(!getArrayList("SearchHistory").contains(query)) {


                   list.remove(0);
                  list.add(query);

                  adapter = new ArrayAdapter<String>(getContext(), R.layout.search_textview,R.id.searchtext, list);



                  listView.setAdapter(adapter);


                  listView.setVisibility(View.GONE );


                  saveArrayList(list, "SearchHistory");



              }





                selectquery = query;
                currentpage = 1;

                if (query.length() > 0) {
                    catagoryList = new ArrayList<>();
                    getSearchResult(query, currentpage);


                } else {
                    Toast.makeText(getContext(), "لا يوجد تطابق", Toast.LENGTH_LONG).show();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (newText.length() > 0) {

                    adapter.getFilter().filter(newText);
                    listView.setVisibility(View.VISIBLE);
                } else {



                    temp.clear();



                    for(int i=list.size()-1;i>=0;i--) {

                        temp.add(list.get(i));

                    }
                    adapter = new ArrayAdapter<String>(getContext(), R.layout.search_textview,R.id.searchtext, temp);



                    listView.setAdapter(adapter);

                }
                return false;
            }
        }



        );



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {



                //Toast.makeText(getContext(),list.get(position)+position+adapter.getItem(position),Toast.LENGTH_SHORT).show();

                serachview.setQuery(adapter.getItem(position), true);


                 scrollListener.resetValues();

                //productListRecycler.addOnScrollListener(scrollListener);


            }
        });


        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {

            parentDialogListener = (ParentDialogsListener) context;

        } catch (ClassCastException e) {

            throw new ClassCastException(context.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void getSearchResult(final String querytext, final int currentpage) {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("keyword", querytext);
        query.put("page", String.valueOf(currentpage));

        Call<CatagoryList> call = apiService.getSearchResult(query);
        call.enqueue(new Callback<CatagoryList>() {
            @Override
            public void onResponse(Call<CatagoryList> call, Response<CatagoryList> response) {
                parentDialogListener.hideProgressDialog();

                if (response.body() != null) {



                    if (response.body().msg.equalsIgnoreCase("success")) {
                        if (response.body().data.size() > 0) {

                            for (int i = 0; i < response.body().data.size(); i++) {
                                catagoryList.add(response.body().data.get(i));
                            }
                            if (currentpage == 1) {
                                scrollListener.resetState();

                                //appsflyer_event_search();
                               // firebase_view_search_result(querytext);

                                initRecycler(view, catagoryList);
                            } else {
                                mAdapter.notifyDataSetChanged();

                            }


                        } else {
                            parentDialogListener.hideProgressDialog();

                            //  initRecycler(view,catagoryList);


                            Toast.makeText(getContext(), "لا يوجد سجلات.", Toast.LENGTH_LONG).show();

                        }


                    } else {


                          // scrollListener.resetState();

                        parentDialogListener.hideProgressDialog();
                        if (currentpage == 1) {







                            catagoryList.clear();
                            initRecycler(view, catagoryList);

                            Toast.makeText(getContext(), response.body().message.toString(), Toast.LENGTH_LONG).show();
                        }


                    }


                }


            }


            @Override
            public void onFailure(Call<CatagoryList> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());


            }
        });


    }

    public void initRecycler(View v, final List<ListItems> catagoryList) {
        productListRecycler.setHasFixedSize(true);


        // use a linear layout manager

        productListRecycler.setLayoutManager(/*new GridLayoutManager(v.getContext(), 2)*/lm);

        // specify an adapter (see also next example)
        mAdapter = new ProductListAdapter(getContext(), catagoryList, new ViewListener() {
            @Override
            public void onClick(int position, View view) {
                int id = view.getId();


                switch (id) {

                    case R.id.product_list_image://button for message
                        Fragment prFrag = new ProductDetail();
                        Bundle databund = new Bundle();
                        databund.putString("sku", catagoryList.get(position).sku);
                        databund.putString("type", catagoryList.get(position).type);
                        databund.putString("pid", catagoryList.get(position).id);
                        databund.putString("size", catagoryList.get(position).size);
                        databund.putString("color", catagoryList.get(position).color);
                        databund.putString("color_name", catagoryList.get(position).color_name);
                        databund.putSerializable("catagoryobject", catagoryList.get(position));
                        prFrag.setArguments(databund);


                        getFragmentManager()
                                .beginTransaction().addToBackStack(null)
                                .add(SearchResultFragment.this.getId(), prFrag)
                                .commit();

                        break;
                }
            }
        });
        productListRecycler.setAdapter(mAdapter);


    }


    public void appsflyer_event_search() {
        Map<String, Object> eventValue = new HashMap<String, Object>();

        eventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, MySharedPreferenceClass.getMyUserId(getContext()));
        AppsFlyerLib.getInstance().trackEvent(getActivity(), AFInAppEventType.SEARCH, eventValue);
    }


    public void firebase_view_search_result(String serachvalue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, serachvalue);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS, bundle);


    }
    public void saveArrayList(ArrayList<String> list, String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();

    }
    public ArrayList<String> getArrayList(String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        toolbar.setVisibility(View.VISIBLE);

        }


}
