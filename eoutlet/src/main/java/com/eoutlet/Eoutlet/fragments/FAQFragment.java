package com.eoutlet.Eoutlet.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.eoutlet.Eoutlet.R;


public class FAQFragment extends Fragment {
    private WebView faqWebview;
    private String value =" ";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_faq, container, false);
        value = getArguments().getString("position");
        initUi(view);
        return view;
    }

    public void initUi(View view) {
        faqWebview  = view.findViewById(R.id.faqWebview);


        //faqWebview.setWebViewClient(new MyBrowser());
        //webView.getSettings().setLoadsImagesAutomatically(true);
        // webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        faqWebview.getSettings().setJavaScriptEnabled(true);


        faqWebview.loadData(value, "text/html", "UTF-8");



    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {


        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getActivity(), "Error" + description + errorCode, Toast.LENGTH_SHORT).show();
        }

    }
}
