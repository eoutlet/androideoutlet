package com.eoutlet.Eoutlet.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.ChangePassword;
import com.eoutlet.Eoutlet.pojo.GuestUser;
import com.eoutlet.Eoutlet.utility.Util;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class NewUpdateInfo extends Fragment {
    View view;
    private EditText updatefName, updatelName, updateMail, updateMobile, changenewpassword, oldPassword,updateoldpassword,updatenewpassword;
    private TextView btnupdatProfile, savenewpassword,fullName,address;
    public ParentDialogsListener parentDialogListener;
    private boolean isAllConditionFulfilled;
    private ScrollView mScrollView;
    private ImageView searchImage,backarrow;
    private Toolbar toolbar1;
    private TextView editfirstname,editlastname,editmail,editmobile,editoldpassword,editnewpassword;

    public static NewUpdateInfo newInstance(String param1, String param2) {
        NewUpdateInfo fragment = new NewUpdateInfo();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_new_update_info, container, false);
        intiViews();
        initialzesavedfields();
        return view;
    }

    public void intiViews() {
        updatefName = view.findViewById(R.id.updatefirstName);
        updatelName = view.findViewById(R.id.updatelastName);
        updateMail = view.findViewById(R.id.updateeMail);

        updateoldpassword = view.findViewById(R.id.oldPassword);
        updatenewpassword = view.findViewById(R.id.newPassword);
        updateMobile = view.findViewById(R.id.updatemobile);
        btnupdatProfile = view.findViewById(R.id.updateProfile);
        savenewpassword = view.findViewById(R.id.saveNewPassword);
        changenewpassword = view.findViewById(R.id.newPassword);
        oldPassword = view.findViewById(R.id.oldPassword);
        fullName = view.findViewById(R.id.fullName);
        address = view.findViewById(R.id.address);
        editfirstname = view.findViewById(R.id.editfirstname);
        editlastname = view.findViewById(R.id.editlastname);
        editmail = view.findViewById(R.id.editemail);
        editmobile = view.findViewById(R.id.editmobile);
        mScrollView = view.findViewById(R.id.scrollContainer);
        toolbar1 = view.findViewById(R.id.toolbar);
        searchImage = toolbar1.findViewById(R.id.serachbar);
        backarrow = toolbar1.findViewById((R.id.backarrow));
        editoldpassword = view.findViewById(R.id.editoldpassword);
        editnewpassword = view.findViewById(R.id.editnewpassword);


        editoldpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




               updateoldpassword.setEnabled(true);
                updateoldpassword.requestFocus();






            }
        });

        editnewpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updatenewpassword.setEnabled(true);
                updatenewpassword.requestFocus();






            }
        });


        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment prFrag = new SearchResultFragment();
                Bundle databund = new Bundle();


                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.profileContainer, prFrag)
                        .commit();



            }
        });

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });





        updatefName.setEnabled(false);
        // updatefName.setFocusable(false);

        updatelName.setEnabled(false);
        // updatelName.setFocusable(false);

        updateMail.setEnabled(false);
        //  updateMail.setFocusable(false);


        updateMobile.setEnabled(false);
        //updateMobile.setFocusable(false);

        mScrollView.setSmoothScrollingEnabled(true);
        editfirstname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatefName.setEnabled(true);
                updatefName.requestFocus();
               /* updatelName.setFocusableInTouchMode(true);
                updatefName.setFocusable(true);*/

            }
        });
        editlastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatelName.setEnabled(true);
                updatelName.requestFocus();
                /*updatelName.setFocusableInTouchMode(true);
                updatelName.setFocusable(true);*/

            }
        });
        editmobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateMobile.setEnabled(true);
                updateMobile.requestFocus();
                /*updatelName.setFocusableInTouchMode(true);
                updateMobile.setFocusable(true);*/
            }
        });
        editmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateMail.setEnabled(true);
                updateMail.requestFocus();
                /*updatelName.setFocusableInTouchMode(true);
                updateMail.setFocusable(true);*/
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isAdded()) {
                    // Scroll 1000px down
                    mScrollView.smoothScrollTo(0, 1000);
                }
            }
        }, 150);


        btnupdatProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                validateProfileFields();


            }
        });


        savenewpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                isAllConditionFulfilled = Util.checkTextViewValidation(oldPassword, "برجاء كتابة كلمة السر")
                        && Util.checkTextViewValidation(changenewpassword, "برجاء ادخال كود التأكيد")
                        && Util.isPasswordMatched(oldPassword, changenewpassword, "كلمة المرور الجديدة لا تتطابق مع تأكيد كلمة المرور.");


                if (!isAllConditionFulfilled) {
                    if (Util.view_final != null) {

                        Util.view_final.requestFocus();
                    }


                    return;
                }
                savenewpassword();


            }
        });


    }


    public void savenewpassword() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map1 = new HashMap<>();
        map1.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        map1.put("newpassword", changenewpassword.getText().toString());


        Call<ChangePassword> call = apiService.saveNewPassword(map1);
        call.enqueue(new Callback<ChangePassword>() {
            @Override
            public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().msg.equals("success")) {
                        Toast.makeText(getContext(), "تم تغيير كلمة السر بنجاح", Toast.LENGTH_LONG).show();

                        getguesttoken();




                    } else {
                        parentDialogListener.hideProgressDialog();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<ChangePassword> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                //Toast.makeText(getContext(), "Something went wrong. Please try again", Toast.LENGTH_LONG).show();
            }
        });


    }
    public void getguesttoken() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Call<GuestUser> call = apiService.getGuestToken();
        call.enqueue(new Callback<GuestUser>() {
            @Override
            public void onResponse(Call<GuestUser> call, Response<GuestUser> response) {


                if (response.body().msg.equals("success")) {
                    parentDialogListener.hideProgressDialog();
                    MySharedPreferenceClass.setMaskkey(getContext(), response.body().maskKey);
                    MySharedPreferenceClass.setCartId(getContext(), response.body().cartId);
                    MySharedPreferenceClass.setMyUserId(getContext(), null);
                    MySharedPreferenceClass.setMyUserName(getContext(), null);
                    MySharedPreferenceClass.setMyFirstNamePref(getContext(), null);
                    MySharedPreferenceClass.setMyLastNamePref(getContext(), null);
                    MySharedPreferenceClass.setBedgeCount(getContext(), 0);
                    MySharedPreferenceClass.setEmail(getContext(), null);


                    MySharedPreferenceClass.setAddressname(getContext(), null);
                    MySharedPreferenceClass.setCityname(getContext(), null);
                    MySharedPreferenceClass.setStreetName(getContext(), null);
                    MySharedPreferenceClass.setCountryname(getContext(), null);

                    MySharedPreferenceClass.setAddressphone(getContext(), null);
                    MySharedPreferenceClass.setCountryId(getContext(), null);
                    MySharedPreferenceClass.setfirstAdressname(getContext(),null);
                    MySharedPreferenceClass.setlastAdressname(getContext(),null);
                    MySharedPreferenceClass.setSelecteAddressdId(getContext(), "0");


                    Fragment prFrag = new LoginNewFragment();
                    Bundle databund = new Bundle();
                    databund.putString("flag", "fromlogout");
                    prFrag.setArguments(databund);
                    ((LoginNewFragment) prFrag).passData(getContext());
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.profileContainer, prFrag)
                            .commit();

                } else {
                    parentDialogListener. hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<GuestUser> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                //Toast.makeText(getContext(), "No Items Found", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void initialzesavedfields() {
        updatefName.setText(MySharedPreferenceClass.getMyFirstNamePref(getContext()));
        updatelName.setText(MySharedPreferenceClass.getMyLastNamePref(getContext()));
        updateMail.setText(MySharedPreferenceClass.getEmail(getContext()));
        updateMobile.setText(MySharedPreferenceClass.getMobileNumber(getContext()));
        fullName.setText(MySharedPreferenceClass.getMyFirstNamePref(getContext()) +" "+  (MySharedPreferenceClass.getMyLastNamePref(getContext())) );
        address.setText(MySharedPreferenceClass.getEmail(getContext()));


    }

    public void validateProfileFields() {
        isAllConditionFulfilled = Util.checkTextViewValidation(updatefName, "برجاء كتابة الاسم الاول")
                && Util.checkTextViewValidation(updatelName, "برجاء كتابة الاسم الاخير")
                && Util.checkTextViewValidation(updateMail, "الرجاء إدخال بريد إلكتروني صحيح")


                && Util.checkTextViewValidation(updateMobile, "من فضلك أدخل رقم الجوال");


        if (!isAllConditionFulfilled) {
            if (Util.view_final != null) {

                Util.view_final.requestFocus();
            }


            return;
        }
        saveUpdateProfile();

    }


    public void saveUpdateProfile() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map1 = new HashMap<>();
        map1.put("firstname", updatefName.getText().toString());
        map1.put("lastname ", updatelName.getText().toString());
        map1.put("email", updateMail.getText().toString());
        map1.put("mobilenumber", updateMobile.getText().toString());
        map1.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<ChangePassword> call = apiService.saveUpdateInfo(map1);
        call.enqueue(new Callback<ChangePassword>() {
            @Override
            public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().msg.equals("success")) {
                        Toast.makeText(getContext(), "تم تحديث الملف الشخصي بنجاح", Toast.LENGTH_LONG).show();


                        MySharedPreferenceClass.setMyFirstNamePref(getContext(), updatefName.getText().toString());
                        MySharedPreferenceClass.setMyLastNamePref(getContext(), updatelName.getText().toString());
                        MySharedPreferenceClass.setMobileNumber(getContext(), updateMobile.getText().toString());
                        MySharedPreferenceClass.setEmail(getContext(), updateMail.getText().toString());
                        MySharedPreferenceClass.setMyUserName(getContext(), updatefName.getText().toString() + " " + updatelName.getText().toString());


                    } else {
                        parentDialogListener.hideProgressDialog();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<ChangePassword> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {

            parentDialogListener = (ParentDialogsListener) context;

        } catch (ClassCastException e) {

            throw new ClassCastException(context.toString()
                    + " Activity's Parent should be Parent Activity");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
