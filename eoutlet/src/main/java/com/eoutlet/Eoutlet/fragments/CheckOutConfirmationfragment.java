package com.eoutlet.Eoutlet.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.eoutlet.Eoutlet.PaymentGateway.PaymentActivity;
import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.activities.ThankyouActivity;
import com.eoutlet.Eoutlet.adpters.CheckOutAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;

import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.listener.CartListener;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.ApplyCoupenResult;
import com.eoutlet.Eoutlet.pojo.GetShippingCharge;
import com.eoutlet.Eoutlet.pojo.GetToken;
import com.eoutlet.Eoutlet.pojo.OrderResponse;
import com.eoutlet.Eoutlet.pojo.OrderResponseCod;
import com.eoutlet.Eoutlet.pojo.PayFortData;
import com.eoutlet.Eoutlet.pojo.SignUpResponse;
import com.eoutlet.Eoutlet.pojo.ViewCart1;
import com.eoutlet.Eoutlet.pojo.ViewCartData;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.payfort.fort.android.sdk.base.FortSdk;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class CheckOutConfirmationfragment extends Fragment implements CartListener {


    View view;
    private int tax_rate_in_percentage;
    private RecyclerView recyclerViewCheckOut;
    private CheckOutAdapter checkOutAdapter;
    private ExecuteFragment execute;
    boolean islayoutvisible = false;
    private String orderId;
    private CartListener cartreference;
    public ParentDialogsListener parentDialogListener;
    private String deviceId = "", sdkToken = "";
    private FirebaseAnalytics mFirebaseAnalytics;
    public boolean stockflag = true;
    private ImageView crossimage;
    private TextView cityName;


    /*String ACCESS_CODE = "Se8GBcDzHUIqpRvyzkmT";
    private final static String MERCHANT_IDENTIFIER = "uRelyHKU";
    private final static String SHA_REQUEST_PHRASE = "TESTSHAIN";
    public final static String SHA_RESPONSE_PHRASE = "asdadseeerg";
*/
    String ACCESS_CODE = "2rV2I57jJhv3aRmWfAP7";//Live
    private final static String MERCHANT_IDENTIFIER = "VwaZsGPf";  //Live
    private final static String SHA_REQUEST_PHRASE = "$2y$10$RiIngXYpb"; //Live
    public final static String SHA_RESPONSE_PHRASE = "$2y$10$CaO9SBtke";//Live

    String LANGUAGE_TYPE = "ar";

    private final static String KEY_MERCHANT_IDENTIFIER = "merchant_identifier";
    private final static String KEY_SERVICE_COMMAND = "service_command";
    private final static String KEY_DEVICE_ID = "device_id";
    private final static String KEY_LANGUAGE = "language";
    private final static String KEY_ACCESS_CODE = "access_code";
    private final static String KEY_SIGNATURE = "signature";
    public final static String AUTHORIZATION = "AUTHORIZATION";
    public final static String PURCHASE = "PURCHASE";
    private final static String SDK_TOKEN = "SDK_TOKEN";
    private final static String SHA_TYPE = "SHA-256";


    public final static String CURRENCY_TYPE = "SAR";

    private ImageView radio1, radio2;

    private static String totaltax/* shippingcharges*/;
    private static TextView tvshippingcharge, tvtotaltax,giftwrappingcharge;
    int codcharges;
    boolean coupenflag = false;
    static int shippingchargebycountry = 0;
    String shippingtitle ="الشحن", shippingmethod;
    static int dicountamount;
    private String tax_apply = " ";
    private final Gson gson = new Gson();

    int radioflag = 0;

    private ImageView searchImage,backarrow;
    private Toolbar toolbar1;

    // private final static String TEST_TOKEN_URL = "http://dev.eoutlet.net/webservice/sbpaymentservices.php ";

    // private final static String TEST_TOKEN_URL = "https://sbpaymentservices.payfort.com/FortAPI/paymentApi";

    //private final static String TEST_TOKEN_URL = "https://paymentservices.payfort.com/FortAPI/paymentApi";
    private RelativeLayout confirmOrder;

    private TextView tvshippingtitle, applycode, tvaddressName, tvstreet, tvCity, tvCountry, tvPhone, tvtotalprice, tvtotalwithTax, newaddress;
    private List<ViewCartData> getdata = new ArrayList<>();
    private ArrayList<String> productPics = new ArrayList<>();
    private ArrayList<String> productname = new ArrayList<>();
    private ArrayList<String> productprice = new ArrayList<>();
    private ArrayList<String> productsize = new ArrayList<>();
    private ArrayList<String> productquant = new ArrayList<>();
    LinearLayout firstRadiobuttonlayout, secondRadioButtonlayout;
    String totalprice, totatlpricewithtax,giftwrapping = " ",code_label;
    private EditText edtcoupencode;
    private TextView codcharge, plusbutton, codtext;
    private String coupencode = " ", discounntamount = " ";

    private TextView coupenAmount, coupentext;
    private RelativeLayout coupenlayout/*, addresslayotut*/;

    public static RelativeLayout addresslayotut;
    private NestedScrollView scroll;

    private LinearLayout coupanmainlayout;

    private boolean is_giftwrap = false;
    private int priceaftercoupencodeappliy;


    private OnFragmentInteractionListener mListener;

    public CheckOutConfirmationfragment() {

    }

    public CheckOutConfirmationfragment(CartFragment cl) {

        cartreference = cl;


    }

    public static CheckOutConfirmationfragment newInstance(String param1, String param2) {
        CheckOutConfirmationfragment fragment = new CheckOutConfirmationfragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);


        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle b = getArguments();

            coupencode = b.getString("coupencode");
            System.out.println("coupancodecheckout" + coupencode);
            discounntamount = b.getString("discounntamount");
            giftwrapping = b.getString("giftwrapping");
            code_label = b.getString("cod_label");

            if(Integer.parseInt(giftwrapping.split(" ")[1])>0)
            {
                is_giftwrap = true;
            }


            System.out.println("discountamoutcheckout" + discounntamount);

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        productPics = new ArrayList<>();
        view = inflater.inflate(R.layout.fragment_check_out_confirmationfragment, container, false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());

        tvtotaltax = view.findViewById(R.id.cototaltax);
        giftwrappingcharge = view.findViewById(R.id.checkoutgiftwrappingcharge);

        tvshippingcharge = view.findViewById(R.id.shippingcharges1);
        coupenAmount = view.findViewById(R.id.discoutnamount);
        coupentext = view.findViewById(R.id.coupenname);
        tvshippingtitle = view.findViewById(R.id.shippingtitle);
        codcharge = view.findViewById(R.id.codcharge);
        codtext = view.findViewById(R.id.codtext);

        //plusbutton = view.findViewById(R.id.pluscoupencode);
        coupenlayout = view.findViewById(R.id.coupan_layout);
        addresslayotut = view.findViewById(R.id.addresslayout);
        scroll = view.findViewById(R.id.scroll);
        plusbutton = view.findViewById(R.id.plus_button);

        coupanmainlayout = view.findViewById(R.id.coupan_mainLayout);
        applycode = view.findViewById(R.id.coupansubmit);
        crossimage = view.findViewById(R.id.crossimage);
        //cityName = view.findViewById(R.id.cityName);



        if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {


            getCartData();


        } else {
            getCartDataforGuestUser();


        }
        crossimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtcoupencode.setEnabled(true);
                coupenflag = false;
                coupentext.setText("القسيمة");
                coupenAmount.setText("- SAR 0");


                System.out.println("discountamtcross....." + dicountamount);

                int amoutaftercoupenremove = Integer.parseInt( tvtotalwithTax.getText().toString().split(" ")[1]) + dicountamount + 0/*Integer.parseInt(totaltax.getText().toString().split(" ")[1])*/;

                System.out.println("amountaftercoupan applied....." + amoutaftercoupenremove);
                tvtotalwithTax.setText("SAR" + " " + amoutaftercoupenremove+" "+"إجمالي الطلب");


                edtcoupencode.setText("");
                dicountamount = 0;

            }
        });
        coupanmainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!islayoutvisible) {


                    Animation animShow = AnimationUtils.loadAnimation(getContext(), R.anim.view_show);


                    //coupenlayout.startAnimation( animShow );
                    coupenlayout.setVisibility(View.VISIBLE);
                    coupenlayout.requestFocus();


// Start the animation

                    islayoutvisible = true;




                } else {


                    Animation animHide = AnimationUtils.loadAnimation(getContext(), R.anim.view_hide);
                    coupenlayout.setVisibility(View.GONE);


                    islayoutvisible = false;


                }
            }
        });
        plusbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!islayoutvisible) {


                    Animation animShow = AnimationUtils.loadAnimation(getContext(), R.anim.view_show);


                    //coupenlayout.startAnimation( animShow );
                    coupenlayout.setVisibility(View.VISIBLE);
                    coupenlayout.requestFocus();

                    plusbutton.setText(" رمز الخصم - ");


// Start the animation

                    islayoutvisible = true;


                } else {

                    plusbutton.setText(" رمز الخصم + ");
                    Animation animHide = AnimationUtils.loadAnimation(getContext(), R.anim.view_hide);
                    coupenlayout.setVisibility(View.GONE);


                    islayoutvisible = false;

                }


            }
        });
        applycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edtcoupencode.getText().toString().equals("")) {
                    if (coupenflag){
                        return;
                    }
                    setApplycode2();








                } else {
                    Toast.makeText(getContext(), "من فضلك ادخل كود الرصيد بشكل صحيح", Toast.LENGTH_SHORT).show();

                }
            }
        });

        execute = (MainActivity) getActivity();

        initUi();
//        registerListener();


        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setUserHintcall() {


        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (Build.VERSION.SDK_INT >= 26) {
            transaction.setReorderingAllowed(false);
        }
        transaction.detach(this).attach
                (this).commit();
        radioflag = 0;
        edtcoupencode.setText("");
        coupenflag = false;
        islayoutvisible = false;




    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    private void initUi() {
        firstRadiobuttonlayout = view.findViewById(R.id.firstradioButton);
        secondRadioButtonlayout = view.findViewById(R.id.secondRadioButton);
        newaddress = view.findViewById(R.id.newaddress);
        recyclerViewCheckOut = view.findViewById(R.id.recyclerViewCheckOut);
        radio1 = view.findViewById(R.id.radio1);
        radio2 = view.findViewById(R.id.radio2);
        tvaddressName = view.findViewById(R.id.addressName);
        tvstreet = view.findViewById(R.id.street);
        tvCity = view.findViewById(R.id.city);
        tvCountry = view.findViewById(R.id.coutnryName);
        tvPhone = view.findViewById(R.id.mobileNumber);
        tvtotalprice = view.findViewById(R.id.totalprice);
        tvtotalwithTax = view.findViewById(R.id.totalpricewithTax);
        confirmOrder = view.findViewById(R.id.confirmOrder);
        applycode = view.findViewById(R.id.applycode);
        edtcoupencode = view.findViewById(R.id.edtcoupen);
        coupenAmount.setText("" + "-" + " " + "SAR" + " " + discounntamount);

        toolbar1 = view.findViewById(R.id.toolbar);
        searchImage = toolbar1.findViewById(R.id.serachbar);
        backarrow = toolbar1.findViewById((R.id.backarrow));

        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment prFrag = new SearchResultFragment();
                Bundle databund = new Bundle();


                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.profileContainer, prFrag)
                        .commit();



            }
        });

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });




        if (!coupencode.equals("")) {
            coupentext.setText("(" + coupencode + ")" + " " + "القسيمة");
        }


        newaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment prFrag = new NewAddressFragment(CheckOutConfirmationfragment.this);
                Bundle databund = new Bundle();
                databund.putString("flag", "fromCheckout");
                prFrag.setArguments(databund);
                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.checkoutContainer, prFrag)
                        .commit();


            }
        });





        if (MySharedPreferenceClass.getAdressname(getContext()) != null) {
            addresslayotut.setVisibility(View.VISIBLE);
            tvaddressName.setText(MySharedPreferenceClass.getAdressname(getContext()));
            tvstreet.setText(MySharedPreferenceClass.getStreetname(getContext())) /*+" "+MySharedPreferenceClass.getCityName(getContext())+" " +MySharedPreferenceClass.getCountryName(getContext()))*/;
            tvCity.setText(MySharedPreferenceClass.getCityName(getContext()));
            tvCountry.setText(MySharedPreferenceClass.getCountryName(getContext()));
            //cityName.setText(MySharedPreferenceClass.getCityName(getContext()));


            tvPhone.setText("رقم الجوال : "+MySharedPreferenceClass.getAdressphone(getContext()));
            System.out.println("mobile../"+MySharedPreferenceClass.getAdressphone(getContext()));

        } else {
            addresslayotut.setVisibility(View.GONE);
        }


        applycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtcoupencode.getText().toString().equals("")) {
                    if (!coupenflag) {


                        setApplycode2();


                    } else {
                        int finaltotalvalue = 0;
                        coupenflag = false;
                        applycode.setText("تطبيق الرصيد");
                        edtcoupencode.setEnabled(true);
                        edtcoupencode.setText(null);

                        if(!MySharedPreferenceClass.getCheckboxflag(getContext())) {
                            int tvtotalpriceafterremovecoupen = Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) + Integer.parseInt(tvshippingcharge.getText().toString().split(" ")[1]);
                            tvtotalwithTax.setText("SAR" + " " +String.valueOf(tvtotalpriceafterremovecoupen )+" "+" الاجمالي الكلي");

                        }

                        else{

                            int tvtotalpriceafterremovecoupen = Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) + Integer.parseInt(tvshippingcharge.getText().toString().split(" ")[1])+Integer.parseInt(giftwrapping);
                            tvtotalwithTax.setText("SAR" + " " +String.valueOf(tvtotalpriceafterremovecoupen )+" "+" الاجمالي الكلي");

                        }




                        coupentext.setText("القسيمة");





                    }
                } else {
                    Toast.makeText(getContext(), "من فضلك ادخل كود الرصيد بشكل صحيح", Toast.LENGTH_SHORT).show();

                }

            }
        });


        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {


                if (MySharedPreferenceClass.getMyUserId(getContext()) == " " || MySharedPreferenceClass.getEmail(getContext()) == null) {
                    openSignindialog();

                    parentDialogListener.hideProgressDialog();
                    MainActivity.notificationBadge.setVisibility(View.GONE);
                    MySharedPreferenceClass.setMyUserId(getContext(), null);
                    MySharedPreferenceClass.setMyUserName(getContext(), null);
                    MySharedPreferenceClass.setMyFirstNamePref(getContext(), null);
                    MySharedPreferenceClass.setMyLastNamePref(getContext(), null);
                    MySharedPreferenceClass.setBedgeCount(getContext(), 0);
                    MySharedPreferenceClass.setEmail(getContext(), null);


                    MySharedPreferenceClass.setAddressname(getContext(), null);
                    MySharedPreferenceClass.setCityname(getContext(), null);
                    MySharedPreferenceClass.setStreetName(getContext(), null);
                    MySharedPreferenceClass.setCountryname(getContext(), null);

                    MySharedPreferenceClass.setAddressphone(getContext(), null);
                    MySharedPreferenceClass.setCountryId(getContext(), null);
                    MySharedPreferenceClass.setfirstAdressname(getContext(), null);
                    MySharedPreferenceClass.setlastAdressname(getContext(), null);
                    MySharedPreferenceClass.setSelecteAddressdId(getContext(), "0");

                } else {

                    //placeorderforpayfort();


                    if (MySharedPreferenceClass.getAdressname(getContext()) != null) {
                        if (Integer.valueOf(tvtotalwithTax.getText().toString().split(" ")[1]) > 0) {
                            Fragment prFrag = new PaymentOptionsFragment();
                            Bundle bundle = new Bundle();


                            bundle.putString("shippingmethod", shippingmethod);
                            bundle.putString("amount", tvtotalwithTax.getText().toString().split(" ")[1]);
                            bundle.putString("codcharges", String.valueOf(codcharges));
                            bundle.putString("codelabel",code_label);
                            bundle.putInt("shippingcharge",  Integer.parseInt(tvshippingcharge.getText().toString().split(" ")[1]));
                            bundle.putBoolean("wrappingflag",is_giftwrap);


                            if(coupenflag) {
                                bundle.putString("coupencode", edtcoupencode.getText().toString().trim());
                            }
                            else{
                                bundle.putString("coupencode", edtcoupencode.getText().toString());


                            }


                            prFrag.setArguments(bundle);

                            getFragmentManager()
                                    .beginTransaction().addToBackStack(null)
                                    .add(R.id.cartcontainer, prFrag)
                                    .commit();

                        }

                        else{

                            placeorderforfree();


                        }


                    } else {

                        Toast.makeText(getContext(), "الرجاء إضافة عنوان واحد على الأقل في دفتر العناوين", Toast.LENGTH_LONG).show();


                    }




                }

            }
        });
    }

    private class GetTokenFromServer extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            parentDialogListener.showProgressDialog();
        }

        @Override
        protected String doInBackground(String... postParams) {
            String response = "";
            try {
                HttpURLConnection conn;
                URL url = new URL(postParams[0].replace(" ", "%20"));
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("content-type", "application/json");

                String str = getTokenParams();
                byte[] outputInBytes = str.getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputInBytes);
                os.close();
                conn.connect();

                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = conn.getInputStream();
                    response = convertStreamToString(inputStream);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            parentDialogListener.hideProgressDialog();
            Log.e("Response", response + "");
            try {

                try {
                    PayFortData payFortData = gson.fromJson(response, PayFortData.class);
                    if (!TextUtils.isEmpty(payFortData.sdkToken)) {
                        sdkToken = payFortData.sdkToken;

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent i = new Intent(getActivity(), PaymentActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("mainData", (Serializable) getdata);
                bundle.putString("totalprice", totalprice.split(" ")[1]);
                bundle.putString("token", sdkToken);
                bundle.putString("shippingmethod", shippingmethod);
                if (coupenflag) {
                    bundle.putString("coupon", coupentext.getText().toString());

                } else {

                    bundle.putString("coupon", coupentext.getText().toString());
                }

                //bundle.putString("price", totatlpricewithtax.split(" ")[1]);

                bundle.putString("price", tvtotalwithTax.getText().toString().split(" ")[1]);
                i.putExtras(bundle);


                startActivity(i);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void placeorder() {

        HashMap<Object, Object> initialorder = new HashMap<>();
        initialorder.put("user_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("devicetype", "android");

        initialorder.put("payment_method", "msp_cashondelivery");
        initialorder.put("shipping_method", shippingmethod);
        if (coupenflag) {
            initialorder.put("coupon", edtcoupencode.getText().toString());
        } else {

            initialorder.put("coupon", " ");


        }

        HashMap<Object, Object> payment = new HashMap<>();
        payment.put("firstname", MySharedPreferenceClass.getMyFirstNamePref(getContext()));
        payment.put("lastname", MySharedPreferenceClass.getMyLastNamePref(getContext()));
        payment.put("city", tvCity.getText().toString());

        payment.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        payment.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        payment.put("telephone", MySharedPreferenceClass.getMobileNumber(getContext()));

        HashMap<String, String> street = new HashMap<>();
        street.put("0", tvstreet.getText().toString());
        street.put("1", " ");
        payment.put("street", street);

        initialorder.put("payment", payment);

        HashMap<Object, Object> shipping = new HashMap<>();

        shipping.put("firstname", MySharedPreferenceClass.getfirstAdressname(getContext()));
        shipping.put("lastname", MySharedPreferenceClass.getlastAdressname(getContext()));
        shipping.put("street", street);
        shipping.put("city", tvCity.getText().toString());
        shipping.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("telephone", tvPhone.getText().toString());

        initialorder.put("shipping", shipping);


        HashMap<String, String> products = new HashMap<>();


        ArrayList<Object> mainarraylist = new ArrayList<>();


        for (int i = 0; i < getdata.size(); i++) {

            HashMap<String, Object> superattribute = new HashMap<>();

            HashMap<String, String> colorandsize = new HashMap<>();

            if (getdata.get(i).option.size() > 0) {
                colorandsize.put(getdata.get(i).option.get(0).color.id.toString(), getdata.get(i).option.get(0).color.value.toString());
                colorandsize.put(getdata.get(i).option.get(0).size.id.toString(), getdata.get(i).option.get(0).size.value.toString());
                superattribute.put("super_attribute", colorandsize);

            }

            superattribute.put("product_id", getdata.get(i).productId);
            superattribute.put("qty", getdata.get(i).qty);

            mainarraylist.add(superattribute);

        }


        initialorder.put("products", mainarraylist);


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<OrderResponse> call = apiService.createorder(initialorder);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {


                if (response.body() != null) {


                 /*   if (response.body().msg.equals("success")) {
                        parentDialogListener.hideProgressDialog();
                        //MySharedPreferenceClass.setAddressname(getContext(),null);
                        // MySharedPreferenceClass.setCityname(getContext(), null);
                        // MySharedPreferenceClass.setStreetName(getContext(), null);
                        // MySharedPreferenceClass.setCountryname(getContext(), null);
                        // MySharedPreferenceClass.setCountryId(getContext(), null);

                        MySharedPreferenceClass.setBedgeCount(getContext(), 0);
                        //Toast.makeText(getContext(), "Order Placed Successful", Toast.LENGTH_LONG).show();

                        Intent in = new Intent(getContext(), ThankyouActivity.class);
                        in.putExtra("orderId", response.body().orderId);
                        in.putExtra("totalvalue", tvtotalwithTax.getText().toString().split(" ")[1]);
                        in.putExtra("coupencode", edtcoupencode.getText().toString());


                        getContext().startActivity(in);

                        getFragmentManager().popBackStack();


                    } else {
                        openNavigatetoCartdialog(response.body().data.toString());
                        parentDialogListener.hideProgressDialog();
                        //Toast.makeText(getContext(), response.body().msg, Toast.LENGTH_LONG).show();

                    }
*/

                }


            }


            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                openNavigatetoCartdialog("بعض المنتجات غير متوفرة بالمخزون");
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString() + call.toString());
                Toast.makeText(getContext(), call.toString(), Toast.LENGTH_LONG).show();

                // Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Toast.makeText(getContext(),"dsfdsfds",Toast.LENGTH_SHORT).show();
        try {

            parentDialogListener = (ParentDialogsListener) activity;

        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }


    public String getTokenParams() {
        JSONObject jsonObject = new JSONObject();
        try {
            String device_id = FortSdk.getDeviceId(getContext());
            String concatenatedString = SHA_REQUEST_PHRASE +

                    KEY_ACCESS_CODE + "=" + ACCESS_CODE +
                    KEY_DEVICE_ID + "=" + device_id +
                    KEY_LANGUAGE + "=" + LANGUAGE_TYPE +
                    KEY_MERCHANT_IDENTIFIER + "=" + MERCHANT_IDENTIFIER +
                    KEY_SERVICE_COMMAND + "=" + SDK_TOKEN +


                    SHA_REQUEST_PHRASE;

            jsonObject.put(KEY_SERVICE_COMMAND, SDK_TOKEN);
            jsonObject.put(KEY_MERCHANT_IDENTIFIER, MERCHANT_IDENTIFIER);
            jsonObject.put(KEY_ACCESS_CODE, ACCESS_CODE);
            String signature = getSignatureSHA256(concatenatedString);
            jsonObject.put(KEY_SIGNATURE, signature);
            jsonObject.put(KEY_DEVICE_ID, device_id);
            jsonObject.put(KEY_LANGUAGE, LANGUAGE_TYPE);


            Log.e("concatenatedString", concatenatedString);
            Log.e("signature", signature);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JsonString", String.valueOf(jsonObject));
        return String.valueOf(jsonObject);
    }

    private static String convertStreamToString(InputStream inputStream) {
        if (inputStream == null)
            return null;
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream), 1024);
            String line;
            while ((line = r.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private static String getSignatureSHA256(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(SHA_TYPE);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            return String.format("%0" + (messageDigest.length * 2) + 'x', new BigInteger(1, messageDigest));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void openSignindialog() {
        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage("يجب تسجيل الدخول أولا");


        alertDialogBuilder.setPositiveButton("نعم",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //  execute.ExecutFragmentListener(1);
                        Fragment prFrag = new LoginNewFragment();
                        Bundle databund = new Bundle();
                        databund.putString("flag", "fromcart");

                        prFrag.setArguments(databund);


                        getFragmentManager()
                                .beginTransaction().addToBackStack(null)
                                .replace(R.id.cartcontainer, prFrag)
                                .commit();
                    }
                });

        alertDialogBuilder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {
                //alertDialog.dismiss();

            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void setApplycode2() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();

        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("coupon", edtcoupencode.getText().toString());
        query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));
        query.put("mask_key", String.valueOf(MySharedPreferenceClass.getMaskkey(getContext())));
        query.put("subtotal",  tvtotalprice.getText().toString().split(" ")[1]);
        query.put("shippingcharge", tvshippingcharge.getText().toString().split(" ")[1]);

        Call<ApplyCoupenResult> call = apiService.applycode(query);
        call.enqueue(new Callback<ApplyCoupenResult>() {
            @Override
            public void onResponse(Call<ApplyCoupenResult> call, Response<ApplyCoupenResult> response) {


                if (response.body() != null) {


                    parentDialogListener.hideProgressDialog();


                    dicountamount = Integer.parseInt(response.body().discountAmount);
                    System.out.println("discountamt....." + response.body().discountAmount);

                    int amoutaftercoupenapplied = Integer.parseInt( tvtotalwithTax.getText().toString().split(" ")[1]) - dicountamount + 0/*Integer.parseInt(totaltax.getText().toString().split(" ")[1])*/;
                    System.out.println("amountaftercoupan applied....." + amoutaftercoupenapplied);


                    if (amoutaftercoupenapplied >= 0) {
                        coupenAmount.setText("" + "-" + " " + "SAR" + " " + response.body().discountAmount);
                        coupentext.setText("(" + edtcoupencode.getText().toString() + ")" + " " + "القسيمة");
                        tvtotalwithTax.setText("SAR" + " " + amoutaftercoupenapplied+" "+" الاجمالي الكلي");

                        applycode.setText("إلغاء القسيمة");
                        edtcoupencode.setEnabled(false);

                        coupenflag = true;
                    } else {
                        int amtwithoutcoupanapplied = Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) - dicountamount + 0/*Integer.parseInt(totaltax.getText().toString().split(" ")[1])*/;


                        tvtotalwithTax.setText("SAR" + " " + amoutaftercoupenapplied+" "+" الاجمالي الكلي");

                    }





                } else {
                    parentDialogListener.hideProgressDialog();


                }

            }


            @Override
            public void onFailure(Call<ApplyCoupenResult> call, Throwable t) {
                parentDialogListener.hideProgressDialog();

                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }



    public void getShippingrateinfo() {


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();


        query.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<GetShippingCharge> call = apiService.getshippingchargebycountry(query);
        call.enqueue(new Callback<GetShippingCharge>() {
            @Override
            public void onResponse(Call<GetShippingCharge> call, Response<GetShippingCharge> response) {


                if (response.body() != null) {

                    parentDialogListener.hideProgressDialog();

                    if (response.body().msg.equals("success")) {

                        shippingchargebycountry = response.body().shipping;
                        shippingtitle = response.body().shipping_title;
                        shippingmethod = response.body().shipping_method;

                        tvshippingcharge.setText("SAR" + " " + String.valueOf(shippingchargebycountry));
                        tvshippingtitle.setText(shippingtitle);
                        if(MySharedPreferenceClass.getCheckboxflag(getContext())) {

                            int totalwithshippngcharge = Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) + shippingchargebycountry + Integer.parseInt(giftwrapping)/*+ totaltax2*/;
                            tvtotalwithTax.setText("SAR" + " " + totalwithshippngcharge+" "+" الاجمالي الكلي");
                            giftwrappingcharge.setText("SAR"+" "+giftwrapping);
                        }
                        else {
                            int totalwithshippngcharge = Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) + shippingchargebycountry;

                            tvtotalwithTax.setText("SAR" + " " + totalwithshippngcharge+" "+" الاجمالي الكلي");



                        }


                        if (shippingchargebycountry != 0) {


                            try {


                                int totaltax2 = getTaxCalculation(Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) + shippingchargebycountry, response.body().shipping_tax);


                                tvtotaltax.setText("SAR" + " " + totaltax2);


                                totaltax = "SAR" + " " + totaltax2;








                            } catch (Exception e) {


                            }
                        }


                    } else {

                        tvshippingtitle.setText("الشحن");
                        parentDialogListener.hideProgressDialog();
                    }


                }

            }


            @Override
            public void onFailure(Call<GetShippingCharge> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                // progressDialog.hide();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void getCartData() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("mask_key", "");
        query.put("cart_id", "");

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {


                int quant;

                if (response.body() != null) {
                    tax_apply = response.body().apply_tax;

                    tax_rate_in_percentage = response.body().goodstax_rate;


                    if (response.body().msg.equalsIgnoreCase("success")) {
                        //  parentDialogListener.hideProgressDialog();

                        getdata = response.body().data;

                        if (MySharedPreferenceClass.getCountryId(getContext())!=null && MySharedPreferenceClass.getCountryId(getContext()).equals("SA")) {
                            totalprice = "SAR" + " " + response.body().subtotal;

                            totaltax = "SAR" + " " + response.body().tax;

                            // totatlpricewithtax = "SAR" + " " + response.body().grandtotal;
                            totatlpricewithtax = "SAR" + " " + response.body().subtotal;
                            tvtotaltax.setText(totaltax);

                            tvtotalprice.setText(totalprice);


                            tvtotalwithTax.setText("SAR" + " " +totatlpricewithtax+" "+" الاجمالي الكلي");

                            codcharges = response.body().cod_charges;

                            codcharge.setText("SAR" + " " + codcharges);
                            giftwrapping = response.body().gift_wrap_fee;
                            //int totaolfinalprice = (Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) - Integer.parseInt(discounntamount) + shippingchargebycountry + Integer.parseInt(tvtotaltax.getText().toString().split(" ")[1]));

                            int totaolfinalprice = (Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) - Integer.parseInt(discounntamount) + shippingchargebycountry +0);

                            tvtotalwithTax.setText("SAR" + " " + Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1])+" "+" الاجمالي الكلي");


                            System.out.println("totalfinalprice......" + totaolfinalprice);
                            System.out.println("totalfinalprice......" + totaolfinalprice);
                            System.out.println("tvtotalprice..." + tvtotalprice.getText().toString().split(" ")[1]);
                            System.out.println("shippingchargebycountry" + shippingchargebycountry);
                            System.out.println("totaltax...." + Integer.parseInt(tvtotaltax.getText().toString().split(" ")[1]));
                            // codtext.setText("الدفع عند الاستلام + codcharges + ريال");

                            // codtext.setText("ريال"+" "+codcharges+" "+ "الدفع عند الاستلام");

                            codtext.setText("الدفع عند الاستلام" + " " + "+" + " " + codcharges + " " + "ريال");
                        } else {
                            totalprice = "SAR" + " " + response.body().subtotal;

                            totaltax = "SAR" + " " + "0";

                            totatlpricewithtax = "SAR" + " " + response.body().subtotal;

                            tvtotaltax.setText(totaltax);

                            tvtotalprice.setText(totalprice);


                            tvtotalwithTax.setText(totatlpricewithtax+" "+" الاجمالي الكلي");

                            codcharges = response.body().cod_charges;
                            giftwrapping = response.body().gift_wrap_fee;

                            codcharge.setText("SAR" + " " + codcharges);


                            int totaolfinalprice = (Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) - Integer.parseInt(discounntamount) + shippingchargebycountry /*+ Integer.parseInt(tvtotaltax.getText().toString().split(" ")[1])*/);
                            tvtotalwithTax.setText("SAR" + " " + String.valueOf(totaolfinalprice)+" "+" الاجمالي الكلي");


                            System.out.println("totalfinalprice......" + totaolfinalprice);
                            System.out.println("totalfinalprice......" + totaolfinalprice);
                            System.out.println("tvtotalprice..." + tvtotalprice.getText().toString().split(" ")[1]);
                            System.out.println("shippingchargebycountry" + shippingchargebycountry);
                            System.out.println("totaltax...." + Integer.parseInt(tvtotaltax.getText().toString().split(" ")[1]));
                            // codtext.setText("الدفع عند الاستلام + codcharges + ريال");

                            // codtext.setText("ريال"+" "+codcharges+" "+ "الدفع عند الاستلام");

                            codtext.setText("الدفع عند الاستلام" + " " + "+" + " " + codcharges + " " + "ريال");

                        }


                        if (MySharedPreferenceClass.getCountryId(getContext()) != null) {
                            getShippingrateinfo();

                        } else {
                            if(MySharedPreferenceClass.getCheckboxflag(getContext())) {

                                int totalwithshippngcharge = Integer.parseInt(tvtotalprice.getText().toString().split(" ")[1]) + /*shippingchargebycountry*/0 + Integer.parseInt(giftwrapping)/*+ totaltax2*/;
                                tvtotalwithTax.setText("SAR" + " " + totalwithshippngcharge+" "+" الاجمالي الكلي");
                                giftwrappingcharge.setText("SAR"+" "+giftwrapping);
                            }
                            tvshippingtitle.setText(shippingtitle);
                            parentDialogListener.hideProgressDialog();
                        }
                        initrecycler();
                        appsflyer_init_checkout();
                        firebase_init_checkout();
                        firebase_payment_info();


                    } else {

                        if (MySharedPreferenceClass.getCountryId(getContext()) != null) {
                            getShippingrateinfo();
                        }


                    }
                }


            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                //   Toast.makeText(getContext(), "No Items Found", Toast.LENGTH_LONG).show();

            }
        });


    }

    public void getCartDataforGuestUser() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", " ");
        query.put("mask_key", MySharedPreferenceClass.getMaskkey(getContext()));
        query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {


                int quant;

                if (response.body() != null) {


                    if (response.body().msg.equals("success")) {
                        //parentDialogListener.hideProgressDialog();

                        getdata = response.body().data;


                        totalprice = response.body().subtotal;
                        totaltax = response.body().tax;
                        totatlpricewithtax = response.body().grandtotal;
                        codcharges = response.body().cod_charges;
                        tvtotaltax.setText(totaltax);
                        codcharge.setText("SAR" + " " + codcharges);
                        giftwrapping = response.body().gift_wrap_fee;

                        if (MySharedPreferenceClass.getCountryId(getContext()) != null) {
                            getShippingrateinfo();
                        }


                        initrecycler();
                        appsflyer_init_checkout();
                        firebase_init_checkout();
                        firebase_payment_info();

                    } else {
                        // parentDialogListener.hideProgressDialog();
                        if (MySharedPreferenceClass.getCountryId(getContext()) != null) {
                            getShippingrateinfo();
                        }

                    }


                }


            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "No Items Found", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void initrecycler() {

        productPics = new ArrayList<>();


        for (int i = 0; i < getdata.size(); i++) {

            productPics.add(getdata.get(i).img);
            productname.add(getdata.get(i).name);
            productprice.add(getdata.get(i).price);
            if (getdata.get(i).option.size() > 0 && getdata.get(i).option.get(0).size.label != null) {
                productsize.add(getdata.get(i).option.get(0).size.label.toString());
            } else {
                productsize.add(" ");
            }
            productquant.add((String.valueOf(getdata.get(i).qty)));


        }


        recyclerViewCheckOut.setHasFixedSize(true);
        if (productPics.size() == 1) {
            recyclerViewCheckOut.setPadding(6,0,6,0);
            recyclerViewCheckOut.setVerticalScrollBarEnabled(false);
            recyclerViewCheckOut.getLayoutParams().height = (int) getResources().getDimension(R.dimen.scroller_height);



            final float scale = getResources().getDisplayMetrics().density;

            int dpHeightInPx = (int) (R.dimen.scroller_height * scale);

            LinearLayout.LayoutParams lp =
                    new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.leftMargin = 0;

            recyclerViewCheckOut.setLayoutParams(lp);




        }
        else{
            // final float scale = getResources().getDisplayMetrics().density;

            //int dpHeightInPx = (int) (R.dimen.scroller_height * scale);
            //recyclerViewCheckOut.setVerticalScrollBarEnabled(true);

          /*  LinearLayout.LayoutParams lp =
                    new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, dpHeightInPx);
            lp.leftMargin = 15;*/
            //   recyclerViewCheckOut.setLayoutParams(lp);
            recyclerViewCheckOut.setPadding(0,0,6,0);
        }

        // use a linear layout manager



        recyclerViewCheckOut.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        // specify an adapter (see also next example)
        checkOutAdapter = new CheckOutAdapter(productPics, productname, productprice, productsize, productquant);
        recyclerViewCheckOut.setAdapter(checkOutAdapter);
        checkOutAdapter.notifyDataSetChanged();

    }


    public void getToken() {
        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Map<String, String> map1 = new HashMap<>();
        map1.put("device_id", FortSdk.getDeviceId(getContext()));

        Call<GetToken> call = apiService.getTokenResponse(map1);
        call.enqueue(new Callback<GetToken>() {
            @Override
            public void onResponse(Call<GetToken> call, Response<GetToken> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();

                    sdkToken = response.body().sdkToken;
                    Intent i = new Intent(getActivity(), PaymentActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("mainData", (Serializable) getdata);
                    bundle.putString("totalprice", totalprice.split(" ")[1]);
                    bundle.putString("token", sdkToken);
                    bundle.putString("shippingmethod", shippingmethod);
                    bundle.putString("oredrId", orderId);
                    bundle.putString("coupencode", edtcoupencode.getText().toString());


                    bundle.putString("price", tvtotalwithTax.getText().toString().split(" ")[1]);
                    i.putExtras(bundle);


                    startActivity(i);


                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<GetToken> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });
    }


    public int getTaxCalculation(double amount, int tax_rate) {

        double taxapplied = (amount * tax_rate) / 100;

        System.out.println("Tax Applied-->" + taxapplied);

        long roundedtax = Math.round(taxapplied);


        return (int) roundedtax;

    }

    public void placeorderforpayfort() {

        HashMap<Object, Object> initialorder = new HashMap<>();
        initialorder.put("amount","500");
        initialorder.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("remember_me","NO");
        initialorder.put("is_partial","0");

        initialorder.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("user_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("payment_method", "payfort_fort_cc");
        initialorder.put("shipping_method", shippingmethod);
        initialorder.put("devicetype", "android");

        if (coupenflag) {
            initialorder.put("coupon", edtcoupencode.getText().toString());
        } else {

            initialorder.put("coupon", " ");


        }





        HashMap<Object, Object> payment = new HashMap<>();
        payment.put("firstname", MySharedPreferenceClass.getMyFirstNamePref(getContext()));
        payment.put("lastname", MySharedPreferenceClass.getMyLastNamePref(getContext()));
        payment.put("city", MySharedPreferenceClass.getCityName(getContext()));

        payment.put("country_id", "SA");
        payment.put("region", "Riyadh");
        payment.put("telephone", MySharedPreferenceClass.getMobileNumber(getContext()));

        HashMap<String, String> street = new HashMap<>();
        street.put("0", MySharedPreferenceClass.getStreetname(getContext()));
        street.put("1", " ");
        payment.put("street", street);

        initialorder.put("payment", payment);


        HashMap<Object, Object> paymentdetail = new HashMap<>();
        paymentdetail.put("name","Arpan");
        paymentdetail.put("number","4005550000000001");
        paymentdetail.put("securitycode","123");
        paymentdetail.put("expiry","2105");

        initialorder.put("card_detail", paymentdetail);



        HashMap<Object, Object> shipping = new HashMap<>();

        shipping.put("firstname", MySharedPreferenceClass.getfirstAdressname(getContext()));
        shipping.put("lastname", MySharedPreferenceClass.getlastAdressname(getContext()));
        shipping.put("street", street);
        shipping.put("city", MySharedPreferenceClass.getCityName(getContext()));
        shipping.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("telephone", MySharedPreferenceClass.getAdressphone(getContext()));

        initialorder.put("shipping", shipping);


        HashMap<String, String> products = new HashMap<>();


        ArrayList<Object> mainarraylist = new ArrayList<>();


        for (int i = 0; i < getdata.size(); i++) {

            HashMap<String, Object> superattribute = new HashMap<>();
            //HashMap<String,String>  size = new HashMap<>();
            HashMap<String, String> colorandsize = new HashMap<>();

            if (getdata.get(i).option.size() > 0) {
                colorandsize.put(getdata.get(i).option.get(0).color.id.toString(), getdata.get(i).option.get(0).color.value.toString());
                colorandsize.put(getdata.get(i).option.get(0).size.id.toString(), getdata.get(i).option.get(0).size.value.toString());
                superattribute.put("super_attribute", colorandsize);

            }

            superattribute.put("product_id", getdata.get(i).productId);
            superattribute.put("qty", getdata.get(i).qty);

            mainarraylist.add(superattribute);

        }


        initialorder.put("products", mainarraylist);
        initialorder.put("transaction_id", " ");


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<OrderResponse> call = apiService.createorder(initialorder);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {


                if (response.body() != null) {

/*

                    if (response.body().msg.equals("success")) {
                        parentDialogListener.hideProgressDialog();
                        orderId = response.body().orderId;
                       //getToken();


                    } else {
                        openNavigatetoCartdialog(response.body().data.toString());
                        parentDialogListener.hideProgressDialog();
                        // Toast.makeText(getContext(), response.body().msg, Toast.LENGTH_LONG).show();

                    }
*/


                }


            }


            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                openNavigatetoCartdialog("بعض المنتجات غير متوفرة بالمخزون");

                //Toast.makeText(getContext(), "Shipping method is not available for this address.", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void appsflyer_init_checkout() {
        Map<String, Object> eventValue = new HashMap<String, Object>();

        eventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, MySharedPreferenceClass.getMyUserId(getContext()));
        eventValue.put(AFInAppEventParameterName.CURRENCY, "SAR");
        eventValue.put(AFInAppEventParameterName.PRICE, tvtotalwithTax.getText().toString());

        eventValue.put(AFInAppEventParameterName.QUANTITY, " ");

        eventValue.put(AFInAppEventParameterName.PAYMENT_INFO_AVAILIBLE, " ");
        eventValue.put(AFInAppEventParameterName.COUPON_CODE, edtcoupencode.getText().toString());

        eventValue.put(AFInAppEventParameterName.CONTENT_ID, " ");


        AppsFlyerLib.getInstance().trackEvent(getContext(), AFInAppEventType.INITIATED_CHECKOUT, eventValue);


    }


    public void firebase_init_checkout() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.COUPON, edtcoupencode.getText().toString());
        bundle.putString(FirebaseAnalytics.Param.CURRENCY, "SAR");
        bundle.putInt(FirebaseAnalytics.Param.VALUE, Integer.valueOf(tvtotalwithTax.getText().toString().split(" ")[1]));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT, bundle);
    }


    public void firebase_payment_info() {
        Bundle bundle = new Bundle();

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, bundle);


    }


    public boolean getstatusofCartItems() {

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("mask_key", "");
        query.put("cart_id", "");

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {


                int quant;

                if (response.body() != null) {


                    if (response.body().msg.equalsIgnoreCase("success")) {


                        for (int i = 0; i < response.body().data.size(); i++) {

                            if (response.body().data.get(i).instock == 0) {
                                openNavigatetoCartdialog("بعض المنتجات غير متوفرة بالمخزون");

                                Toast.makeText(getContext(), "بعض المنتجات غير متوفرة بالمخزون", Toast.LENGTH_SHORT).show();

                                stockflag = false;


                            }


                        }


                    }
                }


            }

            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

            }
        });

        return stockflag;


    }


    public void openNavigatetoCartdialog(String message) {
        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage(message);


        alertDialogBuilder.setPositiveButton("موافق",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
//if(cartreference!=null){
                        cartreference.setUserHintcall();//}
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    }


                });

        alertDialogBuilder.setNegativeButton("إلغاء", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();


            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void placeorderforfree()
    {


        HashMap<Object, Object> initialorder = new HashMap<>();
        initialorder.put("user_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("devicetype", "android");

        initialorder.put("payment_method", "free");
        initialorder.put("shipping_method", shippingmethod);
        if (coupenflag) {
            initialorder.put("coupon", edtcoupencode.getText().toString());
        } else {

            initialorder.put("coupon", " ");


        }

        HashMap<Object, Object> payment = new HashMap<>();
        payment.put("firstname", MySharedPreferenceClass.getMyFirstNamePref(getContext()));
        payment.put("lastname", MySharedPreferenceClass.getMyLastNamePref(getContext()));
        payment.put("city", tvCity.getText().toString());

        payment.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        payment.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        payment.put("telephone", MySharedPreferenceClass.getMobileNumber(getContext()));

        HashMap<String, String> street = new HashMap<>();
        street.put("0", tvstreet.getText().toString());
        street.put("1", " ");
        payment.put("street", street);

        initialorder.put("payment", payment);

        HashMap<Object, Object> shipping = new HashMap<>();

        shipping.put("firstname", MySharedPreferenceClass.getfirstAdressname(getContext()));
        shipping.put("lastname", MySharedPreferenceClass.getlastAdressname(getContext()));
        shipping.put("street", street);
        shipping.put("city", tvCity.getText().toString());
        shipping.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("telephone", tvPhone.getText().toString());

        initialorder.put("shipping", shipping);


        HashMap<String, String> products = new HashMap<>();


        ArrayList<Object> mainarraylist = new ArrayList<>();


        for (int i = 0; i < getdata.size(); i++) {

            HashMap<String, Object> superattribute = new HashMap<>();

            HashMap<String, String> colorandsize = new HashMap<>();

            if (getdata.get(i).option.size() > 0) {
                colorandsize.put(getdata.get(i).option.get(0).color.id.toString(), getdata.get(i).option.get(0).color.value.toString());
                colorandsize.put(getdata.get(i).option.get(0).size.id.toString(), getdata.get(i).option.get(0).size.value.toString());
                superattribute.put("super_attribute", colorandsize);

            }

            superattribute.put("product_id", getdata.get(i).productId);
            superattribute.put("qty", getdata.get(i).qty);

            mainarraylist.add(superattribute);

        }


        initialorder.put("products", mainarraylist);


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<OrderResponseCod> call = apiService.createorderCod(initialorder);
        call.enqueue(new Callback<OrderResponseCod>() {
            @Override
            public void onResponse(Call<OrderResponseCod> call, Response<OrderResponseCod> response) {


                if (response.body() != null) {


                    if (response.body().msg.equals("success")) {
                        parentDialogListener.hideProgressDialog();
                        //MySharedPreferenceClass.setAddressname(getContext(),null);
                        // MySharedPreferenceClass.setCityname(getContext(), null);
                        // MySharedPreferenceClass.setStreetName(getContext(), null);
                        // MySharedPreferenceClass.setCountryname(getContext(), null);
                        // MySharedPreferenceClass.setCountryId(getContext(), null);

                        MySharedPreferenceClass.setBedgeCount(getContext(), 0);
                        //Toast.makeText(getContext(), "Order Placed Successful", Toast.LENGTH_LONG).show();

                        Intent in = new Intent(getContext(), ThankyouActivity.class);
                        in.putExtra("orderId", response.body().orderId);
                        in.putExtra("totalvalue", tvtotalwithTax.getText().toString().split(" ")[1]);
                        in.putExtra("coupencode", edtcoupencode.getText().toString());


                        getContext().startActivity(in);

                        getFragmentManager().popBackStack();


                    } else {
                        openNavigatetoCartdialog(response.body().data.toString());
                        parentDialogListener.hideProgressDialog();
                        //Toast.makeText(getContext(), response.body().msg, Toast.LENGTH_LONG).show();

                    }


                }


            }


            @Override
            public void onFailure(Call<OrderResponseCod> call, Throwable t) {
                //  openNavigatetoCartdialog("بعض المنتجات غير متوفرة بالمخزون");
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString() + call.toString());


                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }
}






