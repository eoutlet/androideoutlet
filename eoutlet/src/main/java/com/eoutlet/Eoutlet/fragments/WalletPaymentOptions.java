package com.eoutlet.Eoutlet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.util.Calendar;
import android.icu.util.TimeZone;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.activities.Threedpasswordactivity;
import com.eoutlet.Eoutlet.adpters.AZAdapter;
import com.eoutlet.Eoutlet.adpters.CartAdpter;
import com.eoutlet.Eoutlet.adpters.HelpSupportAdapter;
import com.eoutlet.Eoutlet.adpters.RememberCardAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.listener.ViewListener3;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.AddMoneyResponse;
import com.eoutlet.Eoutlet.pojo.LoginResponse;
import com.eoutlet.Eoutlet.pojo.RememberCardDetail;
import com.eoutlet.Eoutlet.pojo.RememberCardName;
import com.eoutlet.Eoutlet.pojo.WalletPaymentStatus;
import com.eoutlet.Eoutlet.utility.Util;
import com.github.dewinjm.monthyearpicker.MonthFormat;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WalletPaymentOptions#newInstance} factory method to
 * create an instance of this fragment.
 */
@RequiresApi(api = Build.VERSION_CODES.N)
public class WalletPaymentOptions extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    int yearSelected;
    int monthSelected;

    WebView paymentWebview;
    private CardView edtcardinfo;
    private FrameLayout addmoney;
    private TextView amounttext;

    private EditText cardname, cardnumber, dateandmonth, cvv;
    private String fillname, fillcardnumber, filldateandmonth, fillcvv;
    public ParentDialogsListener parentDialogListener;
    private String amount;
    private final int interval = 5000; // 1 Second

    private RecyclerView cardrecycler;
    private RememberCardAdapter cardAdapter;

    private List<RememberCardDetail> carddetail = new ArrayList<>();

    private boolean remembeMeflag = false;
    private ImageView remembermeselect;
    private int selectedcardposition;


    Handler handler = new Handler();
    private View view;

    private ImageView walletpaymentradio;
    private CardView walletpaymentcard;

    private int walletpaumentflag = 99;
    private CardView editcarddetail;

    private String savedcardcvv, tokenname;


    Calendar calendar = Calendar.getInstance();
    MonthYearPickerDialogFragment dialogFragment;

    public WalletPaymentOptions() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WalletPaymentOptions.
     */
    // TODO: Rename and change types and number of parameters
    public static WalletPaymentOptions newInstance(String param1, String param2) {
        WalletPaymentOptions fragment = new WalletPaymentOptions();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wallet_payment_options, container, false);

        MainActivity.fromcart = false;

        amount = getArguments().getString("addamount");
        initUi(view);


        getRememberCardName();


        yearSelected = calendar.get(Calendar.YEAR);
        monthSelected = calendar.get(Calendar.MONTH);

        return view;
    }


    public void initUi(View view) {
        paymentWebview = view.findViewById(R.id.paymentWebview);
        edtcardinfo = view.findViewById(R.id.edt_card_detail);
        addmoney = view.findViewById(R.id.addmoney);
        amounttext = view.findViewById(R.id.amounttext);

        cardname = view.findViewById(R.id.edtcardname);
        cardnumber = view.findViewById(R.id.edtcardnumber);
        dateandmonth = view.findViewById(R.id.monthandyear);
        cvv = view.findViewById(R.id.cvv);
        walletpaymentcard = view.findViewById(R.id.walletpaymentcard);
        walletpaymentradio = view.findViewById(R.id.walletpaymentradio);
        editcarddetail = view.findViewById(R.id.edt_card_detail);


        remembermeselect = view.findViewById(R.id.remembermeselect);

        remembermeselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!remembeMeflag) {
                    remembermeselect.setImageResource(R.drawable.checkboxselected);
                    remembeMeflag = true;
                } else {
                    remembermeselect.setImageResource(R.drawable.checkboxunselected);
                    remembeMeflag = false;
                }


            }
        });


        walletpaymentcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (walletpaumentflag != 1) {

                    walletpaymentradio.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                    walletpaumentflag = 1;
                    editcarddetail.setVisibility(View.VISIBLE);
                    initSavedCardRecycler();


                }


            }
        });


        amounttext.setText("SAR" + " " + amount + " " + "الاجمالي");


        dateandmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MonthFormat monthFormat = MonthFormat.LONG; //MonthFormat.LONG or MonthFormat.SHORT


                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                calendar.get(Calendar.YEAR);


                Log.d(TAG, "@ thisYear : " + ( calendar.get(Calendar.YEAR)));
                Log.d(TAG, "@ thisYear : " + ( calendar.get(Calendar.MONTH)));
                Log.d(TAG, "@ thisYear : " + ( calendar.get(Calendar.DAY_OF_MONTH)));

                calendar.set(calendar.get(Calendar.YEAR), ( calendar.get(Calendar.MONTH)), calendar.get(Calendar.DAY_OF_MONTH)); // Set minimum date to show in dialog
                long minDate = calendar.getTimeInMillis();

                calendar.clear();
                //MonthFormat.LONG or MonthFormat.SHORT
                Calendar calendarend = Calendar.getInstance(TimeZone.getDefault());

                calendar.set(calendarend.get(Calendar.YEAR)+10, ( calendarend.get(Calendar.MONTH)+10), calendarend.get(Calendar.DAY_OF_MONTH)+10); // Set maximum date to show in dialog
                long maxDate = calendar.getTimeInMillis(); // G


                dialogFragment = MonthYearPickerDialogFragment
                        .getInstance(monthSelected, yearSelected, minDate, maxDate, "", monthFormat);
                dialogFragment.show(getFragmentManager(), null);

                dialogFragment.setOnDateSetListener(new MonthYearPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int year, int monthOfYear) {
                        // do something
                        Log.e("asds", year + "/" + monthOfYear);
                        int monthofyear = monthOfYear + 1;


                        String actualmonth = String.valueOf(monthofyear);

                        if (actualmonth.toString().length() < 2) {

                            actualmonth = "0" + actualmonth;


                        }

                        String actualyear = String.valueOf(year).substring(String.valueOf(year).length() - 2);

                        String concatstring = actualyear + "/" + actualmonth;


                        dateandmonth.setText(concatstring);


                    }
                });


            }
        });





       /* paymentWebview.getSettings().setJavaScriptEnabled(true);

        CookieManager.getInstance().removeAllCookies(null);
        CookieManager.getInstance().flush();

        paymentWebview.getSettings().setJavaScriptEnabled(true);
        paymentWebview.getSettings().setUseWideViewPort(true);
        paymentWebview.getSettings().setLoadWithOverviewMode(true);
        paymentWebview.clearCache(true);
        paymentWebview.clearHistory();
        paymentWebview.clearFormData();
        paymentWebview.setWebViewClient(new MyBrowser());

        paymentWebview.loadUrl("https://staging.eoutlet.com/eoutletpayfort/index/index/");
*/


        addmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (walletpaumentflag == 1) {
                fillname = cardname.getText().toString();
                fillcardnumber = cardnumber.getText().toString();
                filldateandmonth = dateandmonth.getText().toString();
                fillcvv = cvv.getText().toString();

                boolean isAllConditionFulfilled = true;

                isAllConditionFulfilled = cardname.getText().toString().trim().length() > 0
                        && cardnumber.getText().toString().trim().length() > 0 &&
                        dateandmonth.getText().toString().trim().length() > 0 &&
                        cardnumber.getText().toString().trim().length() > 0
                        &&
                        cvv.getText().toString().trim().length() > 0;


               /* isAllConditionFulfilled = Util.checkTextViewValidation(cardnumber, "الرجاء إدخال اسم المستخدم")
                        && Util.checkTextViewValidation(cvv, "برجاء كتابة كلمة السر");
*/

                if (!isAllConditionFulfilled) {
                    Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();

                    return;
                }


                    addmoney();
                }
                else if(walletpaumentflag==2){


                    if(!savedcardcvv.isEmpty()) {
                        addmoneyforsavedcard();
                    }
                    else{

                        Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();

                    }
                }


                else {
                    Toast.makeText(getContext(), "برجاء اختيار طريقة الدفع للاستمرار", Toast.LENGTH_LONG).show();


                }
            }
        });


    }

    private class MyBrowser extends WebViewClient {


        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            //  parentDialogListener.showProgressDialog();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            return super.shouldOverrideUrlLoading(view, url);
/*
            view.loadUrl(url);



            return true;*/
        }

        @Override
        public void onPageFinished(WebView view, String url) {


        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getContext(), "Error" + description + errorCode, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Toast.makeText(getContext(),"dsfdsfds",Toast.LENGTH_SHORT).show();
        try {

            parentDialogListener = (ParentDialogsListener) activity;

        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }

    public void addmoney() {

        String remember_me = " ";
        if (remembeMeflag) {

            remember_me = "YES";

        } else {

            remember_me = "NO";

        }


        parentDialogListener.showProgressDialog();
        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);
        //Call<AddMoneyResponse> call = apiService.addmoney(amount, MySharedPreferenceClass.getMyUserId(getContext()), "YES", "4005550000000001", "Arpan", "2105", "123");

        Call<AddMoneyResponse> call = apiService.addmoney(amount, MySharedPreferenceClass.getMyUserId(getContext()), remember_me, fillcardnumber, fillname, filldateandmonth.replace("/", ""), fillcvv.trim());
        call.enqueue(new Callback<AddMoneyResponse>() {
            @Override
            public void onResponse(Call<AddMoneyResponse> call, Response<AddMoneyResponse> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    if (response.body().status.equals("20")) {


                        if (!response.body()._3dsUrl.isEmpty() && !response.body().orderId.equals(null)) {


                            /*Intent paymentintent = new Intent(getContext(), Threedpasswordactivity.class);
                            paymentintent.putExtra("url", response.body()._3dsUrl);
                            paymentintent.putExtra("order_id", response.body().orderId);

                            startActivity(paymentintent);*/


                            Fragment prFrag = new ThreedpasswordFragments();
                            Bundle bundle = new Bundle();
                            bundle.putString("order_id", response.body().orderId);
                            bundle.putString("url", response.body()._3dsUrl);
                            prFrag.setArguments(bundle);

                            getFragmentManager()
                                    .beginTransaction().addToBackStack(null)
                                    .add(WalletPaymentOptions.this.getId(), prFrag)
                                    .commit();


                        } else {


                        }


                    }


                } else {

                    //Toast.makeText(getContext(), "لم تقم بتسجيل الدخول بشكل صحيح أو أن حسابك معطل مؤقتاً.", Toast.LENGTH_SHORT).show();


                }
            }


            @Override
            public void onFailure(Call<AddMoneyResponse> call, Throwable t) {

                Log.e(TAG, t.toString());
                System.out.println("ErrorApi...." + t.toString());
                parentDialogListener.hideProgressDialog();

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void addmoneyforsavedcard() {

        String remember_me = " ";
        if (remembeMeflag) {

            remember_me = "YES";

        } else {

            remember_me = "NO";

        }


        parentDialogListener.showProgressDialog();
        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);
        //Call<AddMoneyResponse> call = apiService.addmoney(amount, MySharedPreferenceClass.getMyUserId(getContext()), "YES", "4005550000000001", "Arpan", "2105", "123");

        Call<AddMoneyResponse> call = apiService.addmoneyfromsavedcard(amount, tokenname, MySharedPreferenceClass.getMyUserId(getContext()), remember_me, " ", " ", " ", savedcardcvv);
        call.enqueue(new Callback<AddMoneyResponse>() {
            @Override
            public void onResponse(Call<AddMoneyResponse> call, Response<AddMoneyResponse> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    if (response.body().status.equals("20")) {


                        if (!response.body()._3dsUrl.isEmpty() && !response.body().orderId.equals(null)) {


                            /*Intent paymentintent = new Intent(getContext(), Threedpasswordactivity.class);
                            paymentintent.putExtra("url", response.body()._3dsUrl);
                            paymentintent.putExtra("order_id", response.body().orderId);

                            startActivity(paymentintent);*/


                            Fragment prFrag = new ThreedpasswordFragments();
                            Bundle bundle = new Bundle();
                            bundle.putString("order_id", response.body().orderId);
                            bundle.putString("url", response.body()._3dsUrl);
                            prFrag.setArguments(bundle);

                            getFragmentManager()
                                    .beginTransaction().addToBackStack(null)
                                    .add(WalletPaymentOptions.this.getId(), prFrag)
                                    .commit();


                        } else {


                        }


                    }


                } else {

                    //Toast.makeText(getContext(), "لم تقم بتسجيل الدخول بشكل صحيح أو أن حسابك معطل مؤقتاً.", Toast.LENGTH_SHORT).show();


                }
            }


            @Override
            public void onFailure(Call<AddMoneyResponse> call, Throwable t) {

                Log.e(TAG, t.toString());
                System.out.println("ErrorApi...." + t.toString());
                parentDialogListener.hideProgressDialog();

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void clearFragmentBackStack() {
        FragmentManager fm = getFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }


    }

    public void getRememberCardName() {


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();


        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<RememberCardName> call = apiService.getremembercards(query);
        call.enqueue(new Callback<RememberCardName>() {
            @Override
            public void onResponse(Call<RememberCardName> call, Response<RememberCardName> response) {


                if (response.body() != null) {

                    //hideProgressDialog();

                    if (response.body().status.equals("success")) {


                        carddetail = response.body().data;

                        initSavedCardRecycler();


                    }


                }

            }


            @Override
            public void onFailure(Call<RememberCardName> call, Throwable t) {

                //progressDialog.hide();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void initSavedCardRecycler() {
        cardrecycler = view.findViewById(R.id.cardrecycler);

        cardrecycler.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));


        cardrecycler.setItemAnimator(new DefaultItemAnimator());
        // use a linear layout manager


        // specify an adapter (see also next example)
        cardAdapter = new RememberCardAdapter(getContext(), carddetail, selectedcardposition, new ViewListener3() {
            @Override
            public void onClick(int position, View view, String cvv) {
                int id = view.getId();

                savedcardcvv = cvv;
                tokenname = carddetail.get(position).tokenName;


                switch (id) {

                    case R.id.remembercardclick://button for message

                          walletpaumentflag =2;

                          editcarddetail.setVisibility(View.GONE);

                    walletpaymentradio.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));

                        break;
                }
            }
        }


        );
        cardrecycler.setAdapter(cardAdapter);
        cardrecycler.setNestedScrollingEnabled(false);
    }


}