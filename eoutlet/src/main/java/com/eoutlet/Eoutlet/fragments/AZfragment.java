package com.eoutlet.Eoutlet.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.adpters.AZAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.listener.ViewListener4;
import com.eoutlet.Eoutlet.pojo.Alphanumeric;
import com.eoutlet.Eoutlet.pojo.BrandName;
import com.eoutlet.Eoutlet.pojo.BrandNameDetail;
import com.eoutlet.Eoutlet.utility.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class AZfragment extends Fragment {
    private RecyclerView az_recycler;
    private AZAdapter azAdapter;
    public static TextView toolbar_bedgetextbrand, tooltext;
    private ExecuteFragment execute;
    private Context context;
    private Toolbar toolbar1;
    private boolean _hasLoadedOnce = false;
    private View view, toolbarbeg;
    private int totalrowsize = 0;
    private ArrayList<String> alphabetic = new ArrayList<>();

    private HashMap<String, List<BrandNameDetail>> allnames = new HashMap<>();

    private ImageView searchImage, backarrow;
    public ParentDialogsListener parentDialogListener;

    // TODO: Rename and change types and number of parameters
    public static AZfragment newInstance(String param1, String param2) {
        AZfragment fragment = new AZfragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_a_zfragment, container, false);
        context = getActivity();
        execute = (MainActivity) getActivity();
        System.out.println("context.... " + context);
        az_recycler = view.findViewById(R.id.az_recycler);
        toolbar1 = view.findViewById(R.id.toolbar);

        //toolbarbeg = toolbar.findViewById(R.id.toolbarbag);
        searchImage = toolbar1.findViewById(R.id.serachbar);
        // toolbar_bedgetextbrand = toolbar.findViewById(R.id.toolbar_cart_badge_text);
        //tooltext = toolbar.findViewById(R.id.tooltext);
/*
        if(MySharedPreferenceClass.getBedgeCount(getContext())>0) {
            toolbar_bedgetextbrand.setVisibility(View.VISIBLE);
            toolbar_bedgetextbrand.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));

        }*/

/*
        if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {

            tooltext.setText(MySharedPreferenceClass.getMyFirstNamePref(getContext()) *//*+ " " + "مرحبا"*//*);


        }*/


        toolbar1 = view.findViewById(R.id.toolbar);
        searchImage = toolbar1.findViewById(R.id.serachbar);
        backarrow = toolbar1.findViewById((R.id.backarrow));

        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment prFrag = new SearchResultFragment();
                Bundle databund = new Bundle();


                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .add(R.id.profileContainer, prFrag)
                        .commit();


            }
        });

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });




       /* toolbarbeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execute.ExecutFragmentListener(0);
            }
        });*/


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            parentDialogListener = (ParentDialogsListener) activity;

        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void getBrandName() {
        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Call<BrandName> call = apiService.getHomeBrandDetail(Constants.HOST_LINK + Constants.BASE_LINK + "brandcatapi.php");
        call.enqueue(new Callback<BrandName>() {
            @Override
            public void onResponse(Call<BrandName> call, Response<BrandName> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    System.out.println("responsebrand...." + response.body().toString());
                    List<Alphanumeric> alphanumericlist = new ArrayList<>();

                    alphanumericlist = response.body().arr;


                    if (response.body().arr.size() > 1) {


                        for (int i = 0; i < response.body().arr.size(); i++) {


                            alphabetic.add(response.body().arr.get(i).alphabetic);


                        }


                        if (alphabetic.size() == alphanumericlist.size()) {


                            for (int i = 0; i < alphabetic.size(); i++) {


                                allnames.put(alphabetic.get(i), alphanumericlist.get(i).data);


                            }

                        }


                        for (int i = 0; i < alphabetic.size(); i++) {


                            for (int j = 0; j < allnames.get(alphabetic.get(i)).size(); j++) {

                                totalrowsize++;


                            }


                        }

                        totalrowsize = totalrowsize + alphabetic.size();

                        System.out.println(totalrowsize + "dsfsdfdsfdsfdsfsdfsdfsdfs");


                        initRecycler6(response.body().arr, alphabetic);


                    } else {
                        parentDialogListener.hideProgressDialog();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<BrandName> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void initRecycler6(final List<Alphanumeric> cat6, ArrayList<String> alphabetic) {
        // GridLayoutManager manager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        // az_recycler.setLayoutManager(new GridLayoutManager(view.getContext(), 2));

        az_recycler.setHasFixedSize(true);
        az_recycler.setNestedScrollingEnabled(false);
        az_recycler.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));


        az_recycler.setItemAnimator(new DefaultItemAnimator());
        // use a linear layout manager


        // specify an adapter (see also next example)
        azAdapter = new AZAdapter(getContext(), cat6, alphabetic, allnames, totalrowsize, new ViewListener4() {
            @Override
            public void onClick(int position, View view, String name, String ids) {
                int id = view.getId();


                switch (id) {

                    case R.id.parentLayout://button for message
                        if (!ids.equals("999")) {
                            Fragment prFrag = new ProductList();
                            Bundle databund = new Bundle();
                            databund.putInt("productId", Integer.parseInt(ids));
                            databund.putString("name", name);
                            databund.putString("fromwhere", "frombrand");
                            prFrag.setArguments(databund);


                            getFragmentManager()
                                    .beginTransaction().addToBackStack(null)
                                    .replace(R.id.brandcontainer, prFrag)
                                    .commit();
                        }

                        break;
                }
            }
        }


        );
        az_recycler.setAdapter(azAdapter);
        az_recycler.setNestedScrollingEnabled(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        _hasLoadedOnce = false;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);

        if (visible && isResumed()) {
          /*  if(MySharedPreferenceClass.getBedgeCount(getContext())>0) {
                toolbar_bedgetextbrand.setVisibility(View.VISIBLE);
                toolbar_bedgetextbrand.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));
            }
            else {
                toolbar_bedgetextbrand.setVisibility(View.GONE);
            }*/

            searchImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Fragment prFrag = new SearchResultFragment();
                    Bundle databund = new Bundle();


                    getFragmentManager()
                            .beginTransaction().addToBackStack(null)
                            .replace(R.id.brandcontainer, prFrag)
                            .commit();


                }
            });


        }

        if (visible && isResumed() && !_hasLoadedOnce) {


            getBrandName();
            _hasLoadedOnce = true;

        }
    }
}

