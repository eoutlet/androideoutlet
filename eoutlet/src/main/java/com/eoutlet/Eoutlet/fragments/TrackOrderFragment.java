package com.eoutlet.Eoutlet.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.adpters.OrderListAdapter;
import com.eoutlet.Eoutlet.adpters.TrackOrderAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.OrderListItem;
import com.eoutlet.Eoutlet.pojo.OrderListResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TrackOrderFragment extends Fragment {
    private RecyclerView recyclerView;
    private Context context;
    private TrackOrderFragment trackOrderFragment;

    private List<OrderListItem> customDataList = new ArrayList<>();
    private  View rawView;


    public ParentDialogsListener parentDialogListener;
    private OnFragmentInteractionListener mListener;
    private ImageView backButton,serachbar;
    private Toolbar toolbar;

    public TrackOrderFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View rawView = inflater.inflate(R.layout.fragment_track_order, container, false);
        recyclerView=rawView.findViewById(R.id.recyclertrack);
        toolbar = rawView.findViewById(R.id.toolbar);
        serachbar = toolbar.findViewById(R.id.serachbar);
        backButton = toolbar.findViewById((R.id.backButton));
        context=getActivity();
        trackOrderFragment= TrackOrderFragment.this;





        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.hasFixedSize();
        // recyclerView.setAdapter(new TrackOrderAdapter(context,trackOrderFragment));
        callOrderListApi();

        serachbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment prFrag = new SearchResultFragment();
                Bundle databund = new Bundle();


                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .add(R.id.profileContainer, prFrag)
                        .commit();



            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        return rawView;

    }






    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        try {

            parentDialogListener = (ParentDialogsListener) context;

        } catch (ClassCastException e) {

            throw new ClassCastException(context.toString()
                    + " Activity's Parent should be Parent Activity");
        }
    }

    private void callOrderListApi() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Map<String, String> map1 = new HashMap<>();

        map1.put("customer_id", MySharedPreferenceClass.getMyUserId(context));


        Call<OrderListResponse> call = apiService.orderTrackingList(map1);
        call.enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(Call<OrderListResponse> call, Response<OrderListResponse> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().getMsg().equalsIgnoreCase("success")) {

                        List<OrderListItem> data = response.body().getData();
                        if (data.size() > 0 || data != null) {
                            customDataList.addAll(data);
                        }
//                            customDataList.addAll(response.body().getData());


                        recyclerView.setAdapter(new TrackOrderAdapter(context, customDataList, trackOrderFragment));

//                            orderListAdapter.notifyDataSetChanged();


                    } else {
                        parentDialogListener.hideProgressDialog();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<OrderListResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e("Failure..", t.getMessage());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();


            }
        });


    }

}
