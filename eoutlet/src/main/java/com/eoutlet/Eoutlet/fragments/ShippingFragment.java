package com.eoutlet.Eoutlet.fragments;

import android.annotation.TargetApi;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.eoutlet.Eoutlet.R;


public class ShippingFragment extends Fragment {
    private WebView ShippingWebview;
    private String value =" ";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shipping, container, false);
         value = getArguments().getString("position");
        initUi(view);
        return view;
    }
    public void initUi(View view) {
        ShippingWebview  = view.findViewById(R.id.ShippingWebview);


        ShippingWebview.setWebViewClient(new MyBrowser());

        ShippingWebview.getSettings().setJavaScriptEnabled(true);


        ShippingWebview.loadData(value, "text/html", "UTF-8");



    }


    protected class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {


        }
        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }


        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getContext(), "Error" + description + errorCode, Toast.LENGTH_SHORT).show();
        }


    }

}
