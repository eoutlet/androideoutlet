package com.eoutlet.Eoutlet.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.adpters.NewProfileAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.AddressList;
import com.eoutlet.Eoutlet.pojo.GuestUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class NewProfileFragment extends Fragment {
    private ImageView searchImage;
    RecyclerView profilerecycler;
    NewProfileAdapter profileadapter;
    ParentDialogsListener parentDialogListener;
    private boolean _hasLoadedOnce = false;
    private View v;
    private int menuicon[] = {R.drawable.ic_user,R.drawable.ic_wallet_yellow,R.drawable.ic_purchase,R.drawable.ic_ordertrack,R.drawable.ic_address,R.drawable.ic_policy, R.drawable.ic_callus, R.drawable.ic_logout};
    private List<String> profileString;
    private Toolbar toolbar;
    private List<String> street = new ArrayList<>();
    private List<String> city = new ArrayList<>();
    private List<String> country = new ArrayList<>();
    private List<String> phone = new ArrayList<>();
    private List<String> address_id = new ArrayList<>();
    private List<String> countryId = new ArrayList<>();
    private List<String> name = new ArrayList<>();
    private List<String> addressfirstname = new ArrayList<>();
    private List<String> addresslastname = new ArrayList<>();

    private Toolbar toolbar1;
    private OnFragmentInteractionListener mListener;

    public NewProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {

            parentDialogListener = (ParentDialogsListener) context;

        } catch (ClassCastException e) {

            throw new ClassCastException(context.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }
    // TODO: Rename and change types and number of parameters
    public static NewProfileFragment newInstance(String param1, String param2) {
        NewProfileFragment fragment = new NewProfileFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_new_profile, container, false);



        profilerecycler = v.findViewById(R.id.profileRecycler);
        profileString = new ArrayList<>();
        profileString.add("حسابي / التفاصيل");
        profileString.add("رصيد حسابي");
        profileString.add("طلبات الشراء");
        profileString.add("تعقب الطلبات");
        profileString.add("عناوين الشحن");
        profileString.add("سياسات التطبيق");

        profileString.add("اتصل بنا");
        profileString.add("تسجيل الخروج");


        initProfilerecycler();
        toolbar1 = v.findViewById(R.id.toolbar);

        searchImage = toolbar1.findViewById(R.id.serachbar);

        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment prFrag = new SearchResultFragment();
                Bundle databund = new Bundle();


                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.profileContainer, prFrag)
                        .commit();



            }
        });




        if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {


        } else {


            Handler uiHandler = new Handler();
            uiHandler.post(new Runnable()
            {
                @Override
                public void run() {
                    Fragment prFrag = new LoginNewFragment();
                    Bundle databund = new Bundle();
                    databund.putString("flag", "fromProfile");
                    prFrag.setArguments(databund);
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.profileContainer, prFrag)/*.commit();*/
                            .commitAllowingStateLoss();
                }});

        }


        return v;


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {

        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @SuppressLint("WrongConstant")
    public void initProfilerecycler() {

        // use a linear layout manager

        profilerecycler.setLayoutManager(new LinearLayoutManager(v.getContext(), LinearLayoutManager.VERTICAL, false));

        // specify an adapter (see also next example)
        profileadapter = new NewProfileAdapter(getContext(),
                profileString, menuicon,
                new ViewListener() {
                    @Override
                    public void onClick(int position, View view) {
                        int id = view.getId();


                        switch (id) {



                            case R.id.profileItemClick://button for message
                               /* if (position == 9) {
                                    openlogoutdialog();

                                }*/

                             /*   if (position == 7) {

                                    Fragment prFrag = new HelpSupportFragment();
                                    //  ((SaveAddressFragment) prFrag).passData(getContext());
                                    getFragmentManager()
                                            .beginTransaction().addToBackStack(null)
                                            .replace(R.id.profileContainer, prFrag)
                                            .commit();


                                }*/
                                if (position == 0) {

                                    Fragment prFrag = new NewUpdateInfo();
                                    //  ((SaveAddressFragment) prFrag).passData(getContext());
                                    getFragmentManager()
                                            .beginTransaction().addToBackStack(null)
                                            .replace(R.id.profileContainer, prFrag)
                                            .commit();

                                }
                                else if (position == 1) {

                                    Fragment newWalletFragment = new NewWalletFragment();
                                    //  ((SaveAddressFragment) prFrag).passData(getContext());
                                    getFragmentManager()
                                            .beginTransaction().addToBackStack(null)
                                            .replace(R.id.profileContainer, newWalletFragment)
                                            .commit();


                                }
                                else if (position == 2) {

                                    Fragment orderListFragment = new NewOrderListFragment();
                                    //  ((SaveAddressFragment) prFrag).passData(getContext());
                                    getFragmentManager()
                                            .beginTransaction().addToBackStack(null)
                                            .replace(R.id.profileContainer, orderListFragment)
                                            .commit();
                                }
                                else if (position == 3) {

                                    Fragment prFrag = new TrackOrderFragment();

                                    getFragmentManager()
                                            .beginTransaction().addToBackStack(null)
                                            .replace(R.id.profileContainer, prFrag)
                                            .commit();
                                }

                                else if (position == 4){



                                    Fragment prFrag = new NewAddressFragment();
                                    Bundle databund = new Bundle();
                                    databund.putString("flag", "mainprofile");
                                    prFrag.setArguments(databund);
                                    getFragmentManager()
                                            .beginTransaction().addToBackStack(null)
                                            .replace(R.id.profileContainer, prFrag)
                                            .commit();

                                }
                                else if(position==5){
                                    Fragment prFrag = new HelpSupportFragment();
                                    //  ((SaveAddressFragment) prFrag).passData(getContext());
                                    getFragmentManager()
                                            .beginTransaction().addToBackStack(null)
                                            .replace(R.id.profileContainer, prFrag)
                                            .commit();
                                }
                                else if (position == 6){




                                    Fragment contactUsFragment = new ContactUsFragment();
                                    //  ((SaveAddressFragment) prFrag).passData(getContext());
                                    getFragmentManager()
                                            .beginTransaction().addToBackStack(null)
                                            .replace(R.id.profileContainer, contactUsFragment)
                                            .commit();

                                }



                                else if (position == 7) {

                                    openlogoutdialog();



                                    // Fragment prFrag = new SaveAddressFragment();


                                } else if (position == 8) {



                                }







                                break;
                        }
                    }
                }


        );
        profilerecycler.setAdapter(profileadapter);
        profilerecycler.setNestedScrollingEnabled(false);


    }

    public void openlogoutdialog() {


        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage("هل أنت متأكد من تسجيل الخروج؟");


        alertDialogBuilder.setPositiveButton("نعم",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getguesttoken();

                    }
                });

        alertDialogBuilder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {
                //alertDialog.dismiss();

            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void getguesttoken() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        // Map<String, String> query = new HashMap<>();
        //query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext());

        Call<GuestUser> call = apiService.getGuestToken();
        call.enqueue(new Callback<GuestUser>() {
            @Override
            public void onResponse(Call<GuestUser> call, Response<GuestUser> response) {


                if (response.body().msg.equals("success")) {

                    parentDialogListener.hideProgressDialog();
                    MainActivity.notificationBadge.setVisibility(View.GONE);
                    MySharedPreferenceClass.setMaskkey(getContext(), response.body().maskKey);
                    MySharedPreferenceClass.setCartId(getContext(), response.body().cartId);
                    MySharedPreferenceClass.setMyUserId(getContext(), null);
                    MySharedPreferenceClass.setMyUserName(getContext(), null);
                    MySharedPreferenceClass.setMyFirstNamePref(getContext(), null);
                    MySharedPreferenceClass.setMyLastNamePref(getContext(), null);
                    MySharedPreferenceClass.setBedgeCount(getContext(), 0);
                    MySharedPreferenceClass.setEmail(getContext(), null);


                    MySharedPreferenceClass.setAddressname(getContext(), null);
                    MySharedPreferenceClass.setCityname(getContext(), null);
                    MySharedPreferenceClass.setStreetName(getContext(), null);
                    MySharedPreferenceClass.setCountryname(getContext(), null);

                    MySharedPreferenceClass.setAddressphone(getContext(), null);
                    MySharedPreferenceClass.setCountryId(getContext(), null);
                    MySharedPreferenceClass.setfirstAdressname(getContext(),null);
                    MySharedPreferenceClass.setlastAdressname(getContext(),null);
                    MySharedPreferenceClass.setSelecteAddressdId(getContext(), "0");
                    MySharedPreferenceClass.setSelecteTokenName(getContext()," ");

                    Fragment prFrag = new LoginNewFragment();
                    Bundle databund = new Bundle();
                    databund.putString("flag", "fromlogout");
                    prFrag.setArguments(databund);
                    ((LoginNewFragment) prFrag).passData(getContext());
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.profileContainer, prFrag)
                            .commit();

                } else {
                    parentDialogListener. hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<GuestUser> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                //Toast.makeText(getContext(), "No Items Found", Toast.LENGTH_LONG).show();
            }
        });


    }



    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed() && _hasLoadedOnce) {
            if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {
                Fragment prFrag = new NewProfileFragment();
                Bundle databund = new Bundle();
                databund.putString("flag", "fromProfile");
                prFrag.setArguments(databund);
                //  ((SaveAddressFragment) prFrag).passData(getContext());
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.profileContainer, prFrag)
                        .commit();

            } else {

                Fragment prFrag = new LoginNewFragment();
                Bundle databund = new Bundle();
                databund.putString("flag", "fromProfile");
                prFrag.setArguments(databund);
                //  ((SaveAddressFragment) prFrag).passData(getContext());
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.profileContainer, prFrag)
                        .commit();


                //Toast.makeText(getContext(), "You must be logged in first", Toast.LENGTH_SHORT).show();
            }
            // System.out.println("Visible User Method Called.....");
        }
        _hasLoadedOnce = true;
    }
    public void clearFragmentBackStack() {
        FragmentManager fm = getFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount() ; i++) {
            fm.popBackStack();
        }
    }


    public void getAddressDetails() {

        street.clear();
        name.clear();
        city.clear();
        country.clear();
        phone.clear();
        address_id.clear();
        countryId.clear();

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map1 = new HashMap<>();

        map1.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<AddressList> call = apiService.getAddressInfo(map1);
        call.enqueue(new Callback<AddressList>() {
            @Override
            public void onResponse(Call<AddressList> call, Response<AddressList> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().msg.equals("success")) {
                        if(response.body().data.size()>0){


                            for (int i = 0; i < response.body().data.size(); i++) {

                                name.add(response.body().data.get(i).firstname + " " + response.body().data.get(i).lastname);
                                addressfirstname.add(response.body().data.get(i).firstname);
                                addresslastname.add(response.body().data.get(i).lastname);
                                street.add(response.body().data.get(i).street);
                                city.add(response.body().data.get(i).city);
                                country.add(response.body().data.get(i).country);
                                phone.add(response.body().data.get(i).telephone);
                                address_id.add(response.body().data.get(i).addressId);
                                countryId.add(response.body().data.get(i).country_id);


                            }

                            Collections.reverse(name);
                            Collections.reverse(street);
                            Collections.reverse(city);
                            Collections.reverse(country);
                            Collections.reverse(phone);
                            Collections.reverse(address_id);
                            Collections.reverse(countryId);



                            MySharedPreferenceClass.setfirstAdressname(getContext(),addressfirstname.get(0));
                            MySharedPreferenceClass.setlastAdressname(getContext(),addresslastname.get(0));
                            MySharedPreferenceClass.setAddressname(getContext(), name.get(0));
                            MySharedPreferenceClass.setCityname(getContext(), city.get(0));
                            MySharedPreferenceClass.setStreetName(getContext(), street.get(0));
                            MySharedPreferenceClass.setCountryname(getContext(), country.get(0));

                            MySharedPreferenceClass.setAddressphone(getContext(), phone.get(0));
                            MySharedPreferenceClass.setCountryId(getContext(), countryId.get(0));


                            MySharedPreferenceClass.setSelecteAddressdId(getContext(), "0");






                        } else {
                            parentDialogListener.hideProgressDialog();


                        }}
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<AddressList> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void clearStack() {
        //Here we are clearing back stack fragment entries
        int backStackEntry = getFragmentManager().getBackStackEntryCount();
        if (backStackEntry > 0) {
            for (int i = 0; i < backStackEntry; i++) {
                getFragmentManager().popBackStackImmediate();
            }
        }

    }


    @Override
    public  void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }


}
