package com.eoutlet.Eoutlet.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.adpters.FilterCatagoryListAdapter;
import com.eoutlet.Eoutlet.adpters.FilterDetailAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;

import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.pojo.FilterDetail;
import com.eoutlet.Eoutlet.pojo.Filtermainlist;
import com.eoutlet.Eoutlet.pojo.Size;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class FilterFragmentNew extends DialogFragment implements Cloneable {
    View view;
    private FilterCatagoryListAdapter mAdapter;
    private FilterDetailAdapter mAdapter2;
    private LinearLayout filterclick;
    private int value;
    private String prevsize, previtemtype, prevmanufec;

    StringBuilder manufecturer = new StringBuilder();
    StringBuilder item_type = new StringBuilder();
    StringBuilder size = new StringBuilder();


    static boolean applied = false;

    Dialog dialog;

    private RecyclerView catagorylistRecycler;
    private RecyclerView catagorydetailRecycler;
    ProgressDialog progressDialog;
    public ParentDialogsListener parentDialogListener;
    LinearLayout apply;
    private List<String> listname = new ArrayList<>();

    private List<List<String>> mainCatList2 = new ArrayList<>();
    private List<List<String>> mainCatListvalue = new ArrayList<>();

    private List<Integer> countlist2 = new ArrayList<>();

    private HashMap<String, Integer> countlistmain = new HashMap<>();


    static private List<List<String>> mainCatListlastselected2 = new ArrayList<>();
    static private List<List<String>> mainCatListlastselected = new ArrayList<>();
    static private List<Integer> countlistlastselected = new ArrayList<>();


    private List<List<String>> mainCatListimported = new ArrayList<>();
    //private List<Integer> countlistimported = new ArrayList<>();
    private HashMap<String, Integer> countlistimported = new HashMap<>();
    private List<Filtermainlist> fullList = new ArrayList<>();


    private List<String> itemcode = new ArrayList<>();

    private HashMap<String, List<String>> selectedvalue = new HashMap();


    private OnFragmentInteractionListener mListener;


    private int listlastselectedposiion = 0;

    public FilterFragmentNew() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FilterFragmentNew newInstance(String param1, String param2) {
        FilterFragmentNew fragment = new FilterFragmentNew();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);

        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        view = inflater.inflate(R.layout.fragment_filter, null);
        builder.setView(view);
        initViews();
        //applied = false;
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.WHITE));


        return dialog;


        // return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            parentDialogListener = (ParentDialogsListener) activity;

        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }


    public void initViews() {


        value = getArguments().getInt("catId");

        size.append(getArguments().getString("previoussize"));

        item_type.append(getArguments().getString("previousitemtype"));

        manufecturer.append(getArguments().getString("previousmanufecturer"));


        Log.e("prenman-->>", manufecturer.toString());
        Log.e("previtemtype", item_type.toString());
        Log.e("previsize", size.toString());

        //Toast.makeText(getActivity(),prevmanufec,Toast.LENGTH_SHORT).show();


        catagorylistRecycler = view.findViewById(R.id.catagory_list);
        catagorydetailRecycler = view.findViewById(R.id.catagory_list_detail);
        filterclick = view.findViewById(R.id.clear);
        apply = view.findViewById(R.id.apply);

        mainCatList2.clear();
        countlist2.clear();
        fullList.clear();


        mainCatListimported = (List<List<String>>) getArguments().getSerializable("selctecatagory");
        //countlistimported = (List<Integer>) getArguments().getSerializable("countlist");

        countlistimported = (HashMap<String, Integer>) getArguments().getSerializable("countlist");

        selectedvalue = (HashMap<String, List<String>>) getArguments().getSerializable("prevselectedvalue");


        for (int i = 0; i < mainCatListimported.size(); i++) {
            List<String> l2 = new ArrayList<>(mainCatListimported.get(i));
            mainCatList2.add(l2);
        }

        //countlist2 = new ArrayList<>(countlistimported);

        countlistmain = new HashMap<>(countlistimported);


        getCatogary(manufecturer.toString(), item_type.toString(), size.toString());


        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainCatList2.size() > 0 && fullList.size() > 0) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    applied = true;


                    mainCatListlastselected.clear();

                    for (int i = 0; i < mainCatList2.size(); i++) {
                        List<String> l2 = new ArrayList<>(mainCatList2.get(i));
                        mainCatListlastselected.add(l2);
                    }


                    bundle.putString("manufecturer", manufecturer.toString());
                    bundle.putString("size", size.toString());
                    bundle.putString("itemtype", item_type.toString());
                    bundle.putSerializable("selctecatagory", (Serializable) new ArrayList<>(mainCatListlastselected));
                    bundle.putSerializable("maincatagory", (Serializable) new ArrayList<>(fullList));
                    bundle.putSerializable("countlist", (Serializable) new HashMap<String, Integer>(countlistmain));
                    bundle.putSerializable("selectedvalue", (Serializable) new HashMap<>(selectedvalue));

                    intent.putExtra("BUNDLE", bundle);


                    getTargetFragment().onActivityResult(101, 100, intent);






                    getDialog().cancel();
                } else {
                    Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة ", Toast.LENGTH_SHORT).show();


                }


            }
        });


        filterclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                clearall();


            }
        });


    }

    public void  getCatogary(final String manufacturer, final String item_type, final String size) {

        parentDialogListener.showProgressDialog();

        listname.clear();
        countlist2.clear();


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();


        query.put("id", String.valueOf(value));
        if (!manufacturer.equals("null") && !manufacturer.equals(" ")) {
            query.put("manufacturer", manufacturer);
        }
        if (!item_type.equals("null") && !item_type.equals(" ")) {
            query.put("item_type", item_type);
        }
        if (!size.equals("null") && !size.equals(" ")) {
            query.put("size", size);
        }
        Call<FilterDetail> call = apiService.getFilterCatagoryList(query);
        call.enqueue(new Callback<FilterDetail>() {
            @Override
            public void onResponse(Call<FilterDetail> call, Response<FilterDetail> response) {


                if (response.body() != null && response.body().data.size() > 0/*add new*/) {
                    parentDialogListener.hideProgressDialog();


                    fullList = response.body().data;

                    for (int i = 0; i < fullList.size(); i++) {
                        itemcode.add(fullList.get(i).code);

                        List<String> selectsublist = new ArrayList<>();

                        if (!selectedvalue.containsKey(itemcode.get(i))) {

                            selectedvalue.put(itemcode.get(i), selectsublist);
                        }


                    }

                    for (int i = 0; i < response.body().data.size(); i++) {

                        listname.add(response.body().data.get(i).name);


                        List<String> sublist = new ArrayList<>();
                        mainCatList2.add(sublist);

                        List<String> sublistvalue = new ArrayList<>();

                        mainCatListvalue.add(sublistvalue);

                        //countlist2.add(0);

                        if (!countlistmain.containsKey(itemcode.get(i))) {

                            countlistmain.put(itemcode.get(i), 0);
                        }


                    }

                    for (int i = 0; i < fullList.size(); i++) {

                        countlist2.add(countlistmain.get(fullList.get(i).code));


                    }
                } else {
                    parentDialogListener.hideProgressDialog();
                    Toast.makeText(getContext(), "No Record Found", Toast.LENGTH_SHORT).show();
                    //progressDialog.hide();


                }
                //  if (mainCatList2.size()== 0) {
                initCatagoryListRecycler();
                // }
                //else {

                //mAdapter.notifyDataSetChanged();
                // }


                List<String> catagoryselctionlist = new ArrayList<>();
                for (int j = 0; j < fullList.size(); j++) {
                    for (int i = 0; i < fullList.get(j).data.size(); i++) {

                        mainCatList2.get(j).add("false");

                        mainCatListvalue.get(j).add(fullList.get(j).data.get(i).value);
                    }
                }

                if (fullList.size() > 0) {
                    for (int i = 0; i < fullList.get(0).data.size(); i++) {

                        catagoryselctionlist.add(fullList.get(0).data.get(i).display);

                    }

                    initCatagoryDetailAdapter(catagoryselctionlist, mainCatList2.get(0), 0);
                }


            }


            @Override
            public void onFailure(Call<FilterDetail> call, Throwable t) {
                // parentDialogListener.hideProgressDialog();
                // progressDialog.hide();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    @SuppressLint("WrongConstant")
    public void initCatagoryListRecycler() {


        catagorylistRecycler.setHasFixedSize(true);

        // use a linear layout manager

        catagorylistRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        // specify an adapter (see also next example)
        mAdapter = new FilterCatagoryListAdapter(view.getContext(), listname, countlist2, listlastselectedposiion, new ViewListener() {
            @Override
            public void onClick(int position, View view) {
                int id = view.getId();


                switch (id) {

                    case R.id.maincatagoryclick://button for message


                        List<String> catagoryselctionlist = new ArrayList<>();
                        listlastselectedposiion = position;
                        Log.e("onclickselected----->>>", listlastselectedposiion + "");





                        for (int i = 0; i < fullList.get(position).data.size(); i++) {

                            catagoryselctionlist.add(fullList.get(position).data.get(i).display);

                        }

                        initCatagoryDetailAdapter(catagoryselctionlist, mainCatList2.get(position), position);
                        Log.e("Size is", catagoryselctionlist.size() + " ");
                        Log.e("Size is", mainCatList2.get(position).size() + " ");

                        break;
                }
            }
        });
        catagorylistRecycler.setAdapter(mAdapter);


    }


    @SuppressLint("WrongConstant")
    public void initCatagoryDetailAdapter(List<String> name, final List<String> selectionflag, final int mainlistposition) {

        catagorydetailRecycler.setHasFixedSize(true);
        //for(int i = 0;i<itemcode.size();i++) {
        //   selectedvalue.get(itemcode.get(i)).clear();
        //}
        // use a linear layout manager

        catagorydetailRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        // specify an adapter (see also next example)
        mAdapter2 = new FilterDetailAdapter(view.getContext(), name, selectionflag, new ViewListener() {
            @Override
            public void onClick(int position, View view) {
                int id = view.getId();


                switch (id) {

                    case R.id.filtercatagoryclick://button for message
                        boolean valueflag = false;


                        mainCatListlastselected2.clear();
                        for (int i = 0; i < mainCatList2.size(); i++) {
                            List<String> l1 = new ArrayList<>(mainCatList2.get(i));
                            mainCatListlastselected2.add(l1);
                        }


                        // getCatogary(mainCatListlastselected.get(position).get(1),mainCatListlastselected.get(0),mainCatListlastselected.get(2));

                        countlistlastselected = new ArrayList<>(countlist2);
                        // for(int j = 0;j<fullList.get(mainlistposition).data.size();j++) {


                        // if (fullList.get(mainlistposition).data.get(j).value.equals(mainCatListvalue.get(mainlistposition).get(j)))  {


                        //   valueflag = true;

                        //   break;


                        //  }
                        //}


                        if (mainCatList2.get(listlastselectedposiion).get(position).equals("false")) {

                            mainCatList2.get(listlastselectedposiion).set(position, "true");

                            selectedvalue.put(itemcode.get(listlastselectedposiion), selectedvalue.get(itemcode.get(listlastselectedposiion))).add(mainCatListvalue.get(listlastselectedposiion).get(position));

                        } else /*if(valueflag) */ {

                            mainCatList2.get(listlastselectedposiion).set(position, "false");


                            selectedvalue.put(itemcode.get(listlastselectedposiion), selectedvalue.get(itemcode.get(listlastselectedposiion))).remove(mainCatListvalue.get(listlastselectedposiion).get(position));


                        }


                        if (!itemcode.contains("item_type")) {
                            item_type = new StringBuilder();


                        }
                        if (!itemcode.contains("manufacturer")) {
                            manufecturer = new StringBuilder();

                        }

                        if (!itemcode.contains("size")) {
                            size = new StringBuilder();

                        }


                        int mainvalue = 0;
                        for (int i = 0; i < mainCatList2.get(listlastselectedposiion).size(); i++) {


                            if (listname.get(listlastselectedposiion).equals("نوع المنتج")) {


                                if (i == 0) {
                                    item_type = new StringBuilder();
                                    if (mainCatList2.get(listlastselectedposiion).get(i).equals("true")) {
                                        item_type = item_type.append(fullList.get(listlastselectedposiion).data.get(i).value);
                                        mainvalue++;
                                    }
                                } else {
                                    if (mainCatList2.get(listlastselectedposiion).get(i).equals("true")) {

                                        item_type = item_type.append("," + fullList.get(listlastselectedposiion).data.get(i).value);

                                        mainvalue++;
                                    }
                                }


                            } else if (listname.get(listlastselectedposiion).equals("الماركة")) {

                                if (i == 0) {
                                    manufecturer = new StringBuilder();
                                    if (mainCatList2.get(listlastselectedposiion).get(i).equals("true")) {
                                        mainvalue++;
                                        manufecturer = manufecturer.append(fullList.get(listlastselectedposiion).data.get(i).value);
                                    }
                                } else {
                                    if (mainCatList2.get(listlastselectedposiion).get(i).equals("true")) {


                                        manufecturer = manufecturer.append("," + fullList.get(listlastselectedposiion).data.get(i).value);
                                        mainvalue++;
                                    }
                                }
                            } else {


                                if (i == 0) {
                                    size = new StringBuilder();
                                    if (mainCatList2.get(listlastselectedposiion).get(i).equals("true")) {
                                        mainvalue++;

                                        size = size.append(fullList.get(listlastselectedposiion).data.get(i).value);
                                    }
                                } else {
                                    if (mainCatList2.get(listlastselectedposiion).get(i).equals("true")) {
                                        mainvalue++;

                                        size = size.append("," + fullList.get(listlastselectedposiion).data.get(i).value);
                                    }

                                }
                            }


                        }
                        Log.e("manu", manufecturer.toString());
                        Log.e("itemtype", item_type.toString());
                        Log.e("size", size.toString());


                        //countlist2.set(listlastselectedposiion, mainvalue);


                        countlistmain.put(fullList.get(listlastselectedposiion).code, mainvalue);


                        selectcatogoryafterfilterapply(manufecturer.toString(), item_type.toString(), size.toString());


                        break;
                }
            }
        });
        catagorydetailRecycler.setAdapter(mAdapter2);


    }


    public void selectcatogoryafterfilterapply(final String manufacturer, final String item_type, final String size) {

        parentDialogListener.showProgressDialog();

        listname.clear();

        mainCatListvalue.clear();

        countlist2.clear();


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();


        query.put("id", String.valueOf(value));
        if (!manufacturer.equals("null") && !manufacturer.equals(" ")) {
            query.put("manufacturer", manufacturer);
        }
        if (!item_type.equals("null") && !item_type.equals(" ")) {
            query.put("item_type", item_type);
        }
        if (!size.equals("null") && !size.equals(" ")) {
            query.put("size", size);
        }
        Call<FilterDetail> call = apiService.getFilterCatagoryList(query);
        call.enqueue(new Callback<FilterDetail>() {
            @Override
            public void onResponse(Call<FilterDetail> call, Response<FilterDetail> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    fullList = response.body().data;


                    for (int k = 0; k < fullList.size(); k++) {

                        if (itemcode.get(listlastselectedposiion).equals(fullList.get(k).code)) {
                            listlastselectedposiion = k;
                        }




                    }




                    Log.e("lastselected------>>>>", listlastselectedposiion + "");


                    itemcode.clear();




                    for (int i = 0; i < fullList.size(); i++) {

                        itemcode.add(fullList.get(i).code);


                        // countlist2.add(countlistmain.get(fullList.get(i).code));

                    }



                    for (int k = 0; k < countlistmain.size(); k++) {


                        if (!itemcode.contains(countlistmain.keySet().toArray()[k])) {

                            countlistmain.put(countlistmain.keySet().toArray()[k].toString(),0);

                            for(int j = 0; j < selectedvalue.get(countlistmain.keySet().toArray()[k].toString()).size();j++)
                            {
                                selectedvalue.get(countlistmain.keySet().toArray()[k].toString()).clear();

                                //selectedvalue.get(countlistmain.keySet().toArray()[k].toString())






                            }

                        }


                    }


                    for (int i = 0; i < fullList.size(); i++) {

                        countlist2.add(countlistmain.get(fullList.get(i).code));

                    }












                    for (int i = 0; i < response.body().data.size(); i++) {

                        listname.add(response.body().data.get(i).name);


                        List<String> sublist = new ArrayList<>();
                        mainCatList2.add(sublist);

                        List<String> sublistvalue = new ArrayList<>();
                        mainCatListvalue.add(sublistvalue);

                        // countlist2.add(0);


                    }


                } else {
                    // parentDialogListener.hideProgressDialog();
                    //progressDialog.hide();


                }


                // if (manufacturer.equals(" ") && size.equals(" ") && item_type.equals(" ")) {

                initCatagoryListRecycler();
                //} else {

                //   mAdapter.notifyDataSetChanged();
                //}


                List<String> catagoryselctionlist = new ArrayList<>();


                for (int j = 0; j < fullList.size(); j++) {
                    mainCatList2.get(j).clear();

                    for (int i = 0; i < fullList.get(j).data.size(); i++) {
                        // catagoryselctionlist.add(fullList.get(j).data.get(i).display);
                        if (selectedvalue.get(itemcode.get(j)).contains(fullList.get(j).data.get(i).value)) {

                            mainCatList2.get(j).add("true");
                        } else {
                            mainCatList2.get(j).add("false");
                        }

                        mainCatListvalue.get(j).add(fullList.get(j).data.get(i).value);

                    }
                }

                for (int i = 0; i < fullList.get(listlastselectedposiion).data.size(); i++) {

                    catagoryselctionlist.add(fullList.get(listlastselectedposiion).data.get(i).display);
                }


                // for(int i = 0;i<fullList.size();i++) {
                //    List<String> selectsublist = new ArrayList<>();
                // selectedvalue.put(itemcode.get(i),selectsublist);
                //}


               /* if (manufacturer.equals(" ") && size.equals(" ") && item_type.equals(" ")) {

                    initCatagoryDetailAdapter(catagoryselctionlist, mainCatList2.get(0), 0);
                } else {

                    mAdapter2.notifyDataSetChanged();
                }*/
                Log.e("assfdsfsd", catagoryselctionlist.size() + " " + mainCatList2.get(listlastselectedposiion).size());

                initCatagoryDetailAdapter(catagoryselctionlist, mainCatList2.get(listlastselectedposiion), listlastselectedposiion);





            }


            @Override
            public void onFailure(Call<FilterDetail> call, Throwable t) {
                // parentDialogListener.hideProgressDialog();
                // progressDialog.hide();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void clearall() {




        for (int i = 0; i < itemcode.size(); i++) {


            countlistmain.put(itemcode.get(i), 0);


        }


        for (int i = 0; i < mainCatList2.size(); i++) {


            for (int j = 0; j < mainCatList2.get(i).size(); j++) {

                mainCatList2.get(i).set(j, "false");

            }


        }
        if (mAdapter != null && mAdapter2 != null) {
            //mAdapter.notifyDataSetChanged();
            // mAdapter2.notifyDataSetChanged();
            listlastselectedposiion =0;
            getCatogary(" ", " ", " ");
        }

         selectedvalue.clear();
        //item_type =new StringBuilder();

    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        // dialogDismissed is a Class level variable in the containing Activity,
        // must be set to false each time the DialogFragment is shown
        super.onDismiss(dialog);
    }
}
