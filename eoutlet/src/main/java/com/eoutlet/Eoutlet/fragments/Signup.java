package com.eoutlet.Eoutlet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.eoutlet.Eoutlet.R;

import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.pojo.GetCountryCode;
import com.eoutlet.Eoutlet.pojo.OtpResponse;
import com.eoutlet.Eoutlet.pojo.SignUpResponse;
import com.eoutlet.Eoutlet.utility.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class Signup extends Fragment {
    ArrayAdapter<String> adp2 = null;

    // TODO: Rename and change types of parameters
    private View v;
    private Context context;
    private Context mContext;
    private SaveAddressFragment.OnFragmentInteractionListener mListener;
    private List<String> countryname;
    private List<String> countryCode;
    private List<String> celcode;
    private List<String> placeholder;
    Spinner countryspinner;
    public ParentDialogsListener parentDialogListener;
    private LinearLayout signIn;
    static String jsonstring;
    private boolean isAllConditionFulfilled;
    private String countryName;

    String value;

    int selectedgender;


    private int selctedposition = 0;
    private int mobilevalidationlength;

    ExecuteFragment execute;
    TextView tvSignUp;

    EditText edtFirstName, edtLastName, edtEmail, edtPassword, edtConfirmPassword, edtMobile;
    TextView gender;

    public Signup() {
        // Required empty public constructor
    }

    public void passData(Context context) {
        mContext = context;
    }


    // TODO: Rename and change types and number of parameters
    public static Signup newInstance(String param1, String param2) {
        Signup fragment = new Signup();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.activity_signup, container, false);
        value = getArguments().getString("flag");
        intiViews(v);
        getcountryDetail();
        execute = (MainActivity) mContext;


        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void intiViews(View v) {
        signIn = v.findViewById(R.id.signiin);
        edtFirstName = v.findViewById(R.id.firstName);
        edtEmail = v.findViewById(R.id.eMail);
        edtLastName = v.findViewById(R.id.lastName);
        edtPassword = v.findViewById(R.id.password);
        edtConfirmPassword = v.findViewById(R.id.confirmpassword);
        edtMobile = v.findViewById(R.id.mobile);
        tvSignUp = v.findViewById(R.id.signUp);
        countryspinner = v.findViewById(R.id.countrySpinner);
        gender = v.findViewById(R.id.gender);


        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment prFrag = new LoginNewFragment();
                Bundle bundl = new Bundle();

                prFrag.setArguments(bundl);

                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.profileContainer, prFrag)
                        .commit();


                //execute.ExecutFragmentListener(1);

            }
        });

        gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                initgenderDialog();


            }
        });



      /*  signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment prFrag = new LoginFragment();


                getFragmentManager()
                        .beginTransaction()*//*.addToBackStack(null)*//*
                        .replace(R.id.profileContainer, prFrag)
                        .commit();



            }
        });
*/
    }


    public void initgenderDialog() {


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());



        String[] genders = {"رجل", " سيدة"};

        builder.setItems(genders, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int type) {


                if (type == 0) {
                    gender.setText("رجل");

                    selectedgender = 1;

                } else if (type == 1) {

                    gender.setText("سيدة");
                    selectedgender = 2;


                } else if (type == 2) {
                    selectedgender = 3;

                    gender.setText("غير محدد");


                }


            }
        });
        AlertDialog dialog = builder.create();
        /*TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
        messageText.setTypeface(null, Typeface.BOLD);*/
        dialog.show();


    }

    public void getcountryDetail() {
        parentDialogListener.showProgressDialog();
        countryname = new ArrayList<>();
        countryCode = new ArrayList<>();
        placeholder = new ArrayList<>();
        celcode = new ArrayList<>();


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Call<GetCountryCode> call = apiService.getCountryDetail();
        call.enqueue(new Callback<GetCountryCode>() {
            @Override
            public void onResponse(Call<GetCountryCode> call, Response<GetCountryCode> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    for (int i = 0; i < response.body().data.size(); i++) {
                        String countrycode = response.body().data.get(i).cel_code.replace("+", "");

                        // countryname.add("(" + response.body().data.get(i).cel_code + ")" + " " + response.body().data.get(i).name);
                        countryCode.add(response.body().data.get(i).code);
                        countryname.add("(" + countrycode + "+" + ")" + " " + response.body().data.get(i).name);
                        placeholder.add(response.body().data.get(i).placeholder.replaceAll("\\s", ""));
                        celcode.add(response.body().data.get(i).cel_code);





                    }

                    Collections.reverse(countryCode);
                    Collections.reverse(countryname);
                    Collections.reverse(placeholder);
                    Collections.reverse(celcode);
                    mobilevalidationlength = placeholder.get(0).length();
                    parentDialogListener.hideProgressDialog();




                    initSpinner();


                }
            }

            @Override
            public void onFailure(Call<GetCountryCode> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void initSpinner() {
        if (getContext() != null) {

            adp2 = new ArrayAdapter<String>(getContext(), R.layout.spinnertextview2, countryname);
            parentDialogListener.hideProgressDialog();


            adp2.setDropDownViewResource(R.layout.spinnertextview2);


            countryspinner.setAdapter(adp2);


            countryspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    //Toast.makeText(getApplicationContext(),"Position"+ position, Toast.LENGTH_SHORT).show();


                    countryName = countryspinner.getSelectedItem().toString();
                    selctedposition = position;
                    edtMobile.setHint(placeholder.get(selctedposition));
                    mobilevalidationlength = placeholder.get(selctedposition).length();
                    edtMobile.setFilters(new InputFilter[]{new InputFilter.LengthFilter(placeholder.get(selctedposition).length())});
                    edtMobile.setText("");
                    //edtMobile.setHint("رقم الجوال");
                    Log.e("Gender Name--------.>>", countryName);

                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }

            });
        }

    }

    public void register() {


        isAllConditionFulfilled = Util.checkTextViewValidation(edtFirstName, "برجاء كتابة الاسم الاول")
                && Util.checkTextViewValidation(edtLastName, "برجاء كتابة الاسم الاخير")
                && Util.checkTextViewValidation(edtEmail, "الرجاء إدخال بريد إلكتروني صحيح")
                && Util.isEmailValid(edtEmail)
                && Util.checkTextViewValidation(edtPassword, "برجاء كتابة كلمة السر")
                && Util.checkTextViewValidation(edtConfirmPassword, "برجاء ادخال كود التأكيد") && Util.isPasswordLengthCorrect(edtPassword, "كلمة السر يجب أن لا تقل عن 7 أرقام")
                && Util.isPasswordMatched(edtPassword, edtConfirmPassword, "كلمة المرور لا تتطابق مع الكلمة الاولى")


                && Util.checkTextViewValidation(edtMobile, "من فضلك أدخل رقم الجوال")
                && Util.checkMobileValidation(edtMobile, mobilevalidationlength, "من فضلك أدخل رقم الجوال");
        /* && Util.checkTextViewValidation(gender, "اختر صنف");*/


        if (!isAllConditionFulfilled) {
            if (Util.view_final != null) {

                Util.view_final.requestFocus();
            }


            return;
        }
        opengetOtpDialog();


    }

    public void onSignUpClick(String firstName, String lastName, String eMail, String password, String mobile) {
        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Map<String, String> map1 = new HashMap<>();
        map1.put("firstname", firstName);
        map1.put("lastname", lastName);
        map1.put("email", eMail);
        map1.put("mobilenumber", countryCode.get(selctedposition) + mobile);
        map1.put("password", password);


        Call<SignUpResponse> call = apiService.getSignUp(map1);
        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().id != null) {
                        Toast.makeText(getContext(), "Signup Successfully", Toast.LENGTH_SHORT).show();


                    } else {
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), response.body().message, Toast.LENGTH_SHORT).show();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            parentDialogListener = (ParentDialogsListener) activity;

        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void sendOtp() {

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Map<String, String> map1 = new HashMap<>();
        map1.put("mobile", celcode.get(selctedposition) + edtMobile.getText().toString());
        map1.put("resend", "0");


        Call<OtpResponse> call = apiService.sendotp(map1);
        call.enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().success) {
                        Toast.makeText(getContext(), "OTP send Successfully", Toast.LENGTH_SHORT).show();


                        DialogFragment prFrag = new Otp_Verificationfragment();
                        Bundle bund = new Bundle();
                        bund.putString("flag",value);
                        bund.putString("firstname", edtFirstName.getText().toString());
                        bund.putString("lastname", edtLastName.getText().toString());
                        bund.putString("email", edtEmail.getText().toString());
                        bund.putString("password", edtPassword.getText().toString());
                        bund.putString("mobile", celcode.get(selctedposition) + edtMobile.getText().toString());
                        bund.putString("gender", "1" /*String.valueOf(selectedgender)*/);

                        prFrag.setArguments(bund);

                        prFrag.setTargetFragment(Signup.this, 1001);
                        prFrag.show(getFragmentManager(), "signup");


                    } else {
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), response.body().msg.toString(), Toast.LENGTH_SHORT).show();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void opengetOtpDialog() {

        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage("التأكد من صحة رقم الجوال");
        //alertDialogBuilder.setTitle("التأكد من صحة رقم الجوال");


        alertDialogBuilder.setPositiveButton("الحصول على الكود",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        sendOtp();

                    }
                });

        alertDialogBuilder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {


            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();

        TextView messageView = (TextView) alertDialog.findViewById(android.R.id.message);
        messageView.setTypeface(null, Typeface.BOLD);
        messageView.setGravity(Gravity.CENTER);


        final Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        // LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        // positiveButtonLL.gravity = Gravity.CENTER;
        //positiveButton.setLayoutParams(positiveButtonLL);
        positiveButton.setGravity(Gravity.CENTER);

        final Button negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        negativeButton.setVisibility(View.GONE);


    }

}
