package com.eoutlet.Eoutlet.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.eoutlet.Eoutlet.R;

public class ReplacementFragment extends Fragment {
    private WebView replacementWebview;
    private String value =" ";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_replacement, container, false);
        value = getArguments().getString("position");
        initUi(view);
        return view;
    }

    private void initUi(View view) {
        replacementWebview = view.findViewById(R.id.replacementWebview);

        replacementWebview.getSettings().setJavaScriptEnabled(true);


        WebSettings webSettings = replacementWebview.getSettings();

        //webSettings.setDefaultFontSize(10);

        replacementWebview.loadData(value, "text/html", "UTF-8");

    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {


        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getActivity(), "Error" + description + errorCode, Toast.LENGTH_SHORT).show();
        }

    }

}
