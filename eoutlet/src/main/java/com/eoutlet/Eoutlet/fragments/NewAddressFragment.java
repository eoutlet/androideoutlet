package com.eoutlet.Eoutlet.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.adpters.NewAddressListAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;

import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.listener.CartListener;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.AddressList;
import com.eoutlet.Eoutlet.pojo.ChangePassword;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class NewAddressFragment extends Fragment implements View.OnClickListener,CartListener {
    View view;
    private ParentDialogsListener parentDialogListener;
    private String value = " ";
    private CartListener cartreference;

    private List<String> name = new ArrayList<>();
    private List<String> addressfirstname = new ArrayList<>();

    private List<String> addresslastname = new ArrayList<>();


    private List<String> street = new ArrayList<>();
    private List<String> city = new ArrayList<>();
    private List<String> country = new ArrayList<>();
    private List<String> phone = new ArrayList<>();

    private List<String> address_id = new ArrayList<>();
    private List<String> countryId = new ArrayList<>();
    private TextView btnaddAddress;
    private RecyclerView addressRecycler;
    private NewAddressListAdapter mAdapter;

    private ImageView searchImage,backarrow;
    private Toolbar toolbar1;


    public NewAddressFragment(CheckOutConfirmationfragment cl) {

        cartreference = cl;




    }


    public NewAddressFragment() {
        // Required empty public constructor
    }

//    // TODO: Rename and change types and number of parameters
//    public static Addressfragment newInstance(String param1, String param2) {
//        Addressfragment fragment = new Addressfragment();
//
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_new_address, container, false);
        super.onCreateView(inflater, container, savedInstanceState);

        try {
            value = getArguments().getString("flag");
        } catch (Exception e) {
            e.printStackTrace();
        }
        initViews();
        initonClickListener();

        toolbar1 = view.findViewById(R.id.toolbar);
        searchImage = toolbar1.findViewById(R.id.serachbar);
        backarrow = toolbar1.findViewById((R.id.backarrow));

        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment prFrag = new SearchResultFragment();
                Bundle databund = new Bundle();


                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.profileContainer, prFrag)
                        .commit();



            }
        });

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        //getAddressDetails();

        return view;
    }


    public void initonClickListener() {
        btnaddAddress.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.addnewAddress:
                addanewAddress();
                break;

        }
    }


    public void addanewAddress() {

        Fragment prFrag = new SaveAddressFragment(NewAddressFragment.this);

        if (value.equals("fromCheckout")) {

            Bundle databund = new Bundle();
            databund.putString("flag", "fromCheckout");
            prFrag.setArguments(databund);

            getFragmentManager()
                    .beginTransaction().addToBackStack(null)
                    .replace(R.id.checkoutContainer, prFrag)
                    .commit();


        } else {
            Bundle databund = new Bundle();
            databund.putString("flag", " ");
            prFrag.setArguments(databund);

            getFragmentManager()
                    .beginTransaction().addToBackStack(null)
                    .replace(R.id.addressContainer, prFrag)
                    .commit();


        }


    }

    public void onButtonPressed(Uri uri) {

    }


    public void initViews() {


        addressRecycler = view.findViewById(R.id.address_list_Recycler);
        btnaddAddress = view.findViewById(R.id.addnewAddress);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        parentDialogListener = (ParentDialogsListener) context;


    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void setUserHintcall() {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void getAddressDetails() {

        street.clear();
        name.clear();
        city.clear();
        country.clear();
        phone.clear();
        address_id.clear();
        countryId.clear();

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map1 = new HashMap<>();

        map1.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<AddressList> call = apiService.getAddressInfo(map1);
        call.enqueue(new Callback<AddressList>() {
            @Override
            public void onResponse(Call<AddressList> call, Response<AddressList> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().msg.equals("success")) {




                        for (int i = 0; i < response.body().data.size(); i++) {

                            name.add(response.body().data.get(i).firstname + " " + response.body().data.get(i).lastname);
                            addressfirstname.add(response.body().data.get(i).firstname);
                            addresslastname.add(response.body().data.get(i).lastname);
                            street.add(response.body().data.get(i).street);
                            city.add(response.body().data.get(i).city);
                            country.add(response.body().data.get(i).country);
                            phone.add(response.body().data.get(i).telephone);
                            address_id.add(response.body().data.get(i).addressId);
                            countryId.add(response.body().data.get(i).country_id);


                        }

                        Collections.reverse(name);
                        Collections.reverse(street);
                        Collections.reverse(city);
                        Collections.reverse(country);
                        Collections.reverse(phone);
                        Collections.reverse(address_id);
                        Collections.reverse(countryId);




                        if (response.body().data.size() == 0) {


                            MySharedPreferenceClass.setAddressname(getContext(), null);
                            MySharedPreferenceClass.setCityname(getContext(), null);
                            MySharedPreferenceClass.setStreetName(getContext(), null);
                            MySharedPreferenceClass.setCountryname(getContext(), null);

                            MySharedPreferenceClass.setAddressphone(getContext(), null);
                            MySharedPreferenceClass.setCountryId(getContext(), null);
                            MySharedPreferenceClass.setfirstAdressname(getContext(), null);
                            MySharedPreferenceClass.setlastAdressname(getContext(), null);
                            MySharedPreferenceClass.setSelecteAddressdId(getContext(), "null");




                        }

                        /* else if (response.body().data.size() == 1){

                            MySharedPreferenceClass.setfirstAdressname(getContext(), addressfirstname.get(0).toString());
                            MySharedPreferenceClass.setlastAdressname(getContext(), addresslastname.get(0).toString());
                            MySharedPreferenceClass.setAddressname(getContext(), name.get(0).toString());
                            MySharedPreferenceClass.setCityname(getContext(), city.get(0).toString());
                            MySharedPreferenceClass.setStreetName(getContext(), street.get(0).toString());
                            MySharedPreferenceClass.setCountryname(getContext(), country.get(0).toString());

                            MySharedPreferenceClass.setAddressphone(getContext(), phone.get(0).toString());
                            MySharedPreferenceClass.setCountryId(getContext(), countryId.get(0).toString());


                            MySharedPreferenceClass.setSelecteAddressdId(getContext(), address_id.get(0).toString());






                        }*/
                        initRecycler();
                        parentDialogListener.hideProgressDialog();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<AddressList> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void initRecycler() {


        addressRecycler.setHasFixedSize(true);

        // use a linear layout manager

        addressRecycler.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));

        // specify an adapter (see also next example)
        mAdapter = new NewAddressListAdapter(view.getContext(), name, street, city, country, phone,

                address_id, new ViewListener() {
            @Override
            public void onClick(int position, View view) {
                int id = view.getId();


                switch (id) {

                    case R.id.deleteCart:
                        deleteItems(address_id.get(position), position);


                        break;


                    case R.id.cardBackground:
                        //if(MySharedPreferenceClass.getSelectedAddressId(getContext()).equals(address_id.get(position))){
                        System.out.println(value + "from cardclick---->");
                        MySharedPreferenceClass.setfirstAdressname(getContext(), addressfirstname.get(position));
                        MySharedPreferenceClass.setlastAdressname(getContext(), addresslastname.get(position));
                        MySharedPreferenceClass.setAddressname(getContext(), name.get(position));
                        MySharedPreferenceClass.setCityname(getContext(), city.get(position));
                        MySharedPreferenceClass.setStreetName(getContext(), street.get(position));
                        MySharedPreferenceClass.setCountryname(getContext(), country.get(position));

                        MySharedPreferenceClass.setAddressphone(getContext(), phone.get(position));
                        MySharedPreferenceClass.setCountryId(getContext(), countryId.get(position));


                        // MySharedPreferenceClass.setSelecteAddressdId(getContext(), "0");

                        if (value.equals("fromCheckout")) {
                            cartreference.setUserHintcall();
                            getFragmentManager().popBackStackImmediate();

/*
                            Fragment prFrag = new CheckOutConfirmationfragment();
                            Bundle databund = new Bundle();

                            prFrag.setArguments(databund);
                            getFragmentManager()
                                    .beginTransaction()*//*.addToBackStack(null)*//*
                                    .replace(R.id.checkoutContainer, prFrag)
                                    .commit();*/

                        }

                        break;
                }
            }
        }


        );
        addressRecycler.setAdapter(mAdapter);


    }

    public void deleteItems(final String addressId, final int position) {

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map1 = new HashMap<>();

        map1.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        map1.put("address_id", addressId);


        Call<ChangePassword> call = apiService.deleteAddress(map1);
        call.enqueue(new Callback<ChangePassword>() {
            @Override
            public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().msg.equals("success")) {


                        if (MySharedPreferenceClass.getSelectedAddressId(getContext()).equals(address_id.get(position))) {
                            MySharedPreferenceClass.setSelecteAddressdId(getContext(), "null");


                            MySharedPreferenceClass.setAddressname(getContext(), null);
                            MySharedPreferenceClass.setCityname(getContext(), null);
                            MySharedPreferenceClass.setStreetName(getContext(), null);
                            MySharedPreferenceClass.setCountryname(getContext(), null);
                            MySharedPreferenceClass.setCountryId(getContext(), null);




                            if( CheckOutConfirmationfragment.addresslayotut!=null  && MySharedPreferenceClass.getSelectedAddressId(getContext()).equals("null")) {
                                CheckOutConfirmationfragment.addresslayotut.setVisibility(View.GONE);
                            }




                        }

                        getAddressDetails();


                    } else {
                        parentDialogListener.hideProgressDialog();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<ChangePassword> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        getAddressDetails();
    }


    /*@Override
    public void onViewStateRestored(Bundle inState) {
        super.onViewStateRestored(inState);

        Toast.makeText(getContext(),"lfldkjfls",Toast.LENGTH_SHORT).show();
       // if (inState != null) {

            //getAddressDetails();


      //  }

    }*/

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


    }


}
