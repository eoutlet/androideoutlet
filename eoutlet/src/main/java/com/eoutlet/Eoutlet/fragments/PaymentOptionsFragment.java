package com.eoutlet.Eoutlet.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.util.Calendar;
import android.icu.util.TimeZone;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.ThankyouActivity;
import com.eoutlet.Eoutlet.activities.Threedpasswordactivity;
import com.eoutlet.Eoutlet.adpters.RememberCardAdapter;
import com.eoutlet.Eoutlet.adpters.TrackOrderAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.listener.CartListener;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.listener.ViewListener3;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.OrderResponse;
import com.eoutlet.Eoutlet.pojo.OrderResponseCod;
import com.eoutlet.Eoutlet.pojo.OrderResponseWallet;
import com.eoutlet.Eoutlet.pojo.RememberCardDetail;
import com.eoutlet.Eoutlet.pojo.RememberCardName;
import com.eoutlet.Eoutlet.pojo.ViewCart1;
import com.eoutlet.Eoutlet.pojo.ViewCartData;
import com.eoutlet.Eoutlet.pojo.WalletHistory;
import com.eoutlet.Eoutlet.pojo.WalletPaymentStatus;
import com.eoutlet.Eoutlet.utility.Util;
import com.github.dewinjm.monthyearpicker.MonthFormat;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PaymentOptionsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@RequiresApi(api = Build.VERSION_CODES.N)
public class PaymentOptionsFragment extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private List<ViewCartData> getdata = new ArrayList<>();
    WebView paymentWebview;
    private String coupencode = " ", shippingmethod, orderId, amount,codelabel;
    private ImageView remembermeselect;
    public ParentDialogsListener parentDialogListener;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    int yearSelected;
    int monthSelected;
    private ImageView walletcheckbox;
    private boolean walletaddflag = false;
    private int selectpaymentflag = 99;
    private CardView cardradiobutton, codradiobutton,leftbalancecard;
    private ImageView cardradiobtn, codradiobtn;
    private TextView amounttext, codcharges, walletamount,leftwalletamount;
    private List<RememberCardDetail> carddetail = new ArrayList<>();
    private FrameLayout confirmorder;
    private String codamount;
    private int shippingCharge;
    private EditText cardname, cardnumber, dateandmonth, cvv;
    private boolean is_partialpayment = false;

    private boolean selectesavedcard = false;
    private boolean isgiftwrap = false;

    private CardView edt_card_detail;
    private int freshwalletAmount;
    private boolean remembermeflag = false;
    private RecyclerView cardrecycler;
    private RememberCardAdapter cardAdapter;
    private LinearLayout cardrecyclerlayout, cvvlayout;
    View view;
    private String fillname, fillcardnumber, filldateandmonth, fillcvv, savedcardcvv, tokenname;

    private int selectedcardposition =99;
    private CartListener cartreference;
    Calendar calendar = Calendar.getInstance();
    MonthYearPickerDialogFragment dialogFragment;

    public PaymentOptionsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PaymentOptionsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PaymentOptionsFragment newInstance(String param1, String param2) {
        PaymentOptionsFragment fragment = new PaymentOptionsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    /* public PaymentOptionsFragment(CartFragment cl) {

        cartreference = cl;


    }
*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_payment_options, container, false);
        initUi(view);
        getCartData();
        yearSelected = calendar.get(Calendar.YEAR);
        monthSelected = calendar.get(Calendar.MONTH);
        //initpaymentwebview();
        return view;
    }

    public void initUi(View view) {
        paymentWebview = view.findViewById(R.id.paymentWebview);
        walletcheckbox = view.findViewById(R.id.walletcheckbox);
        cardradiobutton = view.findViewById(R.id.cardradiobutton);
        codradiobutton = view.findViewById(R.id.codradiobutton);
       // leftbalancecard = view.findViewById(R.id.leftbalancecard);
        cardradiobtn = view.findViewById(R.id.cardradiobtn);
        codradiobtn = view.findViewById(R.id.codradiobtn);
        edt_card_detail = view.findViewById(R.id.edt_card_detail);
        amounttext = view.findViewById(R.id.checkoueamounttext);
        confirmorder = view.findViewById(R.id.checkoutconfirmorder);
        codcharges = view.findViewById(R.id.codcharges);
        walletamount = view.findViewById(R.id.walletamount);
        leftwalletamount = view.findViewById(R.id.leftwalletamount);

        cardname = view.findViewById(R.id.edtcardname);
        cardnumber = view.findViewById(R.id.edtcardnumber);
        dateandmonth = view.findViewById(R.id.monthandyear);
        cvv = view.findViewById(R.id.cvv);
        remembermeselect = view.findViewById(R.id.remembermeselect);
        cardrecyclerlayout = view.findViewById(R.id.cardrecyclerlayout);
        cvvlayout = view.findViewById(R.id.cvvlayout);

        coupencode = getArguments().getString("coupencode");

        shippingmethod = getArguments().getString("shippingmethod");
        amount = getArguments().getString("amount");
        codamount = getArguments().getString("codcharges");
        shippingCharge  =  getArguments().getInt("shippingcharge");
        isgiftwrap = getArguments().getBoolean("wrappingflag");
        codelabel = getArguments().getString("codelabel");

        amounttext.setText("SAR" + " " + amount + " " + "الاجمالي");


      //  codcharges.setText("SAR" + " " + codamount + " " + "+ الدفع عند الاستلام");
        //codcharges.setText("دفع عند الاستلام " +" " +"+" +codamount+ " " + "والشحن مجاني");
        codcharges.setText(codelabel);


        walletamount.setText("SAR" + " " + MySharedPreferenceClass.getWalletamount(getContext()) + " " + "- رصيد حسابي");

        //leftwalletamount.setText("SAR" + " " + freshwalletAmount + " " + "الرصيد المتبقي بحسابكم:");

        dateandmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MonthFormat monthFormat = MonthFormat.LONG; //MonthFormat.LONG or MonthFormat.SHORT
                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                calendar.get(Calendar.YEAR);


                Log.d(TAG, "@ thisYear : " + ( calendar.get(Calendar.YEAR)));
                Log.d(TAG, "@ thisYear : " + ( calendar.get(Calendar.MONTH)));
                Log.d(TAG, "@ thisYear : " + ( calendar.get(Calendar.DAY_OF_MONTH)));

                calendar.set(calendar.get(Calendar.YEAR), ( calendar.get(Calendar.MONTH)), calendar.get(Calendar.DAY_OF_MONTH)); // Set minimum date to show in dialog
                long minDate = calendar.getTimeInMillis();

                calendar.clear();
               //MonthFormat.LONG or MonthFormat.SHORT
                Calendar calendarend = Calendar.getInstance(TimeZone.getDefault());

                calendar.set(calendarend.get(Calendar.YEAR)+10, ( calendarend.get(Calendar.MONTH)+10), calendarend.get(Calendar.DAY_OF_MONTH)+10); // Set maximum date to show in dialog
                long maxDate = calendar.getTimeInMillis(); // G

                dialogFragment = MonthYearPickerDialogFragment
                        .getInstance(monthSelected, yearSelected, minDate, maxDate, "", monthFormat);
                dialogFragment.show(getFragmentManager(), null);

                dialogFragment.setOnDateSetListener(new MonthYearPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int year, int monthOfYear) {
                        // do something
                        Log.e("asds", year + "/" + monthOfYear);
                        int monthofyear = monthOfYear + 1;


                        String actualmonth = String.valueOf(monthofyear);

                        if (actualmonth.toString().length() < 2) {

                            actualmonth = "0" + actualmonth;


                        }

                        String actualyear = String.valueOf(year).substring(String.valueOf(year).length() - 2);

                        String concatstring = actualyear + "/" + actualmonth;


                        dateandmonth.setText(concatstring);


                    }
                });


            }
        });


        if (!MySharedPreferenceClass.getCountryId(getContext()).equals("SA") || isgiftwrap) {

            codradiobutton.setVisibility(View.GONE);

        }

        remembermeselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!remembermeflag) {
                    remembermeselect.setImageResource(R.drawable.checkboxselected);
                    remembermeflag = true;
                } else {
                    remembermeselect.setImageResource(R.drawable.checkboxunselected);
                    remembermeflag = false;
                }


            }
        });
        // getCartData();


        confirmorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fillname = cardname.getText().toString();
                fillcardnumber = cardnumber.getText().toString();
                filldateandmonth = dateandmonth.getText().toString();
                fillcvv = cvv.getText().toString();
                if (selectpaymentflag == 1) {
                    boolean isAllConditionFulfilled = true;

                    isAllConditionFulfilled = cardname.getText().toString().trim().length() > 0
                            && cardnumber.getText().toString().trim().length() > 0 &&
                            dateandmonth.getText().toString().trim().length() > 0 &&
                            cardnumber.getText().toString().trim().length() > 0
                            &&
                            cvv.getText().toString().trim().length() > 0;

                    if (!isAllConditionFulfilled) {


                        Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();
                        return;

                    }


                    placeorderforpayfort();
                } else if (selectpaymentflag == 2) {

                    placeorder();


                } else if (selectpaymentflag == 0) {
                    if(freshwalletAmount>=Integer.parseInt(amount)) {
                        placeorderwithwallet();
                    }

                    else{
                        Toast.makeText(getContext(), "برجاء اختيار طريقة الدفع للاستمرار", Toast.LENGTH_LONG).show();

                    }

                }




                else if (selectpaymentflag == 3) {
                    if (!savedcardcvv.isEmpty()) {

                        placeorderwithsavedcard();
                    }
                    else{
                        Toast.makeText(getContext(), "برجاء اختيار طريقة الدفع للاستمرار", Toast.LENGTH_LONG).show();

                    }


                } else {
                    Toast.makeText(getContext(), "رصيدك لايكفي برجاء اختيار طريقة دفع اضافية لسداد باقي قيمة الطلب", Toast.LENGTH_LONG).show();


                }

            }
        });


        cardradiobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            /*    if(Integer.valueOf(amount)<200 && MySharedPreferenceClass.getCountryId(getContext()).equals("SA")){
                    int amoutnaftershippingcharge = Integer.valueOf(amount)+25;
                    amounttext.setText("SAR"+" "+amoutnaftershippingcharge);
                }*/

                // Toast.makeText(getContext(),walletaddflag+" ",Toast.LENGTH_SHORT).show();

                if (walletaddflag == true && amounttext.getText().toString().split(" ")[1].equals("0") && selectpaymentflag != 1) {
                    //Toast.makeText(getContext(),walletaddflag+"first",Toast.LENGTH_SHORT).show();

                    walletamount.setText("SAR" + " " + MySharedPreferenceClass.getWalletamount(getContext()) + " " + "- رصيد حسابي");
                    walletcheckbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                   leftwalletamount.setVisibility(View.GONE);
                    cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                    walletaddflag = false;
                    is_partialpayment = false;


                    selectedcardposition = 0;
                    initSavedCardRecycler();
                }

                if (selectpaymentflag != 1) {


                    if (!walletaddflag) {

                        // Toast.makeText(getContext(),walletaddflag+"inside",Toast.LENGTH_SHORT).show();

                        cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                        codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                        amounttext.setText("SAR" + " " + amount + " " + "الاجمالي");
                        edt_card_detail.setVisibility(View.VISIBLE);
                        cardrecyclerlayout.setVisibility(View.VISIBLE);
                        selectpaymentflag = 1;

                        selectedcardposition = 0;
                        initSavedCardRecycler();
                    } else {

                        cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                        codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                        int amount_afterwallet = Integer.parseInt(amount) - freshwalletAmount;
                        amounttext.setText("SAR" + " " + amount_afterwallet + " " + "الاجمالي");
                        edt_card_detail.setVisibility(View.VISIBLE);
                        cardrecyclerlayout.setVisibility(View.VISIBLE);
                        selectpaymentflag = 1;
                        selectedcardposition = 0;
                        initSavedCardRecycler();

                    }


                }


            }
        });


        codradiobutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (walletaddflag && amounttext.getText().toString().split(" ")[1].equals("0") && selectpaymentflag != 2) {
                    /*if(MySharedPreferenceClass.getCountryId(getContext()).equals("SA")*//* && Integer.valueOf(amount) < 200*//*  ){
                        int amountafterremoveshippingcharge =Integer.valueOf(amount)+Integer.valueOf(codamount)-shippingCharge;
                        amounttext.setText("SAR"+" "+amountafterremoveshippingcharge + " " + "الاجمالي");
                        //amount = String.valueOf(amountafterremoveshippingcharge);
                    }
*/

                    walletamount.setText("SAR" + " " + MySharedPreferenceClass.getWalletamount(getContext()) + " " + "- رصيد حسابي");
                    walletcheckbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                    leftwalletamount.setVisibility(View.GONE);
                    codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));



                    walletaddflag = false;
                    is_partialpayment = false;

                    selectedcardposition = 0;
                    initSavedCardRecycler();

                }


                if (selectpaymentflag != 2) {


                    if (!walletaddflag) {
                        if (freshwalletAmount <= Integer.parseInt(amounttext.getText().toString().split(" ")[1])) {
                            codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                            cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                            int amountpluscod = Integer.parseInt(amounttext.getText().toString().split(" ")[1]) + Integer.parseInt(codamount);
                            amounttext.setText("SAR" + " " + amountpluscod + " " + "الاجمالي");
                            selectedcardposition = 0;
                            initSavedCardRecycler();
                        }

                        if (freshwalletAmount > Integer.parseInt(amounttext.getText().toString().split(" ")[1])) {
                            codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                            cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                            int amountpluscod = Integer.parseInt(amount) + Integer.parseInt(codamount);
                            amounttext.setText("SAR" + " " + amountpluscod + " " + "الاجمالي");
                            selectedcardposition = 0;
                            initSavedCardRecycler();
                        }
                       /* if(MySharedPreferenceClass.getCountryId(getContext()).equals("SA")   Integer.valueOf(amount) < 200 ){
                            int amountafterremoveshippingcharge =Integer.valueOf(amount)+Integer.valueOf(codamount)-shippingCharge;
                            amounttext.setText("SAR"+" "+amountafterremoveshippingcharge + " " + "الاجمالي");

                        }*/
                        edt_card_detail.setVisibility(View.GONE);
                        selectpaymentflag = 2;

                    } else {

                        codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                        cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                        int amountpluscod = Integer.parseInt(amount) + Integer.parseInt(codamount) - Integer.parseInt(MySharedPreferenceClass.getWalletamount(getContext()))-shippingCharge;



                        amounttext.setText("SAR" + " " + amountpluscod + " " + "الاجمالي");

                        edt_card_detail.setVisibility(View.GONE);
                        // cardrecyclerlayout.setVisibility(View.GONE);
                        selectpaymentflag = 2;
                        selectedcardposition = 0;
                        initSavedCardRecycler();


                    }
                }


            }
        });


        walletcheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!walletaddflag) {


                    leftwalletamount.setVisibility(View.VISIBLE);
                    walletcheckbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                    walletaddflag = true;
                    is_partialpayment = true;


if(selectpaymentflag==1 || selectpaymentflag==3 || selectpaymentflag==99 ) {
    if (freshwalletAmount <= Integer.parseInt(amounttext.getText().toString().split(" ")[1])) {
        int amountafterwalletapply = Integer.parseInt(amounttext.getText().toString().split(" ")[1]) - freshwalletAmount;
        amounttext.setText("SAR" + " " + amountafterwalletapply + " " + "الاجمالي");
        leftwalletamount.setText("SAR" + " " + "0" + " " + ": الرصيد المتبقي بحسابكم");


        selectpaymentflag = 0;





    } else if (freshwalletAmount > Integer.parseInt(amounttext.getText().toString().split(" ")[1])) {


        int walletbalance = freshwalletAmount- Integer.parseInt(amounttext.getText().toString().split(" ")[1]);

        leftwalletamount.setText("SAR" + " " + walletbalance + " " + ": الرصيد المتبقي بحسابكم");


        walletamount.setText("SAR" + " " + amounttext.getText().toString().split(" ")[1] + " " + "- رصيد حسابي");


        amounttext.setText("SAR" + " " + 0 + " " + "الاجمالي");

       selectpaymentflag = 0;



    }
}
else if(selectpaymentflag==2) {

    if (freshwalletAmount <= Integer.parseInt(amount)) {
        int amountafterwalletapply = Integer.parseInt(amounttext.getText().toString().split(" ")[1]) - freshwalletAmount;

        amounttext.setText("SAR" + " " + amountafterwalletapply + " " + "الاجمالي");
        leftwalletamount.setText("SAR" + " " + "0" + " " + ": الرصيد المتبقي بحسابكم");

    } else if (freshwalletAmount > Integer.parseInt(amount)) {

        int walletbalance = freshwalletAmount - Integer.parseInt(amount);

        leftwalletamount.setText("SAR" + " " + walletbalance + " " + ": الرصيد المتبقي بحسابكم");

        walletamount.setText("SAR" + " " + amounttext.getText().toString().split(" ")[1] + " " + "- رصيد حسابي");


        amounttext.setText("SAR" + " " + 0 + " " + "الاجمالي");

    }

}
                    if (selectpaymentflag == 1 || selectpaymentflag ==0 && amounttext.getText().toString().split(" ")[1].equals("0")) {
                        cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                        selectpaymentflag = 0;


                    } else if (selectpaymentflag == 2 && amounttext.getText().toString().split(" ")[1].equals("0")) {
                        walletamount.setText("SAR" + " " + amount + " " + "- رصيد حسابي");

                        codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                        selectpaymentflag = 0;


                    }



                    else if (selectpaymentflag == 3 && amounttext.getText().toString().split(" ")[1].equals("0")) {

                        selectedcardposition = 0;
                        initSavedCardRecycler();
                        selectpaymentflag = 0;


                    }


                } else {

                    walletcheckbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                    walletaddflag = false;
                    is_partialpayment = false;

                    leftwalletamount.setVisibility(View.GONE);
                    if (freshwalletAmount <= Integer.parseInt(amount)) {
                        walletamount.setText("SAR" + " " + MySharedPreferenceClass.getWalletamount(getContext()) + " " + "- رصيد حسابي");
                        int walletremoveamount = Integer.parseInt(amounttext.getText().toString().split(" ")[1]) + Integer.parseInt(MySharedPreferenceClass.getWalletamount(getContext()));
                        amounttext.setText("SAR" + " " + walletremoveamount + " " + "الاجمالي");

                        selectpaymentflag = 99;

                    }
                    else if (freshwalletAmount > Integer.parseInt(amount)) {
                        walletamount.setText("SAR" + " " + freshwalletAmount + " " + "- رصيد حسابي");
                        amounttext.setText("SAR" + " " + amount + " " + "الاجمالي");

                        selectpaymentflag = 99;

                    }



                }


            }
        });


    }


    private class MyBrowser extends WebViewClient {


        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            //  parentDialogListener.showProgressDialog();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            return super.shouldOverrideUrlLoading(view, url);
/*
            view.loadUrl(url);



            return true;*/
        }

        @Override
        public void onPageFinished(WebView view, String url) {


        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getContext(), "Error" + description + errorCode, Toast.LENGTH_SHORT).show();
        }

    }

    public void initpaymentwebview() {


        paymentWebview.getSettings().setJavaScriptEnabled(true);

        CookieManager.getInstance().removeAllCookies(null);
        CookieManager.getInstance().flush();

        paymentWebview.getSettings().setJavaScriptEnabled(true);
        paymentWebview.getSettings().setUseWideViewPort(true);
        paymentWebview.getSettings().setLoadWithOverviewMode(true);
        paymentWebview.clearCache(true);
        paymentWebview.clearHistory();
        paymentWebview.clearFormData();
        paymentWebview.setWebViewClient(new MyBrowser());

        paymentWebview.loadUrl("https://staging.eoutlet.com/eoutletpayfort/index/index/");


    }


    public void placeorderwithwallet() {

        HashMap<Object, Object> initialorder = new HashMap<>();
        initialorder.put("user_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("devicetype", "android");

        initialorder.put("payment_method", "wallet");
        initialorder.put("shipping_method", shippingmethod);
        initialorder.put("is_partial", "0");
        if(isgiftwrap) {
            initialorder.put("gift_wrap_fee", "1");
        }
        else{
            initialorder.put("gift_wrap_fee", "0");

        }
        if (coupencode != " ") {
            initialorder.put("coupon", coupencode);
        } else {

            initialorder.put("coupon", " ");
        }

        HashMap<Object, Object> payment = new HashMap<>();
        payment.put("firstname", MySharedPreferenceClass.getMyFirstNamePref(getContext()));
        payment.put("lastname", MySharedPreferenceClass.getMyLastNamePref(getContext()));
        payment.put("city", MySharedPreferenceClass.getCityName(getContext()));

        payment.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        payment.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        payment.put("telephone", MySharedPreferenceClass.getMobileNumber(getContext()));

        HashMap<String, String> street = new HashMap<>();
        street.put("0", MySharedPreferenceClass.getStreetname(getContext()));
        street.put("1", " ");
        payment.put("street", street);

        initialorder.put("payment", payment);

        HashMap<Object, Object> shipping = new HashMap<>();

        shipping.put("firstname", MySharedPreferenceClass.getfirstAdressname(getContext()));
        shipping.put("lastname", MySharedPreferenceClass.getlastAdressname(getContext()));
        shipping.put("street", street);
        shipping.put("city", MySharedPreferenceClass.getCityName(getContext()));
        shipping.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("telephone", MySharedPreferenceClass.getAdressphone(getContext()));

        initialorder.put("shipping", shipping);


        HashMap<String, String> products = new HashMap<>();


        ArrayList<Object> mainarraylist = new ArrayList<>();


        for (int i = 0; i < getdata.size(); i++) {

            HashMap<String, Object> superattribute = new HashMap<>();

            HashMap<String, String> colorandsize = new HashMap<>();

            if (getdata.get(i).option.size() > 0) {
                colorandsize.put(getdata.get(i).option.get(0).color.id.toString(), getdata.get(i).option.get(0).color.value.toString());
                colorandsize.put(getdata.get(i).option.get(0).size.id.toString(), getdata.get(i).option.get(0).size.value.toString());
                superattribute.put("super_attribute", colorandsize);

            }

            superattribute.put("product_id", getdata.get(i).productId);
            superattribute.put("qty", getdata.get(i).qty);

            mainarraylist.add(superattribute);

        }
        initialorder.put("products", mainarraylist);
        initialorder.put("transaction_id", " ");


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<OrderResponseWallet> call = apiService.createorderWallet(initialorder);
        call.enqueue(new Callback<OrderResponseWallet>() {
            @Override
            public void onResponse(Call<OrderResponseWallet> call, Response<OrderResponseWallet> response) {

                parentDialogListener.hideProgressDialog();
                if (response.body() != null) {

                    if (response.body().msg!=null &&  response.body().msg.equals("success")) {
                        MySharedPreferenceClass.setBedgeCount(getContext(),0);

                        Intent in = new Intent(getContext(), ThankyouActivity.class);
                        in.putExtra("orderId", response.body().orderId);
                        in.putExtra("statusflag", response.body().msg);
                       in.putExtra("totalvalue",amounttext.getText().toString().split(" ")[1]);
                       in.putExtra("coupencode",coupencode);
                        //in.putExtra("coupencode",coupencode);
                        startActivity(in);

                    } else {
                        // openNavigatetoCartdialog(response.body().data.toString());
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), response.body().data.toString(), Toast.LENGTH_SHORT).show();

                    }


                }


            }


            @Override
            public void onFailure(Call<OrderResponseWallet> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                //  openNavigatetoCartdialog("بعض المنتجات غير متوفرة بالمخزون");

                Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();
            }
        });


    }


    public void placeorder() {

        HashMap<Object, Object> initialorder = new HashMap<>();
        initialorder.put("user_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("devicetype", "android");
        if(isgiftwrap) {
            initialorder.put("gift_wrap_fee", "1");
        }
        else{
            initialorder.put("gift_wrap_fee", "0");

        }

        initialorder.put("payment_method", "msp_cashondelivery");
        initialorder.put("shipping_method", shippingmethod);
        if (coupencode != " ") {
            initialorder.put("coupon", coupencode);
        } else {

            initialorder.put("coupon", " ");


        }


        if (!walletaddflag) {
            initialorder.put("is_partial", "0");
        } else {
            initialorder.put("is_partial", "1");
        }
        HashMap<Object, Object> payment = new HashMap<>();
        payment.put("firstname", MySharedPreferenceClass.getMyFirstNamePref(getContext()));
        payment.put("lastname", MySharedPreferenceClass.getMyLastNamePref(getContext()));
        payment.put("city", MySharedPreferenceClass.getCityName(getContext()));

        payment.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        payment.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        payment.put("telephone", MySharedPreferenceClass.getMobileNumber(getContext()));

        HashMap<String, String> street = new HashMap<>();
        street.put("0", MySharedPreferenceClass.getStreetname(getContext()));
        street.put("1", " ");
        payment.put("street", street);

        initialorder.put("payment", payment);

        HashMap<Object, Object> shipping = new HashMap<>();

        shipping.put("firstname", MySharedPreferenceClass.getfirstAdressname(getContext()));
        shipping.put("lastname", MySharedPreferenceClass.getlastAdressname(getContext()));
        shipping.put("street", street);
        shipping.put("city", MySharedPreferenceClass.getCityName(getContext()));
        shipping.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("telephone", MySharedPreferenceClass.getAdressphone(getContext()));

        initialorder.put("shipping", shipping);


        HashMap<String, String> products = new HashMap<>();


        ArrayList<Object> mainarraylist = new ArrayList<>();


        for (int i = 0; i < getdata.size(); i++) {

            HashMap<String, Object> superattribute = new HashMap<>();

            HashMap<String, String> colorandsize = new HashMap<>();

            if (getdata.get(i).option.size() > 0) {
                colorandsize.put(getdata.get(i).option.get(0).color.id.toString(), getdata.get(i).option.get(0).color.value.toString());
                colorandsize.put(getdata.get(i).option.get(0).size.id.toString(), getdata.get(i).option.get(0).size.value.toString());
                superattribute.put("super_attribute", colorandsize);

            }

            superattribute.put("product_id", getdata.get(i).productId);
            superattribute.put("qty", getdata.get(i).qty);

            mainarraylist.add(superattribute);

        }
        initialorder.put("products", mainarraylist);
        initialorder.put("transaction_id", " ");


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<OrderResponseCod> call = apiService.createorderCod(initialorder);
        call.enqueue(new Callback<OrderResponseCod>() {
            @Override
            public void onResponse(Call<OrderResponseCod> call, Response<OrderResponseCod> response) {

                parentDialogListener.hideProgressDialog();
                if (response.body() != null) {

                    if (response.body().msg!=null && response.body().msg.equals("success")) {


                        if(response.body().paymentMethod.equals("msp_cashondelivery") && is_partialpayment){

                          MySharedPreferenceClass.setBedgeCount(getContext(),0);
                           rungetstatusAPI();


                        }

                        else {

                            MySharedPreferenceClass.setBedgeCount(getContext(),0);
                            Intent in = new Intent(getContext(), ThankyouActivity.class);
                            in.putExtra("orderId", response.body().orderId);
                            in.putExtra("statusflag", response.body().msg);
                            in.putExtra("totalvalue",amounttext.getText().toString().split(" ")[1]);
                            in.putExtra("coupencode",coupencode);
                            //in.putExtra("coupencode",coupencode);
                            startActivity(in);
                        }

                    } else {
                         openNavigatetoCartdialog(response.body().data.toString());
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();

                    }


                }


            }


            @Override
            public void onFailure(Call<OrderResponseCod> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                //  openNavigatetoCartdialog("بعض المنتجات غير متوفرة بالمخزون");
                Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();
                //Toast.makeText(getContext(), "Shipping method is not available for this address.", Toast.LENGTH_LONG).show();
            }
        });


    }
    public void openNavigatetoCartdialog(String message) {
        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage(message);


        alertDialogBuilder.setPositiveButton("موافق",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        cartreference.setUserHintcall();//}
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    }


                });

        alertDialogBuilder.setNegativeButton("إلغاء", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();


            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }


    public void rungetstatusAPI() {


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();


        query.put("order_id", orderId);


        Call<WalletPaymentStatus> call = apiService.getpaymentstatus(query);
        call.enqueue(new Callback<WalletPaymentStatus>() {
            @Override
            public void onResponse(Call<WalletPaymentStatus> call, Response<WalletPaymentStatus> response) {


                if (response.body() != null) {

                    parentDialogListener.hideProgressDialog();

                    if (response.body().msg.equals("success")) {


                        Intent in = new Intent(getContext(), ThankyouActivity.class);
                        in.putExtra("orderId", orderId);
                        in.putExtra("statusflag", response.body().msg);

                        //in.putExtra("coupencode",coupencode);
                        startActivity(in);
                        getActivity().finish();

                    }



                }

            }


            @Override
            public void onFailure(Call<WalletPaymentStatus> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                // progressDialog.hide();
                Log.e(TAG, t.toString());

               // Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }



    public void placeorderforpayfort() {

        HashMap<Object, Object> initialorder = new HashMap<>();
        initialorder.put("amount", amounttext.getText().toString().split(" ")[1]);
        initialorder.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        if(isgiftwrap) {
            initialorder.put("gift_wrap_fee", "1");
        }
        else{
            initialorder.put("gift_wrap_fee", "0");

        }

        if (!remembermeflag) {
            initialorder.put("remember_me", "NO");
        } else {
            initialorder.put("remember_me", "YES");
        }
        if (!walletaddflag) {
            initialorder.put("is_partial", "0");
        } else {
            initialorder.put("is_partial", "1");
        }

        initialorder.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("user_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("payment_method", "payfort_fort_cc");
        initialorder.put("shipping_method", shippingmethod);
        initialorder.put("devicetype", "android");

        if (coupencode != " ") {
            initialorder.put("coupon", coupencode);
        } else {

            initialorder.put("coupon", " ");


        }


        HashMap<Object, Object> payment = new HashMap<>();
        payment.put("firstname", MySharedPreferenceClass.getMyFirstNamePref(getContext()));
        payment.put("lastname", MySharedPreferenceClass.getMyLastNamePref(getContext()));
        payment.put("city", MySharedPreferenceClass.getCityName(getContext()));

        payment.put("country_id", "SA");
        payment.put("region", "Riyadh");
        payment.put("telephone", MySharedPreferenceClass.getMobileNumber(getContext()));

        HashMap<String, String> street = new HashMap<>();
        street.put("0", MySharedPreferenceClass.getStreetname(getContext()));
        street.put("1", " ");
        payment.put("street", street);

        initialorder.put("payment", payment);


        HashMap<Object, Object> paymentdetail = new HashMap<>();
        paymentdetail.put("name", fillname);
        paymentdetail.put("number", fillcardnumber);
        paymentdetail.put("securitycode", fillcvv);
        paymentdetail.put("expiry", filldateandmonth.replace("/", ""));

        initialorder.put("card_detail", paymentdetail);


        HashMap<Object, Object> shipping = new HashMap<>();

        shipping.put("firstname", MySharedPreferenceClass.getfirstAdressname(getContext()));
        shipping.put("lastname", MySharedPreferenceClass.getlastAdressname(getContext()));
        shipping.put("street", street);
        shipping.put("city", MySharedPreferenceClass.getCityName(getContext()));
        shipping.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("telephone", MySharedPreferenceClass.getAdressphone(getContext()));

        initialorder.put("shipping", shipping);


        HashMap<String, String> products = new HashMap<>();


        ArrayList<Object> mainarraylist = new ArrayList<>();


        for (int i = 0; i < getdata.size(); i++) {

            HashMap<String, Object> superattribute = new HashMap<>();
            //HashMap<String,String>  size = new HashMap<>();
            HashMap<String, String> colorandsize = new HashMap<>();

            if (getdata.get(i).option.size() > 0) {
                colorandsize.put(getdata.get(i).option.get(0).color.id.toString(), getdata.get(i).option.get(0).color.value.toString());
                colorandsize.put(getdata.get(i).option.get(0).size.id.toString(), getdata.get(i).option.get(0).size.value.toString());
                superattribute.put("super_attribute", colorandsize);

            }

            superattribute.put("product_id", getdata.get(i).productId);
            superattribute.put("qty", getdata.get(i).qty);

            mainarraylist.add(superattribute);

        }


        initialorder.put("products", mainarraylist);
        initialorder.put("transaction_id", " ");


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<OrderResponse> call = apiService.createorder(initialorder);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {

                parentDialogListener.hideProgressDialog();
                if (response.body() != null) {

                    if (response.body().status!=null && response.body().status.equals("20")) {


                        if (!response.body()._3dsUrl.isEmpty() && !response.body().orderId.equals(null)) {
                            Fragment prFrag = new OrderPaymentWebView();
                            Bundle bundle = new Bundle();
                            bundle.putString("order_id", response.body().orderId);
                            bundle.putString("url", response.body()._3dsUrl);
                            bundle.putString("totalvalue",amounttext.getText().toString().split(" ")[1]);
                            bundle.putString("coupencode",coupencode);
                            prFrag.setArguments(bundle);

                            getFragmentManager()
                                    .beginTransaction().addToBackStack(null)
                                    .add(R.id.cartcontainer, prFrag)
                                    .commit();


                        }

                    } else {
                        // openNavigatetoCartdialog(response.body().data.toString());
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();

                    }


                }


            }


            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();
                //  openNavigatetoCartdialog("بعض المنتجات غير متوفرة بالمخزون");

                //Toast.makeText(getContext(), "Shipping method is not available for this address.", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void placeorderwithsavedcard() {
        HashMap<Object, Object> initialorder = new HashMap<>();
        initialorder.put("amount", amounttext.getText().toString().split(" ")[1]);
        initialorder.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("token_name", tokenname);
        if(isgiftwrap) {
            initialorder.put("gift_wrap_fee", "1");
        }
        else{
            initialorder.put("gift_wrap_fee", "0");

        }
        if (!remembermeflag) {
            initialorder.put("remember_me", "NO");
        } else {
            initialorder.put("remember_me", "YES");
        }
        if (!walletaddflag) {
            initialorder.put("is_partial", "0");
        } else {
            initialorder.put("is_partial", "1");
        }

        initialorder.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("user_id", MySharedPreferenceClass.getMyUserId(getContext()));
        initialorder.put("payment_method", "payfort_fort_cc");
        initialorder.put("shipping_method", shippingmethod);
        initialorder.put("devicetype", "android");

        if (coupencode != " ") {
            initialorder.put("coupon", coupencode);
        } else {

            initialorder.put("coupon", " ");


        }


        HashMap<Object, Object> payment = new HashMap<>();
        payment.put("firstname", MySharedPreferenceClass.getMyFirstNamePref(getContext()));
        payment.put("lastname", MySharedPreferenceClass.getMyLastNamePref(getContext()));
        payment.put("city", MySharedPreferenceClass.getCityName(getContext()));

        payment.put("country_id", "SA");
        payment.put("region", "Riyadh");
        payment.put("telephone", MySharedPreferenceClass.getMobileNumber(getContext()));

        HashMap<String, String> street = new HashMap<>();
        street.put("0", MySharedPreferenceClass.getStreetname(getContext()));
        street.put("1", " ");
        payment.put("street", street);

        initialorder.put("payment", payment);


        HashMap<Object, Object> paymentdetail = new HashMap<>();
        paymentdetail.put("name", " ");
        paymentdetail.put("number", " ");
        paymentdetail.put("securitycode", savedcardcvv);
        paymentdetail.put("expiry", " ");

        initialorder.put("card_detail", paymentdetail);


        HashMap<Object, Object> shipping = new HashMap<>();

        shipping.put("firstname", MySharedPreferenceClass.getfirstAdressname(getContext()));
        shipping.put("lastname", MySharedPreferenceClass.getlastAdressname(getContext()));
        shipping.put("street", street);
        shipping.put("city", MySharedPreferenceClass.getCityName(getContext()));
        shipping.put("country_id", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("region", MySharedPreferenceClass.getCountryId(getContext()));
        shipping.put("telephone", MySharedPreferenceClass.getAdressphone(getContext()));

        initialorder.put("shipping", shipping);


        HashMap<String, String> products = new HashMap<>();


        ArrayList<Object> mainarraylist = new ArrayList<>();


        for (int i = 0; i < getdata.size(); i++) {

            HashMap<String, Object> superattribute = new HashMap<>();
            //HashMap<String,String>  size = new HashMap<>();
            HashMap<String, String> colorandsize = new HashMap<>();

            if (getdata.get(i).option.size() > 0) {
                colorandsize.put(getdata.get(i).option.get(0).color.id.toString(), getdata.get(i).option.get(0).color.value.toString());
                colorandsize.put(getdata.get(i).option.get(0).size.id.toString(), getdata.get(i).option.get(0).size.value.toString());
                superattribute.put("super_attribute", colorandsize);

            }

            superattribute.put("product_id", getdata.get(i).productId);
            superattribute.put("qty", getdata.get(i).qty);

            mainarraylist.add(superattribute);

        }


        initialorder.put("products", mainarraylist);
        initialorder.put("transaction_id", " ");


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<OrderResponse> call = apiService.createorder(initialorder);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {

                parentDialogListener.hideProgressDialog();
                if (response.body() != null) {

                    if (response.body().status!=null && response.body().status.equals("20")) {


                        if (!response.body()._3dsUrl.isEmpty() && !response.body().orderId.equals(null)) {
                            Fragment prFrag = new OrderPaymentWebView();
                            Bundle bundle = new Bundle();
                            bundle.putString("order_id", response.body().orderId);
                            bundle.putString("url", response.body()._3dsUrl);
                            bundle.putString("totalvalue",amounttext.getText().toString().split(" ")[1]);
                            bundle.putString("coupencode",coupencode);
                            prFrag.setArguments(bundle);

                            getFragmentManager()
                                    .beginTransaction().addToBackStack(null)
                                    .add(R.id.cartcontainer, prFrag)
                                    .commit();


                        }

                    } else {
                        // openNavigatetoCartdialog(response.body().data.toString());
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();

                    }


                }


            }


            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                Toast.makeText(getContext(), "برجاء فحص بيانات البطاقة", Toast.LENGTH_SHORT).show();
                //  openNavigatetoCartdialog("بعض المنتجات غير متوفرة بالمخزون");

                //Toast.makeText(getContext(), "Shipping method is not available for this address.", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void getCartData() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("mask_key", "");
        query.put("cart_id", "");

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {


                int quant;

                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();

                    if (response.body().msg.equalsIgnoreCase("success")) {
                        //  parentDialogListener.hideProgressDialog();

                        getdata = response.body().data;
                        getWalletHistory();

                    }


                }
            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                //   Toast.makeText(getContext(), "No Items Found", Toast.LENGTH_LONG).show();

            }
        });


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Toast.makeText(getContext(),"dsfdsfds",Toast.LENGTH_SHORT).show();
        try {

            parentDialogListener = (ParentDialogsListener) activity;

        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }

    public void getWalletHistory() {

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Map<String, String> map1 = new HashMap<>();
        map1.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));

        Call<WalletHistory> call = apiService.getwallethistory(map1);
        call.enqueue(new Callback<WalletHistory>() {
            @Override
            public void onResponse(Call<WalletHistory> call, Response<WalletHistory> response) {


                if (response.body() != null) {
                    getRememberCardName();
                    parentDialogListener.hideProgressDialog();

                    freshwalletAmount =  response.body().data.walletAmount;

                    if(freshwalletAmount==0){

                        walletcheckbox.setEnabled(false);


                    }
                    else{
                        walletcheckbox.setEnabled(true);


                    }
                    MySharedPreferenceClass.setWalletAmount(getContext(), String.valueOf(freshwalletAmount));

                    walletamount.setText("SAR" + " " + MySharedPreferenceClass.getWalletamount(getContext()) + " " + "- رصيد حسابي");

                    leftwalletamount.setText("SAR" + " " + freshwalletAmount + " " + "الرصيد المتبقي بحسابكم:");


                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<WalletHistory> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void getRememberCardName() {


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();


        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<RememberCardName> call = apiService.getremembercards(query);
        call.enqueue(new Callback<RememberCardName>() {
            @Override
            public void onResponse(Call<RememberCardName> call, Response<RememberCardName> response) {


                if (response.body() != null) {

                    //hideProgressDialog();

                    if (response.body().status.equals("success")) {


                        carddetail = response.body().data;


                        initSavedCardRecycler();


                    }


                }

            }


            @Override
            public void onFailure(Call<RememberCardName> call, Throwable t) {

                //progressDialog.hide();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void initSavedCardRecycler() {
        cardrecycler = view.findViewById(R.id.cardrecycler);

        cardrecycler.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));


        cardrecycler.setItemAnimator(new DefaultItemAnimator());
        // use a linear layout manager


        // specify an adapter (see also next example)
        cardAdapter = new RememberCardAdapter(getContext(), carddetail, selectedcardposition, new ViewListener3() {
            @Override
            public void onClick(int position, View view, String cvv) {
                int id = view.getId();
                savedcardcvv = cvv;
                tokenname = carddetail.get(position).tokenName;

                selectedcardposition = position;


                switch (id) {

                    case R.id.remembercardclick://button for message


                        if (walletaddflag == true && amounttext.getText().toString().split(" ")[1].equals("0") && selectpaymentflag != 3) {
                            //Toast.makeText(getContext(),walletaddflag+"first",Toast.LENGTH_SHORT).show();
                            selectedcardposition = position;
                            walletamount.setText("SAR" + " " + MySharedPreferenceClass.getWalletamount(getContext()) + " " + "- رصيد حسابي");

                            cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                            codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));

                            walletcheckbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));

                            // initSavedCardRecycler();
                            walletaddflag = false;
                            is_partialpayment = false;

                        }
                        if (selectpaymentflag != 3) {


                            if (!walletaddflag) {

                                selectedcardposition = position;
                                //  initSavedCardRecycler();
                                cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                                codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                                amounttext.setText("SAR" + " " + amount + " " + "الاجمالي");
                                // edt_card_detail.setVisibility(View.VISIBLE);
                                //cardrecyclerlayout.setVisibility(View.VISIBLE);
                                edt_card_detail.setVisibility(View.GONE);
                                selectpaymentflag = 3;
                            } else {
                                selectedcardposition = position;

                                cardradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                                codradiobtn.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                                int amount_afterwallet = Integer.parseInt(amount) - freshwalletAmount;
                                amounttext.setText("SAR" + " " + amount_afterwallet + " " + "الاجمالي");
                                //edt_card_detail.setVisibility(View.VISIBLE);
                                // cardrecyclerlayout.setVisibility(View.VISIBLE);
                                edt_card_detail.setVisibility(View.GONE);
                                selectpaymentflag = 3;

                                //initSavedCardRecycler();


                            }


                        }


                        //cardname.setText(carddetail.get(position).name);
                        //cardnumber.setText(carddetail.get(position).cardNumber);


                        break;
                }
            }
        }


        );
        cardrecycler.setAdapter(cardAdapter);
        cardrecycler.setNestedScrollingEnabled(false);
    }


}