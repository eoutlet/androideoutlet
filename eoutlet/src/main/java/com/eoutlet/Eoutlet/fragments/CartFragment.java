package com.eoutlet.Eoutlet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.eoutlet.Eoutlet.PaymentGateway.PaymentActivity;
import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.adpters.CartAdpter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;


import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.intrface.UpdateBedgeCount;
import com.eoutlet.Eoutlet.listener.CartListener;
import com.eoutlet.Eoutlet.listener.ViewListener;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.ApplyCoupenResult;
import com.eoutlet.Eoutlet.pojo.CancelOrderListResponse;
import com.eoutlet.Eoutlet.pojo.DeleteitemMessage;

import com.eoutlet.Eoutlet.pojo.Size;
import com.eoutlet.Eoutlet.pojo.ViewCart1;
import com.eoutlet.Eoutlet.pojo.ViewCartData;

import com.eoutlet.Eoutlet.pojo.universalMessage2;
import androidx.appcompat.widget.Toolbar;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;



public class CartFragment extends Fragment implements CartListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    View v;
    UpdateBedgeCount updatefix;

    public ParentDialogsListener parentDialogListener;
    RecyclerView cartRecycler;
    CartAdpter mAdapter;
    private float totalprice;
    private float totalwithtax;
    private Toolbar toolbar, toolbar1;
    private int tax_rate_in_percentage, gift_wrap_fees = 0,codcharges;
    private LinearLayout removealllayout;
    private RelativeLayout removeall;
    static int  totalcount;

    private ImageView searchImage,backarrow;

    public static String seledtedItemid;

    public static int selcteditemquantity;
    private FrameLayout proceedtoCheckout;
    private TextView tvTotalprice, tvTotalPricewithTax, totalPricewithoutTax;
    private List<ViewCartData> cartData;


    public TextView plusbutton;
    private View toolbarbeg;

    private TextView noitemavail, tooltext;
    private TextView shippingcharges, totaltax;
    private List<String> itemId = new ArrayList<>();
    private List<String> skuList = new ArrayList<>();
    private List<Integer> quantityList = new ArrayList<>();
    private List<Size> options = new ArrayList<>();
    boolean islayoutvisible = false;
    private LinearLayout coupenlayout;
    boolean coupenflag = false;
    static int dicountamount = 0;
    private TextView coupantext, coupenAmount;
    private LinearLayout linCartMain;
    ExecuteFragment execute;
    private LinearLayout coupanmainlayout;
    private TextView applycode;
    private TextView viewGift, descriptiontext, giftwrappingcharge;
    private EditText edtcoupencode;
    private OnFragmentInteractionListener mListener;
    private ImageView closeimage, checkbox;
    private RelativeLayout giftboxadd;

   public static boolean checkboxflag = false;

    private FrameLayout giftimage;

    public CartFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        v = inflater.inflate(R.layout.fragment_cart_new, container, false);
        MainActivity.fromcart = true;
        execute = (MainActivity) getActivity();
        initViews(v);
        checkboxflag =  MySharedPreferenceClass.getCheckboxflag(getContext());


        toolbar1 = v.findViewById(R.id.toolbar);
        searchImage = toolbar1.findViewById(R.id.serachbar);


        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment prFrag = new SearchResultFragment();
                Bundle databund = new Bundle();


                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.profileContainer, prFrag)
                        .commit();



            }
        });



      /*  if (MySharedPreferenceClass.getBedgeCount(getContext()) > 0) {
            toolbar_bedgetextcart.setVisibility(View.VISIBLE);
            toolbar_bedgetextcart.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));
        }
*/

        return v;

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void setUserVisibleHint(boolean visible) {


        super.setUserVisibleHint(visible);

        if (visible && isResumed()) {

            checkboxflag =  MySharedPreferenceClass.getCheckboxflag(getContext());


         /*   if (MySharedPreferenceClass.getBedgeCount(getContext()) > 0) {
                toolbar_bedgetextcart.setVisibility(View.VISIBLE);
                toolbar_bedgetextcart.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(getContext())));
            } else {
                toolbar_bedgetextcart.setVisibility(View.GONE);

            }
*/
            searchImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment prFrag = new SearchResultFragment();
                    getFragmentManager()
                            .beginTransaction().addToBackStack(null)
                            .replace(R.id.cartcontainer, prFrag)
                            .commit();
                }
            });
            if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {
                getCartData();
            } else {
                getCartDataforGuestUser();
            }

        }
    }

    public void initViews(View v) {
        cartRecycler = v.findViewById(R.id.cart_recycler);
        tvTotalprice = v.findViewById(R.id.totalprice);
        tvTotalPricewithTax = v.findViewById(R.id.totalpricewithTax);

        proceedtoCheckout = v.findViewById(R.id.proceedtoCheckout);
        linCartMain = v.findViewById(R.id.cartmainlayout);
        noitemavail = v.findViewById(R.id.notItemAvail);
        shippingcharges = v.findViewById(R.id.shippingcharges);
        totaltax = v.findViewById(R.id.totalTax);
        totalPricewithoutTax = v.findViewById(R.id.totalpricewithouttax);
        coupanmainlayout = v.findViewById(R.id.coupanmainlayout);
        removealllayout = v.findViewById(R.id.removealllayout);
        removeall = v.findViewById(R.id.removeall);
        coupenlayout = v.findViewById(R.id.coupenlayout);
        applycode = v.findViewById(R.id.applycode);
        edtcoupencode = v.findViewById(R.id.edt_coupen);
        coupantext = v.findViewById(R.id.coupenname);
        coupenAmount = v.findViewById(R.id.discoutnamount);





        giftimage = v.findViewById(R.id.giftImage);
        closeimage = v.findViewById(R.id.clooseImage);
        viewGift = v.findViewById(R.id.btnviewgift);
        descriptiontext = v.findViewById(R.id.destext);
        checkbox = v.findViewById(R.id.checkbox);
        plusbutton = v.findViewById(R.id.plusbutton);
        giftwrappingcharge = v.findViewById(R.id.giftwrappingcharge);
        giftboxadd = v.findViewById(R.id.giftboxadd);




      giftboxadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!MySharedPreferenceClass.getCheckboxflag(getContext())) {
                    checkbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                    MySharedPreferenceClass.setCheckbofflag(getContext(),true);
                    checkboxflag =  MySharedPreferenceClass.getCheckboxflag(getContext());
                    giftwrappingcharge.setText("SAR" + " " + gift_wrap_fees);
                    int pricewithgift = Integer.parseInt(tvTotalPricewithTax.getText().toString().split(" ")[1]) + Integer.parseInt(giftwrappingcharge.getText().toString().split(" ")[1]);
                    tvTotalPricewithTax.setText("SAR" + " " + String.valueOf(pricewithgift));
                }
                else
                    {
                    checkbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                    MySharedPreferenceClass.setCheckbofflag(getContext(),false);
                    checkboxflag =  MySharedPreferenceClass.getCheckboxflag(getContext());
                    int pricewithgift = Integer.parseInt(tvTotalPricewithTax.getText().toString().split(" ")[1]) - Integer.parseInt(giftwrappingcharge.getText().toString().split(" ")[1]);
                    tvTotalPricewithTax.setText("SAR" + " " + String.valueOf(pricewithgift));
                    giftwrappingcharge.setText("SAR" + " " + "0");
                }


            }
        });

        closeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewGift.setVisibility(View.VISIBLE);
                giftimage.setVisibility(View.GONE);
            }
        });

        viewGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                DialogFragment prFrag = new GiftboxFragment();
                //DialogFragment prFrag = new SuccessThankyouFragment();
                Bundle bund = new Bundle();


                prFrag.setArguments(bund);

                prFrag.setTargetFragment(CartFragment.this, 1002);
                prFrag.show(getFragmentManager(), "signup");


            }
        });


        coupanmainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!islayoutvisible) {


                    Animation animShow = AnimationUtils.loadAnimation(getContext(), R.anim.view_show);


                    //coupenlayout.startAnimation( animShow );
                    coupenlayout.setVisibility(View.VISIBLE);
                    coupenlayout.requestFocus();


// Start the animation

                    islayoutvisible = true;

                    plusbutton.setText("-");


                } else {


                    Animation animHide = AnimationUtils.loadAnimation(getContext(), R.anim.view_hide);
                    coupenlayout.setVisibility(View.GONE);


                    islayoutvisible = false;
                    plusbutton.setText("+");

                }
            }
        });


        removeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                openRemovealldialog();


            }
        });


        applycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edtcoupencode.getText().toString().equals("")) {


                    setApplycode();


                } else {
                    Toast.makeText(getContext(), "من فضلك ادخل كود الرصيد بشكل صحيح", Toast.LENGTH_SHORT).show();

                }
            }
        });


       /* searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment prFrag = new SearchResultFragment();
                Bundle databund = new Bundle();


                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.cartcontainer, prFrag)
                        .commit();



            }
        });
        toolbarbeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserVisibleHint(true);

            }
        });*/

        updatefix = (MainActivity) getActivity();
        noitemavail.setVisibility(View.GONE);


        proceedtoCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (MySharedPreferenceClass.getMyUserId(getContext()) == " ") {
                    getCartDataStatusforGuestUser();
                } else if (MySharedPreferenceClass.getEmail(getContext()) == null) {
                    openSignindialog();
                } else {
                    getStatusofCartItems();

                }


            }

        });


    }



    public void initRecycler(View v) {
        cartRecycler.setHasFixedSize(true);

        if (cartData.size() == 1) {

            final float scale = getResources().getDisplayMetrics().density;

            int dpHeightInPx = (int) (R.dimen.product_height2 * scale);


            LinearLayout.LayoutParams lp =
                    new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, dpHeightInPx);
            lp.leftMargin = 0;


            cartRecycler.setPadding(6, 0, 6, 0);

            cartRecycler.setLayoutParams(lp);


            cartRecycler.getLayoutParams().height = (int) getResources().getDimension(R.dimen.scroller_height);


            cartRecycler.setVerticalScrollBarEnabled(false);


        } else if (cartData.size() == 2) {

            final float scale = getResources().getDisplayMetrics().density;

            int dpHeightInPx = (int) (R.dimen.product_height2 * scale);

            LinearLayout.LayoutParams lp =
                    new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 2 * dpHeightInPx);
            lp.leftMargin = 0;
            cartRecycler.setPadding(6, 0, 6, 0);
            cartRecycler.setLayoutParams(lp);

            cartRecycler.getLayoutParams().height = (int) getResources().getDimension(R.dimen.twoitemheight);
            cartRecycler.setVerticalScrollBarEnabled(false);
        }



        else if(cartData.size()==3) {

            final float scale = getResources().getDisplayMetrics().density;

            int dpHeightInPx = (int) (R.dimen.product_height2 * scale);

            LinearLayout.LayoutParams lp =
                    new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 3 * dpHeightInPx);

            cartRecycler.setPadding(0, 0, 6, 0);
            lp.leftMargin = 0;
            cartRecycler.setLayoutParams(lp);
            cartRecycler.getLayoutParams().height = (int) getResources().getDimension(R.dimen.product_height3);
            cartRecycler.setVerticalScrollBarEnabled(false);


        }
        else{

            final float scale = getResources().getDisplayMetrics().density;

            int dpHeightInPx = (int) (R.dimen.product_height2 * scale);

            LinearLayout.LayoutParams lp =
                    new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 3 * dpHeightInPx+20);

            cartRecycler.setPadding(20, 0, 6, 0);
            lp.leftMargin = 0;
            cartRecycler.setLayoutParams(lp);
            cartRecycler.getLayoutParams().height = (int) getResources().getDimension(R.dimen.product_height3);
            cartRecycler.setVerticalScrollBarEnabled(true);



        }
        cartRecycler.setLayoutManager(new LinearLayoutManager(v.getContext()));

        // specify an adapter (see also next example)
        mAdapter = new CartAdpter(v.getContext(), cartData

                , new ViewListener() {
            @Override
            public void onClick(int position, View view) {
                int id = view.getId();


                switch (id) {

                    case R.id.deleteItemfromCart://button for message

                        seledtedItemid = itemId.get(position);
                        selcteditemquantity = quantityList.get(position);
                        dicountamount = 0;

                        openDeletedialog();


                        break;


                    case R.id.cart_plus_button:
                        dicountamount = 0;
                        seledtedItemid = itemId.get(position);
                        edtcoupencode.setText("");
                        coupantext.setText("الخصم");
                        coupenAmount.setText("- SAR 0");
                        edtcoupencode.setEnabled(true);


                        if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {


                            changequantity(CartAdpter.qtyarray.get(position).toString(), position, (Integer) CartAdpter.qtyarray.get(position) + 1);

                        } else {


                            changequantityforguestuser(CartAdpter.qtyarray.get(position).toString(), position, (Integer) CartAdpter.qtyarray.get(position) + 1);

                        }
                        break;
                    case R.id.mainitemclick:
                        Fragment prFrag = new ProductDetail();
                        Bundle databund = new Bundle();

                        databund.putString("sku", cartData.get(position).productData.sku);
                        databund.putString("type", cartData.get(position).productData.type);
                        databund.putString("pid", cartData.get(position).productData.id);
                        databund.putString("size", cartData.get(position).productData.size);
                        databund.putString("color",cartData.get(position).productData.color);
                        databund.putString("color_name",  cartData.get(position).productData.color_name);
                        databund.putSerializable("catagoryobject",cartData.get(position).productData);


                        prFrag.setArguments(databund);
                        getFragmentManager()
                                .beginTransaction().addToBackStack(null)
                                .replace(R.id.cartcontainer, prFrag)
                                .commit();






                        break;

                    case R.id.cart_minace_button:
                        seledtedItemid = itemId.get(position);
                        seledtedItemid = itemId.get(position);
                        edtcoupencode.setText("");
                        coupantext.setText("الخصم");
                        coupenAmount.setText("- SAR 0");
                        edtcoupencode.setEnabled(true);
                        dicountamount = 0;

                        if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {


                            if ((Integer) CartAdpter.qtyarray.get(position) > 1) {

                                changequantity(CartAdpter.qtyarray.get(position).toString(), position, (Integer) CartAdpter.qtyarray.get(position) - 1);
                            } else {


                                deleteItemfromCart();


                            }


                        } else {
                            if ((Integer) CartAdpter.qtyarray.get(position) > 1) {
                                changequantityforguestuser(CartAdpter.qtyarray.get(position).toString(), position, (Integer) CartAdpter.qtyarray.get(position) - 1);
                            } else {

                                deleteItemfromCartforguestUser();


                            }
                        }
                        break;


                }
            }
        }


        );
        cartRecycler.setAdapter(mAdapter);


    }


    public void changequantity(String quantity, final int position, final int changequnatity) {

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("item_id", seledtedItemid);
        query.put("qty", String.valueOf(changequnatity));
        query.put("sku", skuList.get(position));

        Call<universalMessage2> call = apiService.changequantityfromCart(query);
        call.enqueue(new Callback<universalMessage2>() {
            @Override
            public void onResponse(Call<universalMessage2> call, Response<universalMessage2> response) {


                if (response.body() != null) {


                    if (response.body().msg.equals("success")) {
                        CartAdpter.qtyarray.set(position, changequnatity);
                        parentDialogListener.hideProgressDialog();
                        getCartData();

                    } else {
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), response.body().data.toString(), Toast.LENGTH_LONG).show();


                    }


                }


            }


            @Override
            public void onFailure(Call<universalMessage2> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();

            }
        });


    }


    public void setApplycode() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();

        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("coupon", edtcoupencode.getText().toString());
        query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));
        query.put("mask_key", String.valueOf(MySharedPreferenceClass.getMaskkey(getContext())));
        query.put("subtotal", totalPricewithoutTax.getText().toString().split(" ")[1]);
        query.put("shippingcharge", "0");

        Call<ApplyCoupenResult> call = apiService.applycode(query);
        call.enqueue(new Callback<ApplyCoupenResult>() {
            @Override
            public void onResponse(Call<ApplyCoupenResult> call, Response<ApplyCoupenResult> response) {


                if (response.body() != null) {


                    parentDialogListener.hideProgressDialog();


                    dicountamount = Integer.parseInt(response.body().discountAmount);
                    System.out.println("discountamt....." + response.body().discountAmount);
                    int amoutaftercoupenapplied = Integer.parseInt(totalPricewithoutTax.getText().toString().split(" ")[1]) - dicountamount + Integer.parseInt(totaltax.getText().toString().split(" ")[1]);
                    System.out.println("amountaftercoupan applied....." + amoutaftercoupenapplied);

                    if (amoutaftercoupenapplied >= 0) {
                        coupenAmount.setText("" + "-" + " " + "SAR" + " " + response.body().discountAmount);
                        coupantext.setText("(" + edtcoupencode.getText().toString() + ")" + " " + "الخصم");
                        tvTotalPricewithTax.setText("SAR" + " " + amoutaftercoupenapplied);

                        applycode.setText("إلغاء القسيمة");
                        edtcoupencode.setEnabled(false);

                        coupenflag = true;
                    } else {
                        int amtwithoutcoupanapplied = Integer.parseInt(totalPricewithoutTax.getText().toString().split(" ")[1]) - dicountamount + Integer.parseInt(totaltax.getText().toString().split(" ")[1]);


                        tvTotalPricewithTax.setText("SAR" + " " + amoutaftercoupenapplied);

                    }


                }
                else {
                    parentDialogListener.hideProgressDialog();


                }

            }


            @Override
            public void onFailure(Call<ApplyCoupenResult> call, Throwable t) {
                parentDialogListener.hideProgressDialog();

                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void changequantityforguestuser(String quantity, final int position, final int changequantity) {

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", " ");
        query.put("item_id", seledtedItemid);
        query.put("mask_key", MySharedPreferenceClass.getMaskkey(getContext()));
        query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));
        query.put("qty", String.valueOf(changequantity));
        query.put("sku", skuList.get(position));

        Call<universalMessage2> call = apiService.changequantityfromCart(query);
        call.enqueue(new Callback<universalMessage2>() {
            @Override
            public void onResponse(Call<universalMessage2> call, Response<universalMessage2> response) {


                if (response.body() != null) {


                    if (response.body().msg.equals("success")) {
                        parentDialogListener.hideProgressDialog();
                        getCartDataforGuestUser();
                        CartAdpter.qtyarray.set(position, changequantity);


                    } else {
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), response.body().data.toString(), Toast.LENGTH_LONG).show();


                    }


                }


            }


            @Override
            public void onFailure(Call<universalMessage2> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();

            }
        });


    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {

        }
    }

    public void deleteItemfromCart() {

        edtcoupencode.setText("");
        coupantext.setText("الخصم");
        coupenAmount.setText("- SAR 0");
        edtcoupencode.setEnabled(true);

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("item_id", seledtedItemid);

        Call<DeleteitemMessage> call = apiService.deleteItemfromCart(query);
        call.enqueue(new Callback<DeleteitemMessage>() {
            @Override
            public void onResponse(Call<DeleteitemMessage> call, Response<DeleteitemMessage> response) {


                if (response.body() != null) {


                    if (response.body().msg.equals("success")) {
                        parentDialogListener.hideProgressDialog();
                        getCartData();


                    } else {
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), "لا يوجد لديك عناصر في سلة التسوق الخاصة بك", Toast.LENGTH_LONG).show();
                        linCartMain.setVisibility(View.GONE);
                        proceedtoCheckout.setVisibility(View.GONE);
                        removealllayout.setVisibility(View.GONE);


                    }


                }


            }


            @Override
            public void onFailure(Call<DeleteitemMessage> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
                linCartMain.setVisibility(View.GONE);
                proceedtoCheckout.setVisibility(View.GONE);
                removealllayout.setVisibility(View.GONE);
            }
        });


    }

    public void deleteItemfromCartforguestUser() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", " ");
        query.put("item_id", seledtedItemid);
        query.put("mask_key", MySharedPreferenceClass.getMaskkey(getContext()));
        query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));


        Call<DeleteitemMessage> call = apiService.deleteItemfromCart(query);
        call.enqueue(new Callback<DeleteitemMessage>() {
            @Override
            public void onResponse(Call<DeleteitemMessage> call, Response<DeleteitemMessage> response) {


                if (response.body() != null) {


                    if (response.body().msg.equals("success")) {
                        parentDialogListener.hideProgressDialog();


                        getCartDataforGuestUser();


                    } else {
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), "لا يوجد لديك عناصر في سلة التسوق الخاصة بك", Toast.LENGTH_LONG).show();
                        linCartMain.setVisibility(View.GONE);
                        proceedtoCheckout.setVisibility(View.GONE);
                        removealllayout.setVisibility(View.GONE);

                    }


                }


            }


            @Override
            public void onFailure(Call<DeleteitemMessage> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                linCartMain.setVisibility(View.GONE);
                proceedtoCheckout.setVisibility(View.GONE);
                removealllayout.setVisibility(View.GONE);
            }
        });


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            parentDialogListener = (ParentDialogsListener) activity;

        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getCartData() {
        noitemavail.setVisibility(View.GONE);
        totalcount = 0;
        cartData = new ArrayList<>();
        itemId.clear();
        cartData.clear();
        totalprice = 0;
        totalwithtax = 0;
        quantityList.clear();
        options.clear();
        skuList.clear();


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("mask_key", "");
        query.put("cart_id", "");

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {


                int quant;

                if (response.body() != null) {


                    if (response.body().msg.equalsIgnoreCase("success")) {


                        cartData = response.body().data;

                      if(response.body().gift_wrap_fee!=null) {
                          gift_wrap_fees = Integer.parseInt(response.body().gift_wrap_fee);
                      }

                        for (int i = 0; i < response.body().data.size(); i++) {
                            if (response.body().data.get(i).qty instanceof String) {
                                totalcount = totalcount + (int) Float.parseFloat((String) response.body().data.get(i).qty);
                                quant = (int) Float.parseFloat((String) response.body().data.get(i).qty);
                                quantityList.add(quant);
                            } else {
                                totalcount = totalcount + (int) (response.body().data.get(i).qty);

                                quant = (int) response.body().data.get(i).qty;

                                quantityList.add(quant);
                            }


                            if (quant == 1) {

                                totalprice = totalprice + Float.parseFloat(response.body().data.get(i).price);
                            } else {

                                totalprice = totalprice + Float.parseFloat(response.body().data.get(i).price) * quant;


                            }


                            itemId.add(response.body().data.get(i).id);
                            skuList.add(response.body().data.get(i).sku);
                            tax_rate_in_percentage = response.body().goodstax_rate;


                        }

                        try {
                            linCartMain.setVisibility(View.VISIBLE);
                            proceedtoCheckout.setVisibility(View.VISIBLE);
                            removealllayout.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        initRecycler(v);

                        totalPricewithoutTax.setText("SAR" + " " + response.body().subtotal);

                        shippingcharges.setText("SAR" + " " + response.body().shipping);
                        totaltax.setText("SAR" + " " + response.body().tax);



                        int amtwithoutcoupanapplied = Integer.parseInt(response.body().subtotal) - dicountamount;  /*Integer.parseInt(response.body().tax)*/


                        tvTotalPricewithTax.setText("SAR" + " " + amtwithoutcoupanapplied);

                        MySharedPreferenceClass.setBedgeCount(getContext(), totalcount);
                        updatefix.updateBedgeCount();
                        MainActivity.notificationBadge.setVisibility(View.VISIBLE);

                        parentDialogListener.hideProgressDialog();


                        if (MySharedPreferenceClass.getCheckboxflag(getContext())) {
                            int totalwithgiftwrapping = Integer.parseInt(totalPricewithoutTax.getText().toString().split(" ")[1]) + gift_wrap_fees;
                            checkbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                            giftwrappingcharge.setText("SAR" + " " + response.body().gift_wrap_fee);
                            tvTotalPricewithTax.setText("SAR" + " " + totalwithgiftwrapping);
                        }
                        else {

                            tvTotalPricewithTax.setText("SAR" + " " + response.body().subtotal);
                            checkbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                            giftwrappingcharge.setText("SAR" + " " + "0");

                        }

                    } else {

                        parentDialogListener.hideProgressDialog();
                        MainActivity.notificationBadge.setVisibility(View.GONE);
                        MainActivity.bedgetext.setVisibility(View.GONE);
                        MainActivity.toolbar_bedgetext.setVisibility(View.GONE);
                        linCartMain.setVisibility(View.GONE);
                        removealllayout.setVisibility(View.GONE);
                        noitemavail.setVisibility(View.VISIBLE);

                        proceedtoCheckout.setVisibility(View.GONE);

                        MySharedPreferenceClass.setBedgeCount(getContext(), 0);
                        Toast.makeText(getContext(), "لا يوجد لديك عناصر في سلة التسوق الخاصة بك"
                                , Toast.LENGTH_LONG).show();

                    }


                }


            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                MainActivity.notificationBadge.setVisibility(View.GONE);
                MainActivity.bedgetext.setVisibility(View.GONE);
                MainActivity.toolbar_bedgetext.setVisibility(View.GONE);

                linCartMain.setVisibility(View.GONE);
                proceedtoCheckout.setVisibility(View.GONE);
                removealllayout.setVisibility(View.GONE);
            }
        });


    }


    public void getStatusofCartItems() {

        noitemavail.setVisibility(View.GONE);
        totalcount = 0;
        cartData = new ArrayList<>();
        itemId.clear();
        cartData.clear();
        totalprice = 0;
        totalwithtax = 0;
        quantityList.clear();
        options.clear();
        skuList.clear();


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("mask_key", "");
        query.put("cart_id", "");

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {


                int quant;

                if (response.body() != null) {


                    if (response.body().msg.equalsIgnoreCase("success")) {


                        cartData = response.body().data;


                        for (int i = 0; i < response.body().data.size(); i++) {
                            if (response.body().data.get(i).qty instanceof String) {
                                totalcount = totalcount + (int) Float.parseFloat((String) response.body().data.get(i).qty);
                                quant = (int) Float.parseFloat((String) response.body().data.get(i).qty);
                                quantityList.add(quant);
                            } else {
                                totalcount = totalcount + (int) (response.body().data.get(i).qty);

                                quant = (int) response.body().data.get(i).qty;

                                quantityList.add(quant);
                            }


                            if (quant == 1) {

                                totalprice = totalprice + Float.parseFloat(response.body().data.get(i).price);
                            } else {

                                totalprice = totalprice + Float.parseFloat(response.body().data.get(i).price) * quant;


                            }


                            itemId.add(response.body().data.get(i).id);
                            skuList.add(response.body().data.get(i).sku);
                            tax_rate_in_percentage = response.body().goodstax_rate;


                        }

                        try {
                            linCartMain.setVisibility(View.VISIBLE);
                            proceedtoCheckout.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        initRecycler(v);

                        totalPricewithoutTax.setText("SAR" + " " + response.body().subtotal);

                        shippingcharges.setText("SAR" + " " + response.body().shipping);
                        totaltax.setText("SAR" + " " + response.body().tax);
                        codcharges = response.body().cod_charges;
                        //tvTotalPricewithTax.setText("SAR" + " " + response.body().grandtotal);
                        //  tvTotalPricewithTax.setText("SAR" + " " + response.body().subtotal);



                        MySharedPreferenceClass.setBedgeCount(getContext(), totalcount);
                        updatefix.updateBedgeCount();
                        MainActivity.notificationBadge.setVisibility(View.VISIBLE);

                        parentDialogListener.hideProgressDialog();

                        gift_wrap_fees = Integer.parseInt(response.body().gift_wrap_fee);



                    } else {

                        parentDialogListener.hideProgressDialog();
                        MainActivity.notificationBadge.setVisibility(View.GONE);
                        MainActivity.bedgetext.setVisibility(View.GONE);

                        MainActivity.toolbar_bedgetext.setVisibility(View.GONE);
                        linCartMain.setVisibility(View.GONE);
                        noitemavail.setVisibility(View.VISIBLE);

                        proceedtoCheckout.setVisibility(View.GONE);

                        MySharedPreferenceClass.setBedgeCount(getContext(), 0);
                        Toast.makeText(getContext(), "لا يوجد لديك عناصر في سلة التسوق الخاصة بك"
                                , Toast.LENGTH_LONG).show();

                    }


                }


                for (int i = 0; i < response.body().data.size(); i++) {

                    if (response.body().data.get(i).instock == 0) {

                        Toast.makeText(getContext(), "بعض المنتجات غير متوفرة بالمخزون", Toast.LENGTH_SHORT).show();

                        return;

                    }


                }

                Fragment prFrag = new CheckOutConfirmationfragment(CartFragment.this);
                Bundle bundle = new Bundle();
                bundle.putString("coupencode", edtcoupencode.getText().toString().trim());
                System.out.println("coupencode" + edtcoupencode.getText().toString().trim());
                bundle.putString("discounntamount", String.valueOf(dicountamount));
                System.out.println("discounntamount" + String.valueOf(dicountamount));

                bundle.putString("giftwrapping", giftwrappingcharge.getText().toString());
                bundle.putString("cod_label",response.body().cod_label);




                prFrag.setArguments(bundle);

                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.cartcontainer, prFrag)
                        .commit();


            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                MainActivity.notificationBadge.setVisibility(View.GONE);
                MainActivity.bedgetext.setVisibility(View.GONE);
                MainActivity.toolbar_bedgetext.setVisibility(View.GONE);
                linCartMain.setVisibility(View.GONE);
                proceedtoCheckout.setVisibility(View.GONE);
                removealllayout.setVisibility(View.GONE);
            }
        });


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // getCartData();
        System.out.println("I am on attach Fragment....");
    }

    @Override
    public void setUserHintcall() {
        if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {


            getCartData();


        } else {

            getCartDataforGuestUser();


        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void openRemovealldialog() {


        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage("هل انت متأكد من حذف كل القطع بالسلة؟");


        alertDialogBuilder.setPositiveButton("موافق",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        removeallitems();

                    }
                });

        alertDialogBuilder.setNegativeButton("إلغاء", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {


            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

    }

    public void openDeletedialog() {
        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage("هل أنت متأكد من إلغاء هذا العنصر من سلة التسوق؟");


        alertDialogBuilder.setPositiveButton("موافق",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {
                            deleteItemfromCart();
                        } else {
                            deleteItemfromCartforguestUser();
                        }
                    }
                });

        alertDialogBuilder.setNegativeButton("إلغاء", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {


            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void getCartDataStatusforGuestUser() {
        noitemavail.setVisibility(View.GONE);
        totalcount = 0;
        cartData = new ArrayList<>();
        itemId.clear();
        cartData.clear();
        totalprice = 0;
        totalwithtax = 0;
        quantityList.clear();
        skuList.clear();


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", " ");
        query.put("mask_key", MySharedPreferenceClass.getMaskkey(getContext()));
        query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {


                int quant;

                if (response.body() != null) {


                    if (response.body().msg.equals("success")) {

                        cartData = response.body().data;

                        for (int i = 0; i < response.body().data.size(); i++) {

                            if (response.body().data.get(i).qty instanceof String) {
                                totalcount = totalcount + (int) Float.parseFloat((String) response.body().data.get(i).qty);

                                quant = (int) Float.parseFloat((String) response.body().data.get(i).qty);
                                quantityList.add(quant);
                            } else {

                                quant = (int) response.body().data.get(i).qty;
                                quantityList.add(quant);
                                totalcount = totalcount + (int) (response.body().data.get(i).qty);
                            }


                            if (quant == 1) {

                                totalprice = totalprice + Float.parseFloat(response.body().data.get(i).price);
                            } else {

                                totalprice = totalprice + Float.parseFloat(response.body().data.get(i).price) * quant;


                            }


                            itemId.add(response.body().data.get(i).id);
                            skuList.add(response.body().data.get(i).sku);

                        }

                        linCartMain.setVisibility(View.VISIBLE);


                        proceedtoCheckout.setVisibility(View.VISIBLE);

                        initRecycler(v);


                        totalPricewithoutTax.setText("SAR" + " " + response.body().subtotal);

                        shippingcharges.setText("SAR" + " " + response.body().shipping);
                        totaltax.setText("SAR" + " " + response.body().tax);
                        //tvTotalPricewithTax.setText("SAR" + " " + response.body().grandtotal);
                        tvTotalPricewithTax.setText("SAR" + " " + response.body().subtotal);

                        codcharges = response.body().cod_charges;

                        MySharedPreferenceClass.setBedgeCount(getContext(), totalcount);
                        updatefix.updateBedgeCount();
                        MainActivity.notificationBadge.setVisibility(View.VISIBLE);
                        parentDialogListener.hideProgressDialog();

                        if (MySharedPreferenceClass.getCheckboxflag(getContext())) {
                            int totalwithgiftwrapping = Integer.parseInt(totalPricewithoutTax.getText().toString().split(" ")[1]) + gift_wrap_fees;

                            tvTotalPricewithTax.setText("SAR" + " " + totalwithgiftwrapping);
                        }
                        else {

                            tvTotalPricewithTax.setText("SAR" + " " + response.body().subtotal);
                            checkbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                            giftwrappingcharge.setText("SAR" + " " + "0");

                        }


                    } else {
                        parentDialogListener.hideProgressDialog();
                        linCartMain.setVisibility(View.GONE);
                        noitemavail.setVisibility(View.VISIBLE);
                        proceedtoCheckout.setVisibility(View.GONE);
                        MainActivity.notificationBadge.setVisibility(View.GONE);
                        MainActivity.bedgetext.setVisibility(View.GONE);
                        MySharedPreferenceClass.setBedgeCount(getContext(), 0);
                        Toast.makeText(getContext(), "لا يوجد لديك عناصر في سلة التسوق الخاصة بك.", Toast.LENGTH_LONG).show();

                    }


                }


                for (int i = 0; i < response.body().data.size(); i++) {

                    if (response.body().data.get(i).instock == 0) {

                        Toast.makeText(getContext(), "بعض المنتجات غير متوفرة بالمخزون", Toast.LENGTH_SHORT).show();

                        return;

                    }


                }


                openSignindialog();

            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                MainActivity.notificationBadge.setVisibility(View.GONE);
                MainActivity.bedgetext.setVisibility(View.GONE);
                MainActivity.toolbar_bedgetext.setVisibility(View.GONE);
            }
        });


    }


    public void getCartDataforGuestUser() {
        noitemavail.setVisibility(View.GONE);
        totalcount = 0;
        cartData = new ArrayList<>();
        itemId.clear();
        cartData.clear();
        totalprice = 0;
        totalwithtax = 0;
        quantityList.clear();
        skuList.clear();


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", " ");
        query.put("mask_key", MySharedPreferenceClass.getMaskkey(getContext()));
        query.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {


                int quant;

                if (response.body() != null) {


                    if (response.body().msg.equals("success")) {

                        cartData = response.body().data;

                        for (int i = 0; i < response.body().data.size(); i++) {

                            if (response.body().data.get(i).qty instanceof String) {
                                totalcount = totalcount + (int) Float.parseFloat((String) response.body().data.get(i).qty);

                                quant = (int) Float.parseFloat((String) response.body().data.get(i).qty);
                                quantityList.add(quant);
                            } else {

                                quant = (int) response.body().data.get(i).qty;
                                quantityList.add(quant);
                                totalcount = totalcount + (int) (response.body().data.get(i).qty);
                            }


                            if (quant == 1) {

                                totalprice = totalprice + Float.parseFloat(response.body().data.get(i).price);
                            } else {

                                totalprice = totalprice + Float.parseFloat(response.body().data.get(i).price) * quant;


                            }


                            itemId.add(response.body().data.get(i).id);
                            skuList.add(response.body().data.get(i).sku);

                        }

                        linCartMain.setVisibility(View.VISIBLE);
                        proceedtoCheckout.setVisibility(View.VISIBLE);
                        initRecycler(v);


                        totalPricewithoutTax.setText("SAR" + " " + response.body().subtotal);

                        shippingcharges.setText("SAR" + " " + response.body().shipping);
                        totaltax.setText("SAR" + " " + response.body().tax);
                        int amtwithoutcoupanapplied = Integer.parseInt(response.body().subtotal) - dicountamount + 0;  /* Integer.parseInt(response.body().tax*/


                        tvTotalPricewithTax.setText("SAR" + " " + amtwithoutcoupanapplied);


                        codcharges = response.body().cod_charges;
                        gift_wrap_fees = Integer.parseInt(response.body().gift_wrap_fee);


                        if (MySharedPreferenceClass.getCheckboxflag(getContext())) {
                            int totalwithgiftwrapping = Integer.parseInt(totalPricewithoutTax.getText().toString().split(" ")[1]) + gift_wrap_fees;
                            checkbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_yellow_check));
                            giftwrappingcharge.setText("SAR" + " " + response.body().gift_wrap_fee);
                            tvTotalPricewithTax.setText("SAR" + " " + totalwithgiftwrapping);
                        }
                        else {

                            tvTotalPricewithTax.setText("SAR" + " " + response.body().subtotal);
                            checkbox.setImageDrawable(getContext().getDrawable(R.drawable.ic_circle));
                            giftwrappingcharge.setText("SAR" + " " + "0");

                        }





                        MySharedPreferenceClass.setBedgeCount(getContext(), totalcount);
                        updatefix.updateBedgeCount();
                        MainActivity.notificationBadge.setVisibility(View.VISIBLE);

                        parentDialogListener.hideProgressDialog();




/*
                        int pricewithgift = Integer.parseInt(tvTotalPricewithTax.getText().toString().split(" ")[1]) - 0;
                        tvTotalPricewithTax.setText("SAR" + " " + String.valueOf(pricewithgift));
                        giftwrappingcharge.setText("SAR" + " " + "0");*/


                    } else {
                        parentDialogListener.hideProgressDialog();

                        linCartMain.setVisibility(View.GONE);
                        noitemavail.setVisibility(View.VISIBLE);
                        proceedtoCheckout.setVisibility(View.GONE);
                        MainActivity.notificationBadge.setVisibility(View.GONE);
                        MainActivity.bedgetext.setVisibility(View.GONE);
                        MainActivity.toolbar_bedgetext.setVisibility(View.GONE);
                        MySharedPreferenceClass.setBedgeCount(getContext(), 0);
                        Toast.makeText(getContext(), "لا يوجد لديك عناصر في سلة التسوق الخاصة بك.", Toast.LENGTH_LONG).show();

                    }


                }


            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                MainActivity.notificationBadge.setVisibility(View.GONE);
                MainActivity.bedgetext.setVisibility(View.GONE);
                MainActivity.toolbar_bedgetext.setVisibility(View.GONE);
            }
        });


    }


    public void openSignindialog() {
        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage("يجب تسجيل الدخول أولا");


        alertDialogBuilder.setPositiveButton("نعم",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {


                        Fragment prFrag = new LoginNewFragment(CartFragment.this);
                        Bundle databund = new Bundle();
                        databund.putString("flag", "fromcart");

                        prFrag.setArguments(databund);


                        getFragmentManager()
                                .beginTransaction().addToBackStack(null)
                                .replace(R.id.cartcontainer, prFrag)
                                .commit();

                    }
                });

        alertDialogBuilder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {


            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void removeallitems() {


        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Map<String, String> map1 = new HashMap<>();


        if (!MySharedPreferenceClass.getMyUserId(getContext()).equals(" ")) {

            map1.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        } else {
            map1.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));
        }


        Call<CancelOrderListResponse> call = apiService.clearAllItems(map1);
        call.enqueue(new Callback<CancelOrderListResponse>() {
            @Override
            public void onResponse(Call<CancelOrderListResponse> call, Response<CancelOrderListResponse> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    if (response.body().getMsg().equals("success")) {


                        if (MySharedPreferenceClass.getMyUserId(getContext()) != " ") {


                            getCartData();


                        } else {

                            getCartDataforGuestUser();


                        }
                    }

                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<CancelOrderListResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });
    }


}




