package com.eoutlet.Eoutlet.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.crashlytics.android.Crashlytics;
import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.activities.MainActivity;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.customview.HalveticRegularEditText;
import com.eoutlet.Eoutlet.customview.MyRadioButton;
import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;
import com.eoutlet.Eoutlet.intrface.UpdateBedgeCount;
import com.eoutlet.Eoutlet.listener.CartListener;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.AddressList;
import com.eoutlet.Eoutlet.pojo.GetCountryCode;
import com.eoutlet.Eoutlet.pojo.LoginResponse;
import com.eoutlet.Eoutlet.pojo.OtpResponse;
import com.eoutlet.Eoutlet.pojo.ViewCart1;
import com.eoutlet.Eoutlet.pojo.ViewCartData;
import com.eoutlet.Eoutlet.utility.Util;
import com.google.firebase.iid.FirebaseInstanceId;
import com.payfort.fort.android.sdk.base.FortSdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class LoginNewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match


    Context mContext;


    private LinearLayout signUp, getotpui;
    private Spinner countryspinner;
    private ExecuteFragment execute;
    private String userName, passWord;
    private TextView login, forgotpassword;
    private EditText edtpassword, edtmobile;

    private View passwordivider;
    MyRadioButton btnmobile, btnemail,getotp;
    private boolean isAllConditionFulfilled;
    private Toolbar toolbar;
    int totalcount;
    UpdateBedgeCount updatefix;
    private int mobilevalidationlength;
    private int selctedposition = 0;
    LinearLayout mobileverify, emailview;
    private List<ViewCartData> cartData;

    private String rgistereduseremail = " ";
    private String registereduserpassword = " ";
    private List<String> name = new ArrayList<>();

    private List<String> street = new ArrayList<>();
    private List<String> city = new ArrayList<>();
    private List<String> country = new ArrayList<>();
    private List<String> phone = new ArrayList<>();
    private List<String> address_id = new ArrayList<>();
    private List<String> countryId = new ArrayList<>();
    private List<String> addressfirstname = new ArrayList<>();
    private List<String> addresslastname = new ArrayList<>();
    private String value;


    private List<String> countryname;
    private List<String> countryCode;
    private List<String> celcode;
    private List<String> placeholder;
    private String countryName, otp;

    private CartListener cartreference;
    String selectedmethod = "mobilewithotp";
    private TextView HelpSupport;
    public ParentDialogsListener parentDialogListener;
    public static final String PHONE_STATE_PERMISSION =
            Manifest.permission.READ_PHONE_STATE;
    private LinearLayout loginLayout;
    private HalveticRegularEditText userPassword, edtusername;



    public LoginNewFragment() {
        // Required empty public constructor
    }

    public void passData(Context context) {
        mContext = context;
        execute = (MainActivity) mContext;
    }

    // TODO: Rename and change types and number of parameters
    public static LoginNewFragment newInstance(String param1, String param2) {
        LoginNewFragment fragment = new LoginNewFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login_new, container, false);
        System.out.println("LoginNewFragment");

        intiViews(v);


        getcountryDetail();


        Bundle bundle = getArguments();


        if (bundle.containsKey("flag")) {
            try {

                value = getArguments().getString("flag");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (bundle != null) {
            if (bundle.containsKey("email") && bundle.containsKey("password")) {

                rgistereduseremail = getArguments().getString("email");
                registereduserpassword = getArguments().getString("password");
                loginProceed(rgistereduseremail, registereduserpassword, "0");
            }


        }


        return v;
    }

    public LoginNewFragment(CartFragment cl) {

        cartreference = cl;


    }

    public void intiViews(final View v) {
        signUp = v.findViewById(R.id.signup);
        edtusername = v.findViewById(R.id.userName);
        edtpassword = v.findViewById(R.id.password);
        login = v.findViewById(R.id.loginClick);
        forgotpassword = v.findViewById(R.id.forgotPassword);
        HelpSupport = v.findViewById(R.id.HelpSupport);
        mobileverify = v.findViewById(R.id.veryfybymobile);
        emailview = v.findViewById(R.id.emailView);

        btnmobile = v.findViewById(R.id.btnmobile);
        btnemail = v.findViewById(R.id.btnemail);

        getotpui = v.findViewById(R.id.getotpui);
        getotp = v.findViewById(R.id.getotp);

        countryspinner = v.findViewById(R.id.countrycodeSpinner);
        edtmobile = v.findViewById(R.id.edtmobilrnumber);
        passwordivider = v.findViewById(R.id.passworddivider);

        updatefix = (MainActivity) getActivity();
        View view = getActivity().findViewById(R.id.common_toolbar);
        loginLayout = v.findViewById(R.id.loginLayout);
        userPassword = v.findViewById(R.id.userPassword);
        getotp.setSelected(true);
        getotp.setChecked(true);

        getotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPassword.setVisibility(View.GONE);
                edtusername.setVisibility(View.GONE);

                edtpassword.setVisibility(View.GONE);
                passwordivider.setVisibility(View.GONE);
                mobileverify.setVisibility(View.VISIBLE);
                emailview.setVisibility(View.GONE);
                login.setText("الدخول برسالة نصية");
                selectedmethod = "mobilewithotp";


            }
        });
        btnemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnmobile.setText(R.string.mobilewithoutunderline);
                selectedmethod = "email";
                emailview.setVisibility(View.GONE);
                mobileverify.setVisibility(View.GONE);
                getotpui.setVisibility(View.GONE);
                passwordivider.setVisibility(View.GONE);
                edtpassword.setVisibility(View.GONE);
                loginLayout.setVisibility(View.VISIBLE);
                userPassword.setVisibility(View.VISIBLE);
                edtusername.setVisibility(View.VISIBLE);
                login.setText("الدخول بالايميل");

            }
        });


        btnmobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailview.setVisibility(View.GONE);
                mobileverify.setVisibility(View.VISIBLE);
              /*  userPassword.setVisibility(View.VISIBLE);
                getotpui.setVisibility(View.VISIBLE);*/
                /*passwordivider.setVisibility(View.VISIBLE);*/
                userPassword.setVisibility(View.VISIBLE);
                edtusername.setVisibility(View.GONE);
                selectedmethod = "mobile";
                login.setText("الدخول برقم الجوال");

            }
        });


        HelpSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment helpSupportFragment = new HelpSupportFragment();
                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.profileContainer, helpSupportFragment)
                        .commit();
            }
        });


        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment prFrag = new ForgotPassword();
                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(LoginNewFragment.this.getId(), prFrag)
                        .commit();


            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginValidation();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment prFrag = new Signup();
                Bundle bund = new Bundle();
                bund.putString("flag", value);
                prFrag.setArguments(bund);
                getFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(LoginNewFragment.this.getId(), prFrag)
                        .commit();


            }
        });


        // getcountryDetail();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void sendotp() {


        if (selectedmethod.equals("mobilewithotp")) {

            isAllConditionFulfilled = Util.checkTextViewValidation(edtmobile, "من فضلك أدخل رقم الجوال")
                    && Util.checkMobileValidation(edtmobile, mobilevalidationlength, "من فضلك أدخل رقم الجوال");
            ;
            if (isAllConditionFulfilled) {

                opengetOtpDialog();


            } else {

                if (Util.view_final != null) {

                    Util.view_final.requestFocus();
                }

                return;


            }


        }


    }

    public void opengetOtpDialog() {

        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        alertDialogBuilder.setMessage("التأكد من صحة رقم الجوال");
        //alertDialogBuilder.setTitle("التأكد من صحة رقم الجوال");


        alertDialogBuilder.setPositiveButton("الحصول على الكود",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {


                        sendLoginOtp();


                    }
                });

        alertDialogBuilder.setNegativeButton("لا", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {


            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();

        TextView messageView = (TextView) alertDialog.findViewById(android.R.id.message);
        messageView.setTypeface(null, Typeface.BOLD);
        messageView.setGravity(Gravity.CENTER);


        final Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        // LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        // positiveButtonLL.gravity = Gravity.CENTER;
        //positiveButton.setLayoutParams(positiveButtonLL);
        positiveButton.setGravity(Gravity.CENTER);

        final Button negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        negativeButton.setVisibility(View.GONE);


    }

    public void sendLoginOtp() {

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Map<String, String> map1 = new HashMap<>();
        map1.put("mobile", /*"+917906156955"*/celcode.get(selctedposition) + edtmobile.getText().toString());

        map1.put("resend", "0");


        Call<OtpResponse> call = apiService.sendotpforLogin(map1);
        call.enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().success) {
                        Toast.makeText(getContext(), "تم ارسال الكود بنجاح", Toast.LENGTH_SHORT).show();


                        DialogFragment prFrag = new LoginVerifyOtp();
                        Bundle bund = new Bundle();
                        bund.putString("mobile", celcode.get(selctedposition) + edtmobile.getText().toString());

                        prFrag.setArguments(bund);

                        prFrag.setTargetFragment(LoginNewFragment.this, 1002);
                        prFrag.show(getFragmentManager(), "signup");


                    } else {
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), response.body().msg.toString(), Toast.LENGTH_SHORT).show();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void loginValidation() {


        if (selectedmethod.equals("email")) {
            isAllConditionFulfilled = Util.checkTextViewValidation(edtusername, "الرجاء إدخال اسم المستخدم")
                    && Util.checkTextViewValidation(userPassword, "برجاء كتابة كلمة السر")
                    && Util.checkTextViewlengthValidation(userPassword, "", 16)

                    && Util.checkTextViewlengthValidation(userPassword, " ", 3)
            ;


            if (!isAllConditionFulfilled) {
                if (Util.view_final != null) {

                    Util.view_final.requestFocus();
                }


                return;
            }
            userName = edtusername.getText().toString();
            passWord = userPassword.getText().toString();

            loginProceed(userName, passWord, "0");
        } else if (selectedmethod.equals("mobile")) {


            isAllConditionFulfilled = Util.checkTextViewValidation(edtmobile, "من فضلك أدخل رقم الجوال")
                    && Util.checkMobileValidation(edtmobile, mobilevalidationlength, "من فضلك أدخل رقم الجوال") && Util.checkTextViewValidation(userPassword, "برجاء كتابة كلمة السر");

            if (isAllConditionFulfilled) {

                loginProceed(" ", userPassword.getText().toString(), "1");


            }

            else {

                if (Util.view_final != null) {

                    Util.view_final.requestFocus();
                }

                return;
            }


        }


        else if (selectedmethod.equals("mobilewithotp")) {


            sendotp();


        }


    }


    public void loginProceed(String username, String password, String logintype) {
        String serial;
        String android_id;
        String myKey;

        myKey = FortSdk.getDeviceId(getContext());


        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Todo Don't forget to ask the permission

            android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            myKey = android_id;
        } else {
            serial = Build.SERIAL;
            android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            myKey = serial + android_id;
        }*/

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map1 = new HashMap<>();
        map1.put("email", username);
        map1.put("login_type", logintype);
        map1.put("password", password);
        map1.put("device_type", "android");
        map1.put("device_id", myKey);

        if (celcode.size() > 0) {

            map1.put("mobile_number", celcode.get(selctedposition) + edtmobile.getText().toString());
        } else {

            map1.put("mobile_number", " ");

        }
        map1.put("cart_id", String.valueOf(MySharedPreferenceClass.getCartId(getContext())));
        map1.put("mask_key", MySharedPreferenceClass.getMaskkey(getContext()));

        // Call<LoginResponse> call = apiService.doLogin("http://dev.eoutlet.net/webservice/customerapi.php?password="+password+"&email="+username+"&mask_id="+MySharedPreferenceClass.getMaskkey(getContext())+"&cart_id="+String.valueOf(MySharedPreferenceClass.getCartId(getContext())));

        Call<LoginResponse> call = apiService.doLogin(map1);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                if (response.body() != null) {
                    //parentDialogListener.hideProgressDialog();


                    if (response.body().msg.equals("success")) {
                        hideSoftKeyboard(edtpassword);


                        MySharedPreferenceClass.setMyFirstNamePref(getContext(), response.body().data.get(0).fname);
                        MySharedPreferenceClass.setMyLastNamePref(getContext(), response.body().data.get(0).lname);
                        MySharedPreferenceClass.setMobileNumber(getContext(), response.body().data.get(0).mob);
                        MySharedPreferenceClass.setEmail(getContext(), response.body().data.get(0).email);
                        MySharedPreferenceClass.setMyUserId(getContext(), response.body().data.get(0).id);
                        MySharedPreferenceClass.setMyUserName(getContext(), response.body().data.get(0).fname + " " + response.body().data.get(0).lname);
                        //getAddressDetails();

                        logUser(response.body().data.get(0).id, response.body().data.get(0).email, response.body().data.get(0).fname);


                        appsflyer_event_login(response.body().data.get(0).id);


                        //  Crashlytics.getInstance().crash(); //Used to check the Crashlaytics


                        if (value != null && value.equals("fromdetail")) {

                            getActivity().onBackPressed();


                        } else {


                            getAddressDetails2();


                            //initProfileFragment();


                            //clearFragmentBackStack();

                        }
                        //DeviceRegister();


                    } else {
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getView().getContext(), response.body().message.toString() /*"لم تقم بتسجيل الدخول بشكل صحيح أو أن حسابك معطل مؤقتاً."*/, Toast.LENGTH_SHORT).show();



                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                System.out.println("ErrorApi...." + t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void DeviceRegister() {
        String serial;
        String android_id;
        String myKey;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Todo Don't forget to ask the permission

            android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            myKey = android_id;
        } else {
            serial = Build.SERIAL;
            android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            myKey = serial + android_id;
        }


        // String serial = Build.SERIAL;
           /*String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            String myKey = serial + android_id;
*/
        //  parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        HashMap<String, String> map1 = new HashMap<>();
        map1.put("devicetype", "android");
        map1.put("fcm_token", FirebaseInstanceId.getInstance().getToken());
        map1.put("device_id", myKey);

        Call<LoginResponse> call = apiService.deviceregister(map1);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().msg.equals("success")) {

                        parentDialogListener.hideProgressDialog();


                    } else {
                        parentDialogListener.hideProgressDialog();
                        Toast.makeText(getContext(), "لم تقم بتسجيل الدخول بشكل صحيح أو أن حسابك معطل مؤقتاً.", Toast.LENGTH_SHORT).show();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                System.out.println("ErrorApi...." + t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void initProfileFragment() {

        //Log.e("From inside init profile method"," d;jfdlshjfkjsdfhk");


        if (!registereduserpassword.equals(" ") && !registereduserpassword.equals(" ")) {

            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }


        Fragment prFrag = new NewProfileFragment();
        getFragmentManager()

                .beginTransaction()

                .replace(LoginNewFragment.this.getId(), prFrag)

                .commit();


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            parentDialogListener = (ParentDialogsListener) activity;

        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " Activity's Parent should be Parent Activity");
        }


    }

    public void getAddressDetails2() {

        street.clear();
        name.clear();
        city.clear();
        country.clear();
        phone.clear();
        address_id.clear();
        countryId.clear();

        // parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map1 = new HashMap<>();

        map1.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<AddressList> call = apiService.getAddressInfo(map1);
        call.enqueue(new Callback<AddressList>() {
            @Override
            public void onResponse(Call<AddressList> call, Response<AddressList> response) {


                if (response.body() != null) {
                    // parentDialogListener.hideProgressDialog();


                    if (response.body().msg.equals("success")) {
                        if (response.body().data.size() > 0) {


                            for (int i = 0; i < response.body().data.size(); i++) {

                                name.add(response.body().data.get(i).firstname + " " + response.body().data.get(i).lastname);
                                addressfirstname.add(response.body().data.get(i).firstname);
                                addresslastname.add(response.body().data.get(i).lastname);
                                street.add(response.body().data.get(i).street);
                                city.add(response.body().data.get(i).city);
                                country.add(response.body().data.get(i).country);
                                phone.add(response.body().data.get(i).telephone);
                                address_id.add(response.body().data.get(i).addressId);
                                countryId.add(response.body().data.get(i).country_id);


                            }

                            Collections.reverse(name);
                            Collections.reverse(street);
                            Collections.reverse(city);
                            Collections.reverse(country);
                            Collections.reverse(phone);
                            Collections.reverse(address_id);
                            Collections.reverse(countryId);


                            MySharedPreferenceClass.setfirstAdressname(getContext(), addressfirstname.get(0).toString());
                            MySharedPreferenceClass.setlastAdressname(getContext(), addresslastname.get(0).toString());
                            MySharedPreferenceClass.setAddressname(getContext(), name.get(0).toString());
                            MySharedPreferenceClass.setCityname(getContext(), city.get(0).toString());
                            MySharedPreferenceClass.setStreetName(getContext(), street.get(0).toString());
                            MySharedPreferenceClass.setCountryname(getContext(), country.get(0).toString());

                            MySharedPreferenceClass.setAddressphone(getContext(), phone.get(0).toString());
                            MySharedPreferenceClass.setCountryId(getContext(), countryId.get(0).toString());


                            MySharedPreferenceClass.setSelecteAddressdId(getContext(), address_id.get(0).toString());

                        } else {
                            // parentDialogListener.hideProgressDialog();


                        }


                    }
                } else {
                    // parentDialogListener.hideProgressDialog();


                }


                getCartData();


            }


            @Override
            public void onFailure(Call<AddressList> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }

    public void getCartData() {


        // parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();
        query.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));
        query.put("mask_key", "");
        query.put("cart_id", "");

        Call<ViewCart1> call = apiService.getCartDetail(query);
        call.enqueue(new Callback<ViewCart1>() {
            @Override
            public void onResponse(Call<ViewCart1> call, Response<ViewCart1> response) {

                parentDialogListener.hideProgressDialog();
                int quant;

                if (response.body() != null) {


                    if (response.body().msg.equalsIgnoreCase("success")) {


                        cartData = response.body().data;

                        for (int i = 0; i < response.body().data.size(); i++) {

                            if (response.body().data.get(i).qty instanceof String) {
                                totalcount = totalcount + (int) Float.parseFloat((String) response.body().data.get(i).qty);

                            } else {
                                totalcount = totalcount + (int) (response.body().data.get(i).qty);


                            }


                        }


                        MySharedPreferenceClass.setBedgeCount(getContext(), totalcount);
                        updatefix.updateBedgeCount();
                        MainActivity.notificationBadge.setVisibility(View.VISIBLE);
                        parentDialogListener.hideProgressDialog();


                    }

                    if (value != null && value.equals("fromcart")) {

                        System.out.println("inside----->>>+");

                        cartreference.setUserHintcall();
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        // Fragment prFrag = new CartFragment();
                        // getFragmentManager()

                        //       .beginTransaction()

                        //    .replace(LoginFragment.this.getId(), prFrag)

                        //   .commit();

                    } else {
                        System.out.println("initprofile----->>>+");
                        initProfileFragment();
                    }
                }


            }


            @Override
            public void onFailure(Call<ViewCart1> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());
                MainActivity.notificationBadge.setVisibility(View.GONE);
                MainActivity.bedgetext.setVisibility(View.GONE);
                MainActivity.toolbar_bedgetext.setVisibility(View.GONE);


            }
        });
    }

    public void getAddressDetails() {

        street.clear();
        name.clear();
        city.clear();
        country.clear();
        phone.clear();
        address_id.clear();
        countryId.clear();

        parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> map1 = new HashMap<>();

        map1.put("customer_id", MySharedPreferenceClass.getMyUserId(getContext()));


        Call<AddressList> call = apiService.getAddressInfo(map1);
        call.enqueue(new Callback<AddressList>() {
            @Override
            public void onResponse(Call<AddressList> call, Response<AddressList> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();


                    if (response.body().msg.equals("success")) {


                        for (int i = 0; i < response.body().data.size(); i++) {
                            name.add(response.body().data.get(i).firstname + " " + response.body().data.get(i).lastname);
                            street.add(response.body().data.get(i).street);
                            city.add(response.body().data.get(i).street);
                            country.add(response.body().data.get(i).country);
                            phone.add(response.body().data.get(i).telephone);
                            address_id.add(response.body().data.get(i).addressId);
                            countryId.add(response.body().data.get(i).country_id);


                        }

                        if (response.body().data.size() > 0) {
                            MySharedPreferenceClass.setAddressname(getContext(), name.get(name.size() - 1));
                            MySharedPreferenceClass.setCityname(getContext(), city.get(city.size() - 1));
                            MySharedPreferenceClass.setStreetName(getContext(), street.get(street.size() - 1));
                            MySharedPreferenceClass.setCountryname(getContext(), country.get(country.size() - 1));

                            MySharedPreferenceClass.setAddressphone(getContext(), phone.get(phone.size() - 1));
                            MySharedPreferenceClass.setCountryId(getContext(), countryId.get(phone.size() - 1));

                        } else {
                            MySharedPreferenceClass.setAddressname(getContext(), null);
                            MySharedPreferenceClass.setCityname(getContext(), null);
                            MySharedPreferenceClass.setStreetName(getContext(), null);
                            MySharedPreferenceClass.setCountryname(getContext(), null);
                            MySharedPreferenceClass.setCountryId(getContext(), null);


                        }
                        if (value.equals("fromdetail")) {

                            getActivity().onBackPressed();


                        } else {


                            Fragment prFrag = new NewProfileFragment();
                            getFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.profileContainer, prFrag)
                                    .commit();


                        }

                    } else {
                        parentDialogListener.hideProgressDialog();


                    }
                } else {
                    parentDialogListener.hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<AddressList> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "Something went wrong. Please try again", Toast.LENGTH_LONG).show();
            }
        });


    }

    protected void hideSoftKeyboard(EditText input) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

    private void logUser(String id, String userMail, String userName) {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(id);
        Crashlytics.setUserEmail(userMail);
        Crashlytics.setUserName(userName);


    }

    public void appsflyer_event_login(String customer_id) {
        Map<String, Object> eventValue = new HashMap<String, Object>();

        eventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, customer_id);
        AppsFlyerLib.getInstance().trackEvent(getActivity(), AFInAppEventType.LOGIN, eventValue);
    }

    public void clearFragmentBackStack() {
        FragmentManager fm = getFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }


    }


    public void clearStack() {
        //Here we are clearing back stack fragment entries
        int backStackEntry = getFragmentManager().getBackStackEntryCount();
        if (backStackEntry > 0) {
            for (int i = 0; i < backStackEntry; i++) {
                getFragmentManager().popBackStackImmediate();
            }
        }

    }


    public static boolean checkPermission(String permission, Activity activity) {
        return ContextCompat.checkSelfPermission(activity, permission) ==
                PackageManager.PERMISSION_GRANTED;
    }

    public void getcountryDetail() {
        // parentDialogListener.showProgressDialog();
        countryname = new ArrayList<>();
        countryCode = new ArrayList<>();
        placeholder = new ArrayList<>();
        celcode = new ArrayList<>();


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Call<GetCountryCode> call = apiService.getCountryDetail();
        call.enqueue(new Callback<GetCountryCode>() {
            @Override
            public void onResponse(Call<GetCountryCode> call, Response<GetCountryCode> response) {


                if (response.body() != null) {
                    parentDialogListener.hideProgressDialog();
                    for (int i = 0; i < response.body().data.size(); i++) {
                        String countrycode = response.body().data.get(i).cel_code.replace("+", "");

                        // countryname.add("(" + response.body().data.get(i).cel_code + ")" + " " + response.body().data.get(i).name);
                        countryCode.add(response.body().data.get(i).code);
                        countryname.add("(" + countrycode + "+" + ")" + " " + response.body().data.get(i).name);
                        placeholder.add(response.body().data.get(i).placeholder.replaceAll("\\s", ""));
                        celcode.add(response.body().data.get(i).cel_code);


                    }
                    Collections.reverse(countryname);
                    Collections.reverse(celcode);
                    Collections.reverse(countryCode);
                    Collections.reverse(placeholder);
                    initSpinner();


                }
            }

            @Override
            public void onFailure(Call<GetCountryCode> call, Throwable t) {
                parentDialogListener.hideProgressDialog();
                Log.e(TAG, t.toString());

                Toast.makeText(getContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void initSpinner() {

        if (getActivity() != null) {


            //  CustomArrayAdapter adp2 = new CustomArrayAdapter(getContext(),countryname.toArray(new String[countryname.size()]));
            ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(), R.layout.spinnertextview2, countryname);
            adp2.setDropDownViewResource(R.layout.checkedradiotextview);


            // adp2.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);


            countryspinner.setAdapter(adp2);


            countryspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    //Toast.makeText(getApplicationContext(),"Position"+ position, Toast.LENGTH_SHORT).show();


                    countryName = countryspinner.getSelectedItem().toString();
                    selctedposition = position;
                    edtmobile.setHint(placeholder.get(selctedposition));
                    mobilevalidationlength = placeholder.get(selctedposition).length();
                    edtmobile.setFilters(new InputFilter[]{new InputFilter.LengthFilter(placeholder.get(selctedposition).length())});
                    edtmobile.setText("");
                    //edtMobile.setHint("رقم الجوال");
                    Log.e("Gender Name--------.>>", countryName);

                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }

            });

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        //     otp = data.getStringExtra("otp");

        loginProceed(" ", " ", "2");

    }


}