package com.eoutlet.Eoutlet.fragments;


import android.app.Dialog;
import android.content.Context;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.eoutlet.Eoutlet.R;

import com.eoutlet.Eoutlet.intrface.ParentDialogsListener;


public class GiftboxFragment extends DialogFragment {
    private View view;
    private Dialog dialog;
    private ParentDialogsListener parentDialogListener;
    private ImageView closeImage;
    private RelativeLayout closebtn;

    public GiftboxFragment() {
        // Required empty public constructor
    }





        public static RePasswordVerifyOtp newInstance(String param1, String param2) {
            RePasswordVerifyOtp fragment = new RePasswordVerifyOtp();
            Bundle args = new Bundle();

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {

            }
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState)  {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            // Get the layout inflater
            LayoutInflater inflater = getActivity().getLayoutInflater();
            view = inflater.inflate(R.layout.fragment_giftbox, null);


            builder.setView(view);

            dialog = builder.create();
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.WHITE));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_rounded_bg);

            closeImage = view.findViewById(R.id.closeImage1);
            closebtn = view.findViewById(R.id.closebtn);

            closeImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });


            closebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            return dialog;
        }









        @Override
        public void onAttach(Context context) {
            try {
                // Instantiate the NoticeDialogListener so we can send events to the host
                parentDialogListener = (ParentDialogsListener) context;

            } catch (ClassCastException e) {
                // The activity doesn't implement the interface, throw exception
                throw new ClassCastException(context.toString()
                        + " Activity's Parent should be Parent Activity");
            }
            super.onAttach(context);
        }

        @Override
        public void onDetach() {
            super.onDetach();

        }

        public interface OnFragmentInteractionListener {
            // TODO: Update argument type and name
            void onFragmentInteraction(Uri uri);
        }
}