package com.eoutlet.Eoutlet.listener;

/**
 * Created by indianrenters on 4/20/17.
 */

public interface FragmentSetDateListener {

    public void setDate(String date);
}
