package com.eoutlet.Eoutlet.activities;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.AddMoneyResponse;
import com.eoutlet.Eoutlet.pojo.GetShippingCharge;
import com.eoutlet.Eoutlet.pojo.WalletPaymentStatus;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Handler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class Threedpasswordactivity extends ParentActivity {
    private WebView threedpaymentwebview;
    private String url,orderId;
    private final int interval = 5000; // 1 Second
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threedpasswordactivity);

        threedpaymentwebview = findViewById(R.id.threedpaymentWebview);

        url = getIntent().getStringExtra("url");
        orderId = getIntent().getStringExtra("order_id");

        threedpaymentwebview.getSettings().setJavaScriptEnabled(true);

        CookieManager.getInstance().removeAllCookies(null);
        CookieManager.getInstance().flush();

        threedpaymentwebview.getSettings().setJavaScriptEnabled(true);
        threedpaymentwebview.getSettings().setUseWideViewPort(true);
        threedpaymentwebview.getSettings().setLoadWithOverviewMode(true);
        threedpaymentwebview.clearCache(true);
        threedpaymentwebview.clearHistory();
        threedpaymentwebview.clearFormData();
        threedpaymentwebview.setWebViewClient(new MyBrowser());

        threedpaymentwebview.loadUrl(url);






    }

    private class MyBrowser extends WebViewClient {


        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            //  parentDialogListener.showProgressDialog();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            return super.shouldOverrideUrlLoading(view, url);
/*
            view.loadUrl(url);



            return true;*/
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            new Timer().scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    System.out.println("---------inside timer-------------");
                    rungetstatusAPI();
                    //Toast.makeText(getContext(), "C'Mom no hands!", Toast.LENGTH_SHORT).show();
                }
            }, 0, 7000);

        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getApplicationContext(), "Error" + description + errorCode, Toast.LENGTH_SHORT).show();
        }


    }

    public void rungetstatusAPI() {


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        Map<String, String> query = new HashMap<>();


        query.put("order_id", orderId);


        Call<WalletPaymentStatus> call = apiService.getpaymentstatus(query);
        call.enqueue(new Callback<WalletPaymentStatus>() {
            @Override
            public void onResponse(Call<WalletPaymentStatus> call, Response<WalletPaymentStatus> response) {


                if (response.body() != null) {

                    hideProgressDialog();

                    if (response.body().msg.equals("success")) {
                        finish();
                        Toast.makeText(Threedpasswordactivity.this,"تم اضافة الرصيد بنجاح الى حسابك",Toast.LENGTH_SHORT).show();

                    }


                }

            }


            @Override
            public void onFailure(Call<WalletPaymentStatus> call, Throwable t) {
                hideProgressDialog();
                // progressDialog.hide();
                Log.e(TAG, t.toString());

                //Toast.makeText(Threedpasswordactivity.this, "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }
}