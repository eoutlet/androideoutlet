package com.eoutlet.Eoutlet.activities;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.fragments.CartFragment;
import com.eoutlet.Eoutlet.fragments.GiftboxFragment;
import com.eoutlet.Eoutlet.fragments.SuccessThankyouFragment;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.GuestUser;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ThankyouActivity extends ParentActivity{
    LinearLayout thankmain;
    String statusflag = " ";
    TextView message,status;
    private  String orderId,price,coupencode;
    private TextView tvorderId ;
    private ImageView statusimage;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
       orderId = getIntent().getStringExtra("orderId");
       price = getIntent().getStringExtra("totalvalue");
       coupencode = getIntent().getStringExtra("coupencode");

       if( getIntent().getStringExtra("statusflag")!=null) {
           statusflag = getIntent().getStringExtra("statusflag");
       }
        tvorderId = (TextView) findViewById(R.id.resultOrderId);
        statusimage = (ImageView)findViewById(R.id.statuImage);
        message  = (TextView)findViewById(R.id.acc1) ;
        status = (TextView)findViewById(R.id.statusflag);

        thankmain = (LinearLayout) findViewById(R.id.thankyoumain);
        tvorderId.setText(orderId);

        MySharedPreferenceClass.setCheckbofflag(this,false);

       // Toast.makeText(getApplicationContext(),statusflag,Toast.LENGTH_LONG).show();
      getguesttoken();




       /* thankmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ThankyouActivity.this, MainActivity.class);
                startActivity(in);
                finish();


            }
        });
*/

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent in = new Intent(ThankyouActivity.this, MainActivity.class);
        startActivity(in);
        finish();

    }


    public void appsflyer_event_purchase(){
        //Purchase


        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.PRICE, price);
        eventValue.put(AFInAppEventParameterName.CONTENT_ID, "");
// for multiple product categories, set the param value as: // new String {"221", "124"}
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, "");
// for multiple product categories,, set the param value as: new String {"shoes", "pants"}
        eventValue.put(AFInAppEventParameterName.CURRENCY, "SAR");
        eventValue.put(AFInAppEventParameterName.QUANTITY, "");
// for multiple product categories, set the param value as: new int {2, 5}
        eventValue.put(AFInAppEventParameterName.RECEIPT_ID, orderId);
        eventValue.put("af_order_id", orderId);
        eventValue.put(AFInAppEventParameterName.REVENUE,price);
        eventValue.put(AFInAppEventParameterName.COUPON_CODE, coupencode);
        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.PURCHASE, eventValue);
        appsflyer_payment_info();
    }


    public void appsflyer_payment_info()

    {
        Map<String, Object> eventValue = new HashMap<String, Object>();

        eventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, MySharedPreferenceClass.getMyUserId(getApplicationContext()));
        eventValue.put(AFInAppEventParameterName.CURRENCY,"SAR");
        eventValue.put(AFInAppEventParameterName.PRICE, price);

        eventValue.put(AFInAppEventParameterName.QUANTITY, " ");

        eventValue.put(AFInAppEventParameterName.PAYMENT_INFO_AVAILIBLE, orderId);




        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.ADD_PAYMENT_INFO, eventValue);

        firebase_event_purchase();

    }



    public void firebase_event_purchase()
    {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.COUPON, coupencode);
        bundle.putString(FirebaseAnalytics.Param.CURRENCY,"SAR");
        bundle.putInt(FirebaseAnalytics.Param.VALUE, Integer.valueOf(price));
        bundle.putString(FirebaseAnalytics.Param.TAX," ");
        bundle.putString(FirebaseAnalytics.Param.SHIPPING, " ");
        bundle.putString(FirebaseAnalytics.Param.TRANSACTION_ID,orderId );

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, bundle);




    }


    public void getguesttoken() {


         showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);
        Map<String, String> map = new HashMap<>();
        map.put("customer_id", MySharedPreferenceClass.getMyUserId(getApplicationContext()));


        Call<GuestUser> call = apiService.getGuestTokenforthankyou(map);
        call.enqueue(new Callback<GuestUser>() {
            @Override
            public void onResponse(Call<GuestUser> call, Response<GuestUser> response) {


                if (response.body().msg.equals("success")) {
                      hideProgressDialog();
                    MySharedPreferenceClass.setMaskkey(getBaseContext(), response.body().maskKey);
                    MySharedPreferenceClass.setCartId(getBaseContext(), response.body().cartId);

                    initUI();


                } else {
                    initUI();
                      hideProgressDialog();


                }


            }


            @Override
            public void onFailure(Call<GuestUser> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, t.toString());

            }
        });


    }
public void initUI()
{


    if(!statusflag.equals(" ")) {

        if (statusflag.equals("failed") || statusflag.equals("cancel")) {

           thankmain.setVisibility(View.VISIBLE);

            if(statusflag.equals("failed")){

                status.setText("فشل");
                message.setText("آسف فشل طلبك");


            }
            else if(statusflag.equals("cancel")) {


                thankmain.setVisibility(View.VISIBLE);
                status.setText("طلب ملغي");
                message.setText("نأسف لقد تم الغاء طلبك");



            }


            statusimage.setImageDrawable(getDrawable(R.drawable.ic_cancel));
            statusimage.setVisibility(View.VISIBLE);


        }
        else {

                 thankmain.setVisibility(View.VISIBLE);
                 if(MainActivity.fromcart) {

                     try {

                         DialogFragment prFrag = new SuccessThankyouFragment();
                         Bundle bund = new Bundle();
                         prFrag.setArguments(bund);
                         prFrag.show(getSupportFragmentManager(), "Thankyou");



                     }
                     catch (Exception e){


                     }
                 }

            appsflyer_event_purchase();

        }



    }


    else {




        appsflyer_event_purchase();





    }


}

}
