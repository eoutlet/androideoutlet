package com.eoutlet.Eoutlet.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.api.request.CatagoryList;
import com.eoutlet.Eoutlet.pojo.AppCurrentVersion;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class Splash2 extends AppCompatActivity {
    private ImageView zoomoutImage;
    private int progressStatus = 0;
    private Handler handler = new Handler();
    String version, latestversion;

    private static int SPLASH_TIME_OUT = 2200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash2);
        Animation animZoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
        zoomoutImage = (ImageView) findViewById(R.id.zoomoutImage);
        zoomoutImage.startAnimation(animZoomOut);
       // getversionname();


        final ProgressBar pb = (ProgressBar) findViewById(R.id.pb);


        // Set the progress status zero on each button click
        progressStatus = 0;

                /*
                    A Thread is a concurrent unit of execution. It has its own call stack for
                    methods being invoked, their arguments and local variables. Each application
                    has at least one thread running when it is started, the main thread,
                    in the main ThreadGroup. The runtime keeps its own threads
                    in the system thread group.
                */
        // Start the lengthy operation in a background thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressStatus < 100) {
                    // Update the progress status
                    progressStatus += 1;

                    // Try to sleep the thread for 20 milliseconds
                    try {
                        Thread.sleep(21);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // Update the progress bar
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            pb.setProgress(progressStatus);
                            // Show the progress on TextView

                        }
                    });
                }
            }
        }).start(); // Start the operation*/

        openNextActivity();
    }


    public void openNextActivity() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent in = new Intent(Splash2.this, MainActivity.class);
                //Intent in = new Intent(Splash2.this, Splash2.class);
                startActivity(in);
                //overridePendingTransition(R.anim.slide_to_left,R.anim.slide_to_left);
                finish();


            }
        }, SPLASH_TIME_OUT);


    }

    public void getversionname() {

        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            Toast.makeText(getApplicationContext(), "version is" + version, Toast.LENGTH_SHORT).show();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


    }

    public void getLatestversionCode() {


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Call<AppCurrentVersion> call = apiService.getLatestAppVersion();
        call.enqueue(new Callback<AppCurrentVersion>() {
            @Override
            public void onResponse(Call<AppCurrentVersion> call, Response<AppCurrentVersion> response) {


                if (response.body() != null) {

                    if (response.body().msg.equals("success")) {


                        latestversion = response.body().androidVersion;
                        Toast.makeText(getApplicationContext(), "version is" + latestversion, Toast.LENGTH_SHORT).show();
                    }


                }


            }


            @Override
            public void onFailure(Call<AppCurrentVersion> call, Throwable t) {

                Log.e(TAG, t.toString());


            }
        });


    }



}
