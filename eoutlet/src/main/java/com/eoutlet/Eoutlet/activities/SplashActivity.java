package com.eoutlet.Eoutlet.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.utility.Util;

import java.util.Map;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        if (Util.checkConnection(this)) {
            openNextActivity();

        } else {

            openNoInternerdialog();


        }



    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Toast.makeText(getBaseContext(),"FromNew Intent",Toast.LENGTH_SHORT).show();
        ///onPushNotificationClick(intent);
    }

    public void openNoInternerdialog() {
        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertDialogBuilder.setTitle("No Internet Connection");
        alertDialogBuilder.setMessage("Sorry,no Internet connectivity detected.Please reconnect and try again");


        alertDialogBuilder.setPositiveButton("RETRY",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (Util.checkConnection(getApplicationContext())) {
                            openNextActivity();

                        } else {

                            openNoInternerdialog();


                        }

                    }
                });

        alertDialogBuilder.setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which) {
                finish();

            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void openNextActivity() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent in = new Intent(SplashActivity.this, MainActivity.class);
                //Intent in = new Intent(SplashActivity.this, EoutletSplash.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(in);

               // overridePendingTransition(R.anim.slide_to_left,R.anim.slide_to_left);

                finish();

            }
        }, SPLASH_TIME_OUT);


    }
}
