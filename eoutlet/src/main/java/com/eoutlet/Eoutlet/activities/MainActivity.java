package com.eoutlet.Eoutlet.activities;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.crashlytics.android.Crashlytics;
import com.eoutlet.Eoutlet.R;
import com.eoutlet.Eoutlet.adpters.BottomBarAdapter;
import com.eoutlet.Eoutlet.api.BasicBuilder;
import com.eoutlet.Eoutlet.api.request.BasicRequest;
import com.eoutlet.Eoutlet.fragments.AZfragment;
import com.eoutlet.Eoutlet.fragments.CartFragment;

import com.eoutlet.Eoutlet.fragments.NewCategoryFragment;

import com.eoutlet.Eoutlet.fragments.NewHomeFragment2;
import com.eoutlet.Eoutlet.fragments.NewProfileFragment;
import com.eoutlet.Eoutlet.fragments.OrderPaymentWebView;
import com.eoutlet.Eoutlet.fragments.ProductDetail;
import com.eoutlet.Eoutlet.fragments.ProductList;
import com.eoutlet.Eoutlet.fragments.SearchResultFragment;
import com.eoutlet.Eoutlet.fragments.Signup;
import com.eoutlet.Eoutlet.fragments.ThreedpasswordFragments;
import com.eoutlet.Eoutlet.intrface.ExecuteFragment;
import com.eoutlet.Eoutlet.intrface.UpdateBedgeCount;
import com.eoutlet.Eoutlet.others.MySharedPreferenceClass;
import com.eoutlet.Eoutlet.pojo.AppCurrentVersion;
import com.eoutlet.Eoutlet.pojo.GuestUser;
import com.eoutlet.Eoutlet.pojo.LoginResponse;
import com.eoutlet.Eoutlet.services.MyFirebaseMessagingService;
import com.eoutlet.Eoutlet.utility.Constants;
import com.eoutlet.Eoutlet.utility.Util;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.payfort.fort.android.sdk.base.FortSdk;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class MainActivity extends ParentActivity implements ExecuteFragment, UpdateBedgeCount {
    private BottomNavigationView navigationView;
    Toolbar toolbar;
    private Context mcontext = MainActivity.this;
    public static TextView bedgetext, toolbar_bedgetext;


    public static View notificationBadge;
    static int totalcount;
    private String mCurrentTab;
    private HashMap<String, Stack<Fragment>> mStacks;
    public static final String TAB_HOME = "tab_home";
    public static final String TAB_CATOGORY = "tab_catagory";
    public static final String TAB_PROFILE = "tab_profile";
    public static final String TAB_CART = "tab_cart";
    MenuItem lastclickeditem;
    public static String shortlink1 = " ";
    public static String id = " ";
    public static String name = " ";
    public static String opencartview = " ";
    private String version, latestversion;
    private int versioncode;
    private NoSwipePager viewPager;
    private BottomBarAdapter pagerAdapter;
    private BottomNavigationView navigation;
    private ImageView toolbarsearch;
    private RelativeLayout toolbarbeg;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    public static boolean fromcart = false;

    CartFragment frag1 = new CartFragment();

    NewProfileFragment frag2 = new NewProfileFragment();
    //BrandFragment frag3 = new BrandFragment();


    AZfragment frag3 = new AZfragment();
    NewCategoryFragment frag4 = new NewCategoryFragment();
    NewHomeFragment2 frag5 = new NewHomeFragment2();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());



        initViews();


        if (MySharedPreferenceClass.getMaskkey(getBaseContext()) == null) {


            getguesttoken();


        }
        getversionname();
        //getLatestversionCode();

        createBroadCastReceiver();


        AppsFlyerLib.getInstance().sendDeepLinkData(this);


        AppsFlyerLib.getInstance().registerConversionListener(this, new AppsFlyerConversionListener() {


            @Override
            public void onConversionDataSuccess(Map<String, Object> map) {
                for (String attrName : map.keySet()) {
                    Log.d("ConversionData", "conversion_attribute: " + attrName + " = " +
                            map.get(attrName));


                    if (attrName.equals("shortlink")) {
                        shortlink1 = map.get(attrName) + " ";

                        Toast.makeText(getBaseContext(), "" + shortlink1, Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onConversionDataFail(String errorMessage) {
                Log.d("Fail", "error onAttributionFailure : " + errorMessage);
            }

            /* for direct deep linking */
            @Override
            public void onAppOpenAttribution(Map<String, String> conversionData) {
                for (String attrName : conversionData.keySet()) {
                    Log.d(" Attribution", "onAppOpen_attribute: " + attrName + " = " +
                            conversionData.get(attrName));


                    if (attrName.equals("id")) {

                        id = conversionData.get(attrName) + "";
                    } else if (attrName.equals("name")) {


                        name = conversionData.get(attrName) + "";
                    } else if (attrName.equals("opencartview")) {
                        opencartview = conversionData.get(attrName) + "";

                    }
                }
            }

            @Override
            public void onAttributionFailure(String errorMessage) {
                Log.d("Fail2", "error onAttributionFailure : " + errorMessage);
            }
        });


    }


    public void getguesttoken() {


        //  showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Call<GuestUser> call = apiService.getGuestToken();
        call.enqueue(new Callback<GuestUser>() {
            @Override
            public void onResponse(Call<GuestUser> call, Response<GuestUser> response) {

                if (response.body() != null && response.body().msg != null) {
                    if (response.body().msg.equals("success")) {
                        //  hideProgressDialog();
                        MySharedPreferenceClass.setMaskkey(getBaseContext(), response.body().maskKey);
                        MySharedPreferenceClass.setCartId(getBaseContext(), response.body().cartId);


                    } else {
                        //  hideProgressDialog();


                    }
                }

            }


            @Override
            public void onFailure(Call<GuestUser> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, t.toString());

            }
        });


    }


    public void initViews() {

        navigationView = findViewById(R.id.navigationView);
        toolbar = findViewById(R.id.common_toolbar);


        viewPager = findViewById(R.id.mainviewPager);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setPagingEnabled(false);


        pagerAdapter = new BottomBarAdapter(getSupportFragmentManager());

        pagerAdapter.addFragments(frag1);
        pagerAdapter.addFragments(frag2);
        pagerAdapter.addFragments(frag3);
        pagerAdapter.addFragments(frag4);
        pagerAdapter.addFragments(frag5);

        viewPager.setAdapter(pagerAdapter);


        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navigationView.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(0);
        notificationBadge = LayoutInflater.from(this).inflate(R.layout.cart_bedge, menuView, false);

        itemView.addView(notificationBadge);

        bedgetext = findViewById(R.id.cart_badge_text);


        navigationView.setItemIconTintList(null);
        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigationView.setSelectedItemId(R.id.home);


        toolbar_bedgetext = toolbar.findViewById(R.id.toolbar_cart_badge_text);
        toolbarsearch = toolbar.findViewById(R.id.serachbar);
        toolbarbeg = toolbar.findViewById(R.id.toolbarbag);
        toolbarsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment prFrag = new SearchResultFragment();
                loadFragment(prFrag);


            }
        });


        toolbarbeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewPager.setCurrentItem(0);

            }
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        Fragment fragment = null;
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.container);

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {

            if (lastclickeditem != null) {

                Fragment fragment;
                Fragment manager = getSupportFragmentManager().getFragments().get(viewPager.getCurrentItem());

                if (manager.getFragmentManager() != null) {
                    if (manager.getFragmentManager().getBackStackEntryCount() > 0) {
                        while (manager.getFragmentManager().getBackStackEntryCount() > 0) {

                            manager.getFragmentManager().popBackStackImmediate();
                        }
                    }

                }
            }
            lastclickeditem = item;


            switch (item.getItemId()) {
                case R.id.cart:

                    toolbar.setVisibility(View.VISIBLE);
                    viewPager.setCurrentItem(0);

                    bedgetext.setTextColor(ContextCompat.getColor(MainActivity.this,R.color.colour_black));
                    bedgetext.setBackground(ContextCompat.getDrawable(MainActivity.this,R.drawable.selected_badgedrawable));
                    // getSupportFragmentManager().popBackStackImmediate();
                    return true;


                       /* fragment = new CartFragment();

                        loadFragment(fragment);*/
                // bedgetext.setVisibility(View.GONE);


                case R.id.profile:
                    viewPager.setCurrentItem(1);
                    bedgetext.setTextColor(ContextCompat.getColor(MainActivity.this,R.color.white));
                    bedgetext.setBackground(ContextCompat.getDrawable(MainActivity.this,R.drawable.noti_bedge2));
                    toolbar.setVisibility(View.GONE);
                    // getSupportFragmentManager().popBackStackImmediate();
//                    selectedTab(TAB_PROFILE);
                   /* if (MySharedPreferenceClass.getMyUserId(mcontext) == null) {


                        fragment = new LoginFragment();
                        Bundle databund = new Bundle();
                        databund.putString("flag","fromlogin");
                        fragment.setArguments(databund);
                        ((LoginFragment) fragment).passData(mcontext);
                    } else {
                        fragment = new ProfileFragment();
                    }*/

                 /*   loadFragment(fragment);
                    return true;*/
                    return true;
                case R.id.home:

//                     selectedTab(TAB_HOME);

                    // getSupportFragmentManager().popBackStackImmediate();


                    //fragment = new HomeFragment();
                    toolbar.setVisibility(View.VISIBLE);
                    bedgetext.setTextColor(ContextCompat.getColor(MainActivity.this,R.color.white));
                    bedgetext.setBackground(ContextCompat.getDrawable(MainActivity.this,R.drawable.noti_bedge2));
                    fragment = new NewHomeFragment2();


                    ((NewHomeFragment2) fragment).passData(mcontext);

                    viewPager.setCurrentItem(4);


                    return true;


                case R.id.catagory:
                    toolbar.setVisibility(View.VISIBLE);
                    bedgetext.setTextColor(ContextCompat.getColor(MainActivity.this,R.color.white));
                    bedgetext.setBackground(ContextCompat.getDrawable(MainActivity.this,R.drawable.noti_bedge2));
                    //getSupportFragmentManager().popBackStackImmediate();
                    viewPager.setCurrentItem(3);
//                    selectedTab(TAB_CATOGORY);
                    fragment = new NewCategoryFragment();

                    ((NewCategoryFragment) fragment).passData(mcontext);

//                    loadFragment(fragment);
                    return true;

                case R.id.AtoZ:
                    //getSupportFragmentManager().popBackStackImmediate();
                    //  fragment = new BrandFragment();
//                    ((CatagoryFragment) fragment).passData(mcontext);
//                    ((ProductDetail) fragment).passData(mcontext);
//                    loadFragment(fragment);
                    bedgetext.setTextColor(ContextCompat.getColor(MainActivity.this,R.color.white));
                    bedgetext.setBackground(ContextCompat.getDrawable(MainActivity.this,R.drawable.noti_bedge2));
                    toolbar.setVisibility(View.VISIBLE);
                    viewPager.setCurrentItem(2);
                    return true;


            }
            return false;
        }







    };

    private void
    loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container2, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
//        transaction.commitNowAllowingStateLoss();
    }


    @Override
    public void ExecutFragmentListener(int x) {

        if (x == 0) {

            // Toast.makeText(getApplicationContext(),"fromdetail",Toast.LENGTH_LONG).show();
            viewPager.setCurrentItem(0);


        } else if (x == 1) {
            viewPager.setCurrentItem(1);

            // Fragment fragment = new LoginFragment();
            // ((LoginFragment) fragment).passData(this);
            // loadFragment(fragment);
            Menu menu = navigationView.getMenu();
            MenuItem item = menu.getItem(1);
            item.setChecked(item.getItemId() == R.id.profile);


        } else if (x == 2) {

            Fragment fragment = new Signup();
            ((Signup) fragment).passData(this);
            loadFragment(fragment);


        } else if (x == 3) {

            Fragment fragment = new ProductList();
            ((ProductList) fragment).passData(this);
            loadFragment(fragment);


        } else if (x == 4) {

            Fragment fragment = new ProductDetail();
            ((ProductDetail) fragment).passData(this);
            loadFragment(fragment);


        } else if (x == 5) {

            Menu menu = navigationView.getMenu();
            MenuItem item = menu.getItem(1);
            item.setChecked(item.getItemId() == R.id.profile);
            Fragment fragment = new NewProfileFragment();

            loadFragment(fragment);


        } else if (x == 6) {


            Fragment fragment = new CartFragment();

            loadFragment(fragment);


        }
    }


    public void updateBedgeCount() {

        bedgetext.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(this)));
        bedgetext.setVisibility(View.VISIBLE);
        toolbar_bedgetext.setVisibility(View.VISIBLE);
        toolbar_bedgetext.setText(String.valueOf(MySharedPreferenceClass.getBedgeCount(this)));


    }


    @Override
    public void onBackPressed() {

        if ( OrderPaymentWebView.ispaymentwebview || ThreedpasswordFragments.isthreedpasswordwalletwebview) {
            return;
        }
        super.onBackPressed();
      /*  FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment current
                = fragmentManager.findFragmentById(R.id.container2);

        Toast.makeText(getApplicationContext(),current.toString(),Toast.LENGTH_LONG).show();

        //FragmentTags fragmentTag = FragmentTags.valueOf(current.getTag());*/

    }



    /*
    @Override
    protected void onRestart() {
        super.onRestart();
        getLatestversionCode();

    }*/


    public void getversionname() {

        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            versioncode = pInfo.versionCode;
            //Toast.makeText(getApplicationContext(), "version is" + version, Toast.LENGTH_SHORT).show();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


    }

    public void getLatestversionCode() {


        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);


        Call<AppCurrentVersion> call = apiService.getLatestAppVersion();
        call.enqueue(new Callback<AppCurrentVersion>() {
            @Override
            public void onResponse(Call<AppCurrentVersion> call, Response<AppCurrentVersion> response) {


                if (response.body() != null) {

                    if (response.body().msg.equals("success")) {


                        latestversion = response.body().androidVersion;

                        if (versioncode < Integer.parseInt(latestversion))

                        // if(version.equals(latestversion))
                        {
                            openUpdatedialog();


                        } else {
                            if (viewPager == null) {

                                initViews();

                            }

                        }
                        //Toast.makeText(getApplicationContext(), "version is" + latestversion, Toast.LENGTH_SHORT).show();
                    }


                }


            }


            @Override
            public void onFailure(Call<AppCurrentVersion> call, Throwable t) {

                Log.e(TAG, t.toString());


            }
        });


    }

    public void openUpdatedialog() {
        final AlertDialog alertDialog;
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);


        TextView myMsg = new TextView(this);


        myMsg.setText("رساله");
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextSize(25);
        myMsg.setTextColor(Color.BLACK);
        alertDialogBuilder.setCustomTitle(myMsg);


        TextView myMsg2 = new TextView(this);
        myMsg2.setText("هناك إصدار جديد من هذا التطبيق متاح");
        myMsg2.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg2.setTextSize(25);
        myMsg2.setTextColor(Color.BLACK);
        alertDialogBuilder.setMessage(myMsg2.getText().toString());


        //alertDialogBuilder.setMessage("هناك إصدار جديد من هذا التطبيق متاح");

        alertDialogBuilder.setPositiveButton(
                "تحديث",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (Util.checkConnection(getApplicationContext())) {

                            appsflyer_event_update();
                            navigatetoPlaystore();

                        }

                    }
                });
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();


    }


    public void navigatetoPlaystore() {


        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            intent.setAction(Intent.ACTION_MAIN);
            startActivity(intent);


        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }


    }

    public void appsflyer_event_update() {
        Map<String, Object> eventValue = new HashMap<String, Object>();

        eventValue.put(AFInAppEventParameterName.OLD_VERSION, versioncode);
        eventValue.put(AFInAppEventParameterName.NEW_VERSION, latestversion);
        AppsFlyerLib.getInstance().trackEvent(getBaseContext(), AFInAppEventType.UPDATE, eventValue);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLatestversionCode();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.PUSH_NOTIFICATION));
        //  Toast.makeText(this,"resume",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }


    void createBroadCastReceiver() {


        //Creating broadcast receiver
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String type = intent.getStringExtra("type");
                //  if (intent.getAction().equals(Constants.PUSH_NOTIFICATION)) {
                Toast.makeText(getApplicationContext(), "From BroadCastReceiver", Toast.LENGTH_SHORT).show();


                // }


            }


        };

    }


    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (MyFirebaseMessagingService.id != null) {


            Fragment prFrag = new ProductList();
            Bundle databund = new Bundle();

            databund.putInt("productId", Integer.parseInt(MyFirebaseMessagingService.id));
            databund.putString("name", MyFirebaseMessagingService.catagoryname);
            databund.putString("fromwhere", "fromhome");
            prFrag.setArguments(databund);


            getSupportFragmentManager()
                    .beginTransaction().addToBackStack(null)
                    .replace(R.id.container2, prFrag)
                    .commit();


        }


        //Toast.makeText(getBaseContext(),"Arpan",Toast.LENGTH_SHORT).show();
    }

    public void DeviceRegister() {
        String serial;
        String android_id;
        String myKey;

        myKey = FortSdk.getDeviceId(getApplicationContext());



      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            // Todo Don't forget to ask the permission

            android_id = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            myKey =  android_id;
        }
        else
        {
            serial = Build.SERIAL;
            android_id = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            myKey = serial + android_id;
        }
*/

        // String serial = Build.SERIAL;
           /*String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            String myKey = serial + android_id;
*/
        //  parentDialogListener.showProgressDialog();

        BasicRequest apiService =
                BasicBuilder.getClient().create(BasicRequest.class);

        HashMap<String, String> map1 = new HashMap<>();
        map1.put("devicetype", "android");
        map1.put("fcm_token", FirebaseInstanceId.getInstance().getToken());
        map1.put("device_id", myKey);

        Call<LoginResponse> call = apiService.deviceregister(map1);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                if (response.body() != null) {


                    if (response.body().msg.equals("success")) {


                    } else {

                        Toast.makeText(getApplicationContext(), "لم تقم بتسجيل الدخول بشكل صحيح أو أن حسابك معطل مؤقتاً.", Toast.LENGTH_SHORT).show();


                    }
                } else {


                }


            }


            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

                Log.e(TAG, t.toString());
                System.out.println("ErrorApi...." + t.toString());

                Toast.makeText(getApplicationContext(), "حدث خطأ - يرجي اعادة المحاولة", Toast.LENGTH_LONG).show();
            }
        });


    }
}